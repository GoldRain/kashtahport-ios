//
//  CustomViewForMap.swift
//  Kashta Port
//
//  Created by mac on 22/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import Cosmos
import MapViewPlus

class CustomViewForMap: UIView,CalloutViewPlus {
    
    @IBOutlet weak var lblPriceTag: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDec: UILabel!
    @IBOutlet weak var viewStarRating: CosmosView!
    @IBOutlet weak var btnBookNow: UIButton!
    
    var property:PropertyModel?
    
    func configureCallout(_ viewModel: CalloutViewModel) {
        if let model = viewModel as? PropertyModel{
            property = model
            self.lblTitle.text = model.name!
            self.lblDec.text = model.description!
            self.viewStarRating.rating = model.rating!
            var currency = CurrentCurrency
            if let value = self.property?.currency, value != ""{
                if value.lowercased() == "usd"{
                }else{
                    currency = value
                }
            }
            self.lblPriceTag.text  = "\(currency.uppercased()) \(model.rate!)   "
        }
    }
    
    @IBAction func onBookNowClick(_ sender: Any) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HotelDetailsViewController") as? HotelDetailsViewController {
            vc.fromMap = true
            vc.property = property
            vc.userType = .Buyer
            vc.fromVC = .HomeVC
            appDel.topViewController?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
