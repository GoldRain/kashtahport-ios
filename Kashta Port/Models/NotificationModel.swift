//
//  NotificationModel.swift
//  kashtahPORT
//
//  Created by mac on 14/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation

class NotificationModel {
    
    var id:String = ""
    var user_id:String = ""
    var hint_id:String = ""
    var hint:String = ""
    var message:String = ""
    var created_at:String = ""
    var hotelData:PropertyModel?
    var bookingData:BookingModel?
    var rating:Double = 1.0
    var comment:String = ""
    var bookingId:String = ""
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self.id = value
        }
        if let value = data["user_id"] as? String{
            self.user_id = value
        }
        if let value = data["hint_id"] as? String{
            self.hint_id = value
        }
        if let value = data["hint"] as? String{
            self.hint = value
        }
        if let value = data["message"] as? String{
            self.message = value
        }
        if let value = data["created_at"] as? String{
            self.created_at = value
        }
        
        if let value = data["hotelData"] as? [[String:Any]] {
            if let data = value.first{
                self.hotelData = PropertyModel(data: data)
            }
        }
        
        if let value = data["bookingData"] as? [[String:Any]] {
            if let data = value.first{
                self.bookingData = BookingModel(data: data)
            }
        }
        
        if let value = data["feedbackData"] as? [[String:Any]] {
            if let data = value.first{
                if let p = data["price"] as? Int,let c = data["communication"] as? Int,let d = data["delivery"] as? Int,let pr = data["proficiency"] as? Int{
                    self.rating = Double(integerLiteral: (Int64(p + c + d + pr)))/4.0
                }
                
                if let comment = data["comment"] as? String{
                    self.comment = comment
                }
                
                if let value = data["booking_id"] as? String{
                    self.bookingId = value
                }
            }
        }
        
        
        
    }
    
}
