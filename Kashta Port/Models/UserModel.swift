//
//  UserModel.swift
//  kashtahPORT
//
//  Created by mac on 02/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation

class UserModel {
    
    var userId:String?
    var name:String?
    var email:String?
    var password:String?
    var phone:String?
    var type:String?
    var profileImageUrl:String?
    var deviceId:String?
    var deviceType:String?
    var notificationToken:String?
    var accessToken:String?
    var socketId:String?
    var onlineStatus:Bool?
    var isLogin:Bool?
    var isBlocked:Bool?
    var createdAt:String?
    var lastLogin:String?
    var favourites: [Any]?
    var canSale:Bool?
    
    
    
    init(data:[String:Any]) {
        if let value = data["_id"] as? String{
            self.userId = value
        }
        if let value = data["name"] as? String {
            self.name = value
        }
        
        if let value = data["email"] as? String {
            self.email = value
        }
        
        if let value = data["password"] as? String {
            self.password = value
        }
        
        if let value = data["phone"] as? String {
            self.phone = value
        }
        
        if let value = data["profile_image_url"] as? String {
            self.profileImageUrl = value
        }
        
        if let value = data["device_id"] as? String {
            self.deviceId = value
        }
        
        if let value = data["access_token"] as? String {
            self.accessToken = value
        }
        
        if let value = data["socket_id"] as? String {
            self.socketId = value
        }
        
        if let value = data["online_status"] as? Bool {
            self.onlineStatus = value
        }
        
        if let value = data["is_login"] as? Bool {
            self.isLogin = value
        }
        
        if let value = data["is_blocked"] as? Bool {
            self.isBlocked = value
        }
        
        if let value = data["last_login"] as? String {
            self.lastLogin = value
        }
        
        if let value = data["favourites"] as? [Any] {
            self.favourites = value
        }
        
        if let value = data["can_sale"] as? Bool {
            self.canSale = value
        }
    }
    
    
    
}
