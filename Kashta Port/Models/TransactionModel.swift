//
//  TransactionModel.swift
//  kashtahPORT
//
//  Created by mac on 16/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation

class TransactionModel {
    
    var transactionMongoId:String?
    var transactionId:String?
    var amount:Int?
    var bookingId:String?
    var buyerId:String?
    var cardDetail:[Any]?
    var createdAt:String?
    var displayId:Int?
    var hotelId:String?
    var ownerId:String?
    var paymentType:String?
    var title:String?
    var type:String?
    var cardType:String?
    var refId:String?
    var currency:String?
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self.transactionMongoId = value
        }
        
        if let value = data["amount"] as? Int{
            self.amount = value
        }
        
        if let value = data["booking_id"] as? String{
            self.bookingId = value
        }
        
        if let value = data["buyer_id"] as? String{
            self.buyerId = value
        }
        
        if let value = data["card_detail"] as? [Any]{
            self.cardDetail = value
        }
        
        if let value = data["created_at"] as? String{
            self.createdAt = value
        }
        
        if let value = data["display_id"] as? Int{
            self.displayId = value
        }
        
        
        if let value = data["hotel_id"] as? String{
            self.hotelId = value
        }
        
        if let value = data["owner_id"] as? String{
            self.ownerId = value
        }
        
        
        if let value = data["payment_type"] as? String{
            self.paymentType = value
        }
        
        if let value = data["title"] as? String{
            self.title = value
        }
        
        if let value = data["transaction_id"] as? String{
            self.transactionId = value
        }
        
        
        if let value = data["type"] as? String{
            self.type = value
        }
        
        if let value = data["ref_id"] as? String{
            self.refId = value
        }
        
        if let value = data["card_type"] as? String{
            self.cardType = value
        }
        
        if let value = data["currency"] as? String{
            self.currency = value
        }
        
    }
    
}
