//
//  IBANModel.swift
//  kashtahPORT
//
//  Created by alienbrainz on 27/06/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation


class IBANModel: NSObject, NSSecureCoding {
    
    static var supportsSecureCoding: Bool = true
    
    var country:String?
    var bankNumber:String?
    var bankName:String?
    var phoneNumber:String?
    
    init(data:[String:Any]) {
        
        if let value = data["country_code"] as? String{
            self.country = value
        }
        if let value = data["IBAN"] as? String{
            self.bankNumber = value
        }
        if let value = data["bank_name"] as? String{
            self.bankName = value
        }
        if let value = data["phone"] as? String{
            self.phoneNumber = value
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(country, forKey: "country_code")
        aCoder.encode(bankNumber, forKey: "IBAN")
        aCoder.encode(bankName, forKey: "bank_name")
        aCoder.encode(phoneNumber, forKey: "phone")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.country = aDecoder.decodeObject(forKey: "country_code") as? String
        self.bankNumber = aDecoder.decodeObject(forKey: "IBAN") as? String
        self.bankName = aDecoder.decodeObject(forKey: "bank_name") as? String
        self.phoneNumber = aDecoder.decodeObject(forKey: "phone") as? String
    }
}
