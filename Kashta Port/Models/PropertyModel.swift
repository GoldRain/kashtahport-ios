//
//  PropertyModel.swift
//  kashtahPORT
//
//  Created by mac on 02/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation
import CoreLocation

class PropertyModel{
    
    var propertyId:String?
    var location:CLLocationCoordinate2D?
    var ownerId:String?
    var type:String?
    var name:String?
    var city:String?
    var country:String?
    var pictures:[String]?
    var roomsNumber:String?
    var wcNumber:String?
    var personNumber:String?
    var childernNumber:String?
    var amenities = [Amenities]()
    var conditions:String?
    var availableDate = [HotelDate]()
    var rate:String?
    var rating:Double?
    var isEnabled:Bool?
    var isApproved:Bool?
    var createdAt:String?
    var updatedAt:String?
    var longitude:Double?
    var latitude:Double?
    var description:String?
    var address:String?
    var contactInfo:String?
    var isDeleted:Bool?
    var currency:String?
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self.propertyId = value
        }else{
            print("1")
        }
        
        if let value = data["location"] as? [String:Any] {
            if let cor = value["coordinates"] as? [Double]{
                self.location = CLLocationCoordinate2D(latitude: cor[0], longitude: cor[1])
            }
        }else{
            print("2")
        }
        
        if let value = data["owner_id"] as? String {
            self.ownerId = value
        }else{
            print("3")
        }
        
        if let value = data["type"] as? String {
            self.type = value
        }else{
            print("4")
        }
        
        if let value = data["name"] as? String {
            self.name = value
        }else{
            print("5")
        }
        
        if let value = data["city"] as? String {
            self.city = value
        }else{
            print("6")
        }
        
        if let value = data["country"] as? String {
            self.country = value
        }else{
            print("7")
        }
        
        if let value = data["pictures"] as? [String] {
            self.pictures = value
        }else{
            print("8")
        }
        
        if let value = data["rooms_number"] as? String {
            self.roomsNumber = value
        }else{
            print("9")
        }
        
        if let value = data["wc_number"] as? String {
            self.wcNumber = value
        }else{
            print("10")
        }
        
        if let value = data["person_number"] as? String {
            self.personNumber = value
        }else{
            print("11")
        }
        
        if let value = data["children"] as? String {
            self.childernNumber = value
        }else{
            print("12")
        }
        
        if let value = data["amenities"] as? [Any] {
            for item in value.enumerated(){
                if let data = item.element as? [String:Any]{
                    if let name = data["name"] as? String, let status = data["status"] as? Bool{
                        let am = Amenities(name: name, status: status)
                        self.amenities.append(am)
                    }
                }
            }
        }else{
            print("13")
        }
        
        if let value = data["conditions"] as? [String] {
            self.conditions = value.joined(separator: ",")
        }else{
            print("14")
        }
        
        if let value = data["available_date"] as? [Any] {
            for item in value.enumerated(){
                if let data = item.element as? [String:Any]{
                    if let startDate = data["start_date"] {
                        if let s = Int("\(startDate)"){
                            if let endDate = data["end_date"]{
                                if let d  = Int("\(endDate)") {
                                    if let nightrate = data["night_rate"] as? String{
                                        let hoteDate = HotelDate(startDate: s - (TimeZone.current.secondsFromGMT() * 1000), endDate: d - (TimeZone.current.secondsFromGMT() * 1000), nRate: nightrate)
                                        self.availableDate.append(hoteDate)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }else{
            print("15")
        }
        
        
        if let value = data["rate"] as? String {
            self.rate = value
        }else{
            print("16")
        }
        
        if let value = data["is_enable"] as? Bool {
            self.isEnabled = value
        }else{
            print("17")
        }
        
        if let value = data["is_approved"] as? Bool {
            self.isApproved = value
        }else{
            print("18")
        }
        
        if let value = data["rating"] as? Double {
            self.rating = value
        }else{
            print("19")
        }
        
        if let value = data["created_at"] as? String {
            self.createdAt = value
        }else{
            print("20")
        }
        
        if let value = data["updated_at"] as? String {
            self.updatedAt = value
        }else{
            print("21")
        }
        
        if let value = data["longitude"] as? Double {
            self.longitude = value
        }else{
            print("22")
        }
        
        if let value = data["latitude"] as? Double {
            self.latitude = value
        }else{
            print("23")
        }
        
        if let value = data["description"] as? String {
            self.description = value
        }else{
            print("24")
        }
        
        if let value = data["address"] as? String {
            self.address = value
        }else{
            print("25")
        }
        
        if let value = data["contact_info"] as? String {
            self.contactInfo = value
        }else{
            print("26")
        }
        
        if let value = data["is_deleted"] as? Bool {
            self.isDeleted = value
        }else{
            print("27")
        }
        
        if let value = data["currency"] as? String {
            self.currency = value
        }else{
            self.currency = ""
        }
    }
    
    
}
