//
//  BookingModel.swift
//  kashtahPORT
//
//  Created by mac on 07/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation

class BookingModel {
    
    var bookingId:String?
    var buyerId:String?
    var hotelId:String?
    var ownerId:String?
    var rooms:Int?
    var adults:Int?
    var childrens:Int?
    var dateFrom:String?
    var dateTo:String?
    var price:String?
    var status:String?
    var hotel:PropertyModel?
    var buyer:UserModel?
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self.bookingId = value
        }
        
        if let value = data["buyer_id"] as? String{
            self.buyerId = value
        }
        
        if let value = data["hotel_id"] as? String{
            self.hotelId = value
        }
        
        if let value = data["owner_id"] as? String{
            self.ownerId = value
        }
        
        if let value = data["rooms"] as? Int{
            self.rooms = value
        }
        
        if let value = data["childrens"] as? Int{
            self.childrens = value
        }
        if let value = data["adults"] as? Int{
            self.adults = value
        }
        
        if let value = data["date_from"] as? String{
            self.dateFrom = value
        }
        
        if let value = data["date_to"] as? String{
            self.dateTo = value
        }
        
        if let value = data["price"] as? String{
            self.price = value
        }
        
        if let value = data["status"] as? String{
            self.status = value
        }
        
        if let value = data["hotelData"] as? [Any]{
            if let item = value.first as? [String:Any]{
                self.hotel = PropertyModel(data: item)
            }
        }
        
        if let value = data["buyerData"] as? [Any]{
            if let item = value.first as? [String:Any]{
                 self.buyer = UserModel(data: item)
            }
        }
        
    }
}
