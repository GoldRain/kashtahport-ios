//
//  HotelDate.swift
//  kashtahPORT
//
//  Created by mac on 03/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation

class HotelDate {
    var startDate:Int
    var endDate:Int
    var nightRate:String
    
    init(startDate:Int,endDate:Int,nRate:String) {
        self.startDate = startDate
        self.endDate = endDate
        self.nightRate = nRate
    }
}
