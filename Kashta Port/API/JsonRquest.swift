
import UIKit


///CallBack Clousre For JSON Responce
///
public typealias JSONCallBack = (AnyObject?, Error?)->Void


///
///Class JSONRequest for making a request to web server and fetching responce data as async.
///
public final class JSONRequest{
    
    
    ///Private initilizer
    private init(){}
    
    ///
    ///Create Request to web server on url
    ///- Parameter url: Webservice URL
    ///- Parameter parameters: parameters to request
    ///- Parameter callBack: Call Back to process JSON Responce
    ///
    public static func makeRequest(_ url: String, parameters inputParameters: [String: String], callback: JSONCallBack?){
        
        appDel.printMessage(message: "Requesting url ==> \(url)")
            
            var parameters: [String: String] = inputParameters
        
            parameters["api_key"] = "T0BAnEzf6CaWwIMulImtA05nT6fDVjXKHoJM136S"
            
            for (key, value) in parameters{
                appDel.printMessage(message: "\(key) ==> \(value)")
            }
            
            DispatchQueue(label: "JSON_REQUEST_QUEUE", attributes: []).async(execute: {
                
                var request = URLRequest(url: URL(string: url)!)
                
                request.httpMethod = "POST"
                
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                
                request.setValue("T0BAnEzf6CaWwIMulImtA05nT6fDVjXKHoJM136S", forHTTPHeaderField: "api_key")
                
                let postString:NSMutableString = ""
                
                if parameters.count > 0{
                    let firstArgs = parameters.popFirst()!
                    postString.append(firstArgs.0+"="+firstArgs.1)
                }
                
                while let value = parameters.popFirst(){
                    postString.append("&"+value.0+"="+value.1.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)
                }
                
                request.httpBody = postString.data(using: String.Encoding.utf8.rawValue)
                
                request.setValue("\(postString.length)", forHTTPHeaderField: "Content-Length")
                
                let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
                    
                    if let data = data{
                        if let string = String(data: data, encoding: String.Encoding.utf8){
//                            appDel.printMessage(message: "RAW --> \(string)")
                        }
                    }
                    
                    guard error == nil && data != nil else {
                        
                        // check for fundamental networking error
                        self.processResponce(nil, error: error, callback: callback)
                        return
                    }
                    
                    if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200 {
                        // check for http errors
                        let error = NSError(domain: "Error", code: httpStatus.statusCode, userInfo: [NSLocalizedDescriptionKey: response!])
                        self.processResponce(nil, error: error, callback: callback)
                        return
                    }
                    
                    let object = self.getJSONObject(data!)
                    
                    self.processResponce(object, error: nil, callback: callback)
                })
                task.resume()
            })
        
    }
    
    public static func uploadData(_ url: String, parameters inputParameters: [String: String], uploadData data:[String:NSData?]?, callback: JSONCallBack?){
        
        var parameters: [String: String] = inputParameters
        
        print(parameters as Any)
        
        DispatchQueue(label: "JSON_REQUEST_QUEUE", attributes: []).async(execute: {
            
            // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
            let BoundaryConstant = "----------V2ymHFg03ehbqgZCaKO6jy"
            
            //create request
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "POST"
            //set request content type
            let contentType = "multipart/form-data; boundary=\(BoundaryConstant)"
            request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            
            let postString:NSMutableData = NSMutableData()
            //let postString:NSMutableString = ""
            
            // add params (all params are strings)
            for param: String in inputParameters.keys {
                postString.append("--\(BoundaryConstant)\r\n".data(using: .utf8)!)
                postString.append("Content-Disposition: form-data; name=\"\(param)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                postString.append("\(inputParameters[param]!)\r\n".data(using: String.Encoding.utf8)!)
                
            }
            
            
            //add upload data to the request
            
            postString.append("--\(BoundaryConstant)\r\n".data(using: .utf8)!)
            postString.append("Content-Disposition: form-data; name=image; filename=d.jpg\r\n".data(using: String.Encoding.utf8)!)
            postString.append("Content-Type: application/octet-stream\r\n\r\n".data(using: .utf8)!)
            if let data = data?.first?.value{
                postString.append(data as Data)
            }
            postString.append("\r\n".data(using: .utf8)!)
            
            postString.append("--\(BoundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            
//            print("request : \(postString)")
            
            request.httpBody = postString as Data //.data(using: String.Encoding.utf8.rawValue)
            
            
            
            let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
                
//                print("Recieve Resp from server ====> \(data as Any)")
                
                guard error == nil && data != nil else {
                    // check for fundamental networking error
                    self.processResponce(nil, error: error, callback: callback)
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200 {
                    // check for http errors
                    let error = NSError(domain: "Error", code: httpStatus.statusCode, userInfo: [NSLocalizedDescriptionKey: response!])
                    self.processResponce(nil, error: error, callback: callback)
                    return
                }
                
                let object = self.getJSONObject(data!)
                
                self.processResponce(object, error: nil, callback: callback)
            })
            task.resume()
        })
    }
    ///
    ///Process Responce data with call back Closure
    ///
    private static func processResponce(_ object: AnyObject?, error: Error?, callback: JSONCallBack?){
        
        DispatchQueue.main.async(execute: {
            callback?(object, error)
        })
    }
    
    ///
    ///Parse JSON Responce
    ///
    public static func getJSONObject(_ inputData: Data) -> AnyObject?{
        
        do{
//            print("\n\nJSON OBJECTT\n\n",String(data: inputData, encoding: .utf8) as Any,"\n***********\n\n")
            let value = try JSONSerialization.jsonObject(with: inputData, options: .allowFragments)
            print("\n------------\nResult of parsing \n***********\n\(value)\n*********")
            
            return value as AnyObject?
        }catch{
            print(error)
            return nil
        }
    }
}

