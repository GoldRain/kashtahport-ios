


//for local
//let apiBaseUrl = "http://192.168.0.130:8080"


// for server
let apiBaseUrl = "http://18.191.10.69:8080"


let kLogin                       = "\(apiBaseUrl)/login"
let kRegister                    = "\(apiBaseUrl)/registerUser"
let kLogout                      = "\(apiBaseUrl)/logout"
let kAddProperty                 = "\(apiBaseUrl)/addProperty"
let kListProperty                = "\(apiBaseUrl)/listProperty"
let kSingleProperty              = "\(apiBaseUrl)/singleProperty"
let kUpdateProperty              = "\(apiBaseUrl)/updateProperty"
let kManageProperty              = "\(apiBaseUrl)/manageProperty"
let kFavouriteHotelList          = "\(apiBaseUrl)/FavouriteHotelList"
let kRemoveToFavourite           = "\(apiBaseUrl)/removeToFavourite"
let kAddToFavourite              = "\(apiBaseUrl)/addToFavourite"
let kSearchHotel                 = "\(apiBaseUrl)/searchHotel"
let kCreateBooking               = "\(apiBaseUrl)/createBooking"
let kBookingRequest              = "\(apiBaseUrl)/bookingRequest"
let kUpdateBooking               = "\(apiBaseUrl)/updateBooking"
let kFeedback                    = "\(apiBaseUrl)/feedback"
let kViewProfile                 = "\(apiBaseUrl)/viewProfile"
let kNearByHotel                 = "\(apiBaseUrl)/nearByHotel"
let kUpdateProfile               = "\(apiBaseUrl)/updateProfile"
let kRating                      = "\(apiBaseUrl)/getFeedbackOfSingleHotel"
let kDeleteHotel                 = "\(apiBaseUrl)/deleteHotel"
let kGetOTP                      = "\(apiBaseUrl)/getOtp"
let kGetCallOTP                  = "\(apiBaseUrl)/makeCall"
let kVerifyOTP                   = "\(apiBaseUrl)/verifyOtp"
let kPaymentRequest              = "\(apiBaseUrl)/paymentRequest"
let kTransactionList             = "\(apiBaseUrl)/getPaymentList"
let kGetCountries                = "\(apiBaseUrl)/getCountries"
let kGetActivity                 = "\(apiBaseUrl)/getActivity"
let kGetSingleActivity           = "\(apiBaseUrl)/getSingleActivity"
let kSingleBookingRequest        = "\(apiBaseUrl)/singleBookingRequest"
let kFacebookLogin               = "\(apiBaseUrl)/facebook"

let kAboutUsEn                   = "\(apiBaseUrl)/public/aboutus.html"
let kAboutUsAr                   = "\(apiBaseUrl)/public/aboutusArbic.html"
let kPrivacyPolicyEn             = "\(apiBaseUrl)/public/policy.html"
let kPrivacyPolicyAr             = "\(apiBaseUrl)/public/policyArbic.html"
let kTermsAndConditionEn         = "\(apiBaseUrl)/public/term.html"
let kTermsAndConditionAr         = "\(apiBaseUrl)/public/termArbic.html"

let kCheckEmailAndPhone          = "\(apiBaseUrl)/checkEmailAndPhone"
let kSendEmailForChangePassword  = "\(apiBaseUrl)/sendEmailForChangePassword"
let kCheckEmail                  = "\(apiBaseUrl)/checkEmail"
let kUpdateIBAN                  = "\(apiBaseUrl)/updateIBAN"
let kHotelCheck                  = "\(apiBaseUrl)/hotelCheck"
let kUpdateNotificationToken     = "\(apiBaseUrl)/updateNotificationToken"
let kDeleteAllActivity           = "\(apiBaseUrl)/deleteAllActivity"
let kDeleteSingleActivity        = "\(apiBaseUrl)/deleteSingleActivity"
let kUpdateNotificationStatus    = "\(apiBaseUrl)/updateNotificationEnable"
let kUpdateCount                 = "\(apiBaseUrl)/updateCount"
