//
//  MessageCenter.swift
//  kashtahPORT
//
//  Created by mac on 04/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FBSDKCoreKit

class MessageCenter{
    
    static func isLiked(hotelId:String) -> Bool{
        if let favourites = MyProfile.favourites{
            if favourites.contains(hotelId){
                return true
            }
        }
        return false
    }
    
    static func isHotelDeleted(hotelId:String,completion:@escaping ((Bool) -> ())){
        
        let params = ["hotel_id":hotelId]
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kHotelCheck, parameters: params) { (data, error) in
            
            if let waitView = wait{
                appDel.topViewController?.removeWaitSpinner(waitView: waitView)
            }
            
            if error == nil {
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool,err{
                        completion(true)
                    }else{
                        if let msg = data["message"] as? String{
                            completion(false)
                        }
                    }
                }
            }else{
                completion(true)
            }
        }
    }
    
    static func addFavouritesHotel(hotelId:String,completion:@escaping (() -> ())){
        guard let userId = MyProfile.userId else {
            return
        }
        
        let params = ["user_id":userId,
                      "hotel_id":hotelId]
        
        JSONRequest.makeRequest(kAddToFavourite, parameters: params) { (data, error) in
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    print("Error")
                }else{
                    if let _ = data["message"] as? String{
                        MyProfile.favourites?.append(hotelId)
                        completion()
                    }
                }
            }
        }
    }
    
    static func removeFavouritesHotel(hotelId:String,completion:@escaping (() -> ())){
        guard let userId = MyProfile.userId else {
            return
        }
        
        let params = ["user_id":userId,
                      "hotel_id":hotelId]
        
        JSONRequest.makeRequest(kRemoveToFavourite, parameters: params) { (data, error) in
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    print("Error")
                }else{
                    if let _ = data["message"] as? String{
                        if let index = MyProfile.favourites?.firstIndex(of: hotelId){
                            MyProfile.favourites?.remove(at: index)
                            completion()
                        }
                    }
                }
            }
        }
    }
    
    static func logout(completion:@escaping (() -> ())){
    
        guard let email = MyProfile.userEmail else { return }
        
        let params = ["email":email]
        
        JSONRequest.makeRequest(kLogout, parameters: params) { (data, error) in
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    print("Error")
                }else{
                    if let _ = data["message"] as? String{
                        completion()
                    }
                }
            }
        }
    }
    
    static func logOutFacebook() {
        if AccessToken.current != nil{
            let loginManager = LoginManager()
            loginManager.logOut() // this is an instance function
            AccessToken.current = nil
        }
    }
    
    static func bookingUpdateStatus(id:String,status:String,completion:@escaping ((Bool, String?) -> ())){
        
        let params = ["booking_id":id,
                      "status":status.capitalized]
        
        JSONRequest.makeRequest(kUpdateBooking, parameters: params) { (data, error) in
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    if let message = data["message"] as? String {
                        completion(true, message)
                    }else{
                        completion(true, nil)
                    }
                }else{
                    completion(false, nil)
                }
            }
        }
    }
    
    static func paymentRequest(paymentData:PaymentData,booking:BookingModel,completion:@escaping ((Bool) -> ())){
        if let cardType = paymentData.crdtype{
            if let transactionId = paymentData.ref{
                if let bookingId = booking.bookingId{
                    if let hotelId = booking.hotelId{
                        if let ownerId = booking.ownerId{
                            if let buyerId = booking.buyerId{
                                if let amount = booking.price{
                                    if let name = booking.hotel?.name{
                                        
                                        var currency = CurrentCurrency
                                        if let value = booking.hotel?.currency, value != ""{
                                            currency = value
                                        }
                                        
                                        let params = ["hotel_id":hotelId,
                                                      "owner_id":ownerId,
                                                      "buyer_id":buyerId,
                                                      "booking_id":bookingId,
                                                      "amount":amount,
                                                      "payment_type":"Card",
                                                      "transaction_id":transactionId,
                                                      "title":"Paid For Booking \(name)",
                                                      "type":"Book",
                                                      "ref_no":transactionId,
                                                      "card_type":cardType,
                                                      "currency":currency
                                                        ]
                                        
                                        JSONRequest.makeRequest(kPaymentRequest, parameters: params) { (data, error) in
                                            if error == nil{
                                                if let data = data as? [String:Any]{
                                                    if let err = data["error"] as? Bool, err{
                                                        completion(true)
                                                    }else{
                                                        if let _ = data["message"] as? String{
                                                            completion(false)
                                                        }
                                                    }
                                                }
                                            }else{
                                                completion(true)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    static func updateNotificationToken(){
        
        guard let userId = MyProfile.userId, let notToken  = MyProfile.notificationToken else {
            return
        }
        
        let params = ["user_id":userId,
                      "notification_token":notToken,
                      "device_type":"IOS"]
        
        print("updateNotificatuib token")
        
        JSONRequest.makeRequest(kUpdateNotificationToken, parameters: params) { (data, error) in
            if error == nil{
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool,err{
                        if let msg = data["message"] as? String{
                            print(msg)
                        }
                    }else{
                        print("Update Notification Token")
                    }
                }
            }else{
                print(Error)
            }
        }
    }
    
}
