//
//  AppDelegate.swift
//  Kashta Port
//
//  Created by alienbrainz on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import DropDown
import GoogleMaps
import GooglePlaces
import AWSS3
import AWSCore
import goSellSDK
import FBSDKCoreKit
import Fabric
import Crashlytics
import OneSignal
import Firebase
import FirebaseDynamicLinks

var appDel:AppDelegate!
var myProfile:MyProfile?

//Support
let SupportMail = "kashtahport@gmail.com"

//Google API Maps
let kGMSPlacesClientKey   = "AIzaSyAgOG1E-tCFQvrGSP1ZOx4MjamFZRQf4q8"
let kGMSServicesClientKey = "AIzaSyAgOG1E-tCFQvrGSP1ZOx4MjamFZRQf4q8"

//AWS Details
let accessKey = "AKIAJ7NUFTKSHAX2IQWA"
let secretKey = "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"
let bucket    = "kashport"

//TAP Keys

//Test
//let authenticationKey = "sk_test_XKokBfNWv6FIYuTMg5sLPjhJ"
//let encryptionKey = "pk_test_EtHFV4BuPQokJT6jiROls87Y"
//let authenticationKey = "sk_test_Y3xjFq8ZePsNkIdUD2lu69b1"
//let encryptionKey = "pk_test_BZU8QszKkClpAbWFqDVogHr0"

//Live
let authenticationKey = "sk_live_JcmiLUzWl20noA9kyqsfYhNK"
let encryptionKey = "pk_live_H3Ed9aRDe8bAFshQWB6pUIwt"

//One-Signal Key
let ONESIGNAL = "134846ad-d9da-4221-8ee1-cc594387cdb5" //"144eccd5-4016-4a53-b05a-afa5f971ddf3"

var CurrentCurrency = "$"
var CurrencyType = "usd"

 let APPLE_LANGUAGE_KEY = "AppleLanguages"

//UIImage compression
let compression:CGFloat = 0.5

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,OSSubscriptionObserver{

    var window: UIWindow?
    
    var dynamicLink:String?
    
    var hotelImagesFolder:String{
        return "HotelImages/"
    }
    
    var userImageFolder:String{
        return "Userimages/"
    }
    
    var workItem:DispatchWorkItem?
    
    /// get current Apple language
    func currentAppleLanguage() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    
    /// set @lang to be the first in Applelanguages list
    func setAppleLAnguageTo(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        appDel = self
        
        
        
        DropDown.startListeningToKeyboard()
        
        //FBSDKCoreKit
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Setup IQKeyboardManager
        IQKeyboardManager.shared.enable = true
//      IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(AddPropertyViewController.self)
//      IQKeyboardManager.shared.disabledToolbarClasses.append(AddPropertyViewController.self)
        
        
        
        // SetUp UINavigationbar
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)

        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().clipsToBounds = false
        
        let color = UIColor.black
        let font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        
        let attributes: [NSAttributedString.Key: AnyObject] = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.foregroundColor: color
        ]
        
        UINavigationBar.appearance().titleTextAttributes = attributes
        
        if let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView {
            statusBarView.backgroundColor = .clear
        }
        
        let backImage = UIImage(named: "go_back")?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        if #available(iOS 11, *) {
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -300, vertical: 0), for:UIBarMetrics.default)
        } else {
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: -200), for:UIBarMetrics.default)
        }
        
        //Google maps
        GMSServices.provideAPIKey(kGMSServicesClientKey)
        GMSPlacesClient.provideAPIKey(kGMSPlacesClientKey)
        
        
        //Configure AWS US_EAST_2
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.USEast2, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        //Go Sell SDK
        goSellSDK.authenticationKey = authenticationKey
        goSellSDK.encryptionKey = encryptionKey
        
        //Fabric
        Fabric.with([Crashlytics.self])
        
        //ONE SIGNAL
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'ONESIGNAL' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: ONESIGNAL,
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
//        OneSignal.setEmail("ming22@gmail.com")
//        OneSignal.setSubscription(true)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            
            print("User accepted notifications: \(accepted)")
            
            if #available(iOS 10.0, *) {
                
                let center = UNUserNotificationCenter.current()
                center.delegate = self
                
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound, .badge]) {(accepted, error) in
                    if !accepted {
                        print("Notification access denied")
                    }else{
//                        OneSignal.setSubscription(true)
//                        let status = OneSignal.getPermissionSubscriptionState()
//                        let userId = status?.subscriptionStatus.userId
//
//                        print("User id =>> \(userId)")
                    }
                }
            }
        })
        
        // Add your AppDelegate as an subscription observer
        OneSignal.add(self as OSSubscriptionObserver)
        
        FirebaseApp.configure()
        
        return true
    }
    
    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        print("Darshan   5556")
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            guard error == nil else{
                print(("Dynamic link url 3 Found Error \(error!.localizedDescription)"))
                self.topViewController?.displayMessage("Failed to handle dynamic link. Please try again.")
                return
            }
            if let dynamicLink = dynamiclink{
                self.handleIncomingDynaminLink(dynamicLink)
            }
        }
        
        return handled
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return application(app, open: url,
                           sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                           annotation: "")
    }
    
    // After you add the observer on didFinishLaunching, this method will be called when the notification subscription property changes.
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        
        printM("SubscriptionStateChange: \n\(stateChanges)")
        printM("user id \(stateChanges.to.userId)")
        
        MyProfile.notificationToken = stateChanges.to.userId

    }
    
    //FBSDKCoreKit
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    
        print("Darshan   7793")
        
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            self.handleIncomingDynaminLink(dynamicLink)
            return true
        }
        
        let handled = ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
        return handled
    }
    
    func handleIncomingDynaminLink(_ dynamicLink:DynamicLink){
        
        guard  let url = dynamicLink.url else {
            print(("Dynamic link has no url"))
            return
        }
        print("Dynamic link url \(url.absoluteString)")
        self.dynamicLink = url.absoluteString
        
        NotificationCenter.default.post(name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

        self.updateCount()
        if let badge = MyProfile.badge{
            if badge != 0 {
                UIApplication.shared.applicationIconBadgeNumber = badge
            }
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.getNotificationsFromServer()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//        badge = 0
        self.updateCount()
        if let badge = MyProfile.badge{
            if badge != 0 {
                UIApplication.shared.applicationIconBadgeNumber = badge
            }
        }
    }
    
    var topViewController:UIViewController? {
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // topController should now be your topmost view controller
            if topController is UINavigationController {
                if let top = topController as? UINavigationController {
                    if let newTop = top.visibleViewController.self {
                        return newTop
                    }
                }
            } else {
                return topController
            }
        }
        return nil
    }
    
    func updateCount() {
        guard let userId = MyProfile.userId else{
            return;
        }
        if let badge = MyProfile.badge{
            JSONRequest.makeRequest(kUpdateCount, parameters: ["user_id" : userId, "count": "\(badge)"]) { (_, _) in
            }
        }
    }
    
    func printM(_ str:String){
        print("Kashta Port --> \(str)")
    }
    func printMessage(message str:String){
        self.printM(str)
    }
    
    func getNotificationsFromServer() {
        
        print("RDX == Method Called")
        
//        badge = 0
        
        MyProfile.badge = 0
        
        var arr = [String]()
        
        guard let userId = MyProfile.userId else{
            return;
        }
        
        print("RDX == Method Called")
        
        let params = ["user_id":userId]
        
        workItem = DispatchWorkItem{
            JSONRequest.makeRequest(kGetActivity, parameters: params) { (data, error) in
                if error == nil {
                    if let data = data as? [String:Any]{
                        if let err = data["error"] as? Bool, !err {
                            if let notifications = data["data"] as? [[Any]] {
                                for not in notifications{
                                    if let notificationData = not.first as? [String:Any]{
                                        let not = NotificationModel(data: notificationData)
                                        arr.append(not.id)
                                        
                                    }
                                }
                            }
                        }
                    }
                }
                for item in arr{
                    if let reads = readActivitys{
                        if reads.contains(item){
                            print("RDX ==> hello")
                        }else{
//                            badge = badge + 1
                            if let badge = MyProfile.badge {
                                MyProfile.badge = badge + 1
                                print("RDX ==> \(MyProfile.badge)")
                            }else{
                                MyProfile.badge = 1
                            }
                        }
                    }
                }
                NotificationCenter.default.post(name: .UpdateBadge, object: nil)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: workItem!)
        
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    //MARK: APNS
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("I am not available in simulator \(error)")
        
    }
    
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("Kastaport aaa  =====>>     0")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Kastaport aaa  =====>>     1 \(userInfo)")
        if let aps = userInfo["aps"] as? [String:Any] {
            if let alert = aps["alert"] as? [String:Any] {
                if let body = alert["body"] as? String{
                    if body == "You got a booking" {
                        TabBarVC?.selectedIndex = 1
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ManagePropertyViewController") as! ManagePropertyViewController
                        appDel.topViewController?.navigationController?.pushViewController(vc , animated: true)
                    }
                }
            }
        }
        if UIApplication.shared.applicationState == .background{
            completionHandler(.noData)
        }else{
            completionHandler(.noData)
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Kastaport aaa  =====>>     2  \(notification)")
        
//        badge = badge + 1
        if let badge = MyProfile.badge {
            MyProfile.badge = badge + 1
        }else{
            MyProfile.badge = 1
        }
        NotificationCenter.default.post(name: .UpdateBadge, object: nil, userInfo: nil)
        
        if UIApplication.shared.applicationState == .background{
            completionHandler( [.alert, .badge, .sound])
        }else{
            completionHandler(.alert)
        }
    }
    
    private func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Kastaport aaa  =====>>     3")
        completionHandler()
    }
    
    // MARK: Utility
    func dispatchlocalNotification(identifier:String, title: String, body: String, userInfo: [AnyHashable: Any]?) {
        
        print("Title --> \(title) Body --> \(body)")
        
        if #available(iOS 10.0, *) {
            
            //iOS 10 or above version
            let content = UNMutableNotificationContent()
            content.title = title
            content.body = body
            content.sound = UNNotificationSound.default
            if let userInfo = userInfo{
                content.userInfo = userInfo
            }
            
            var dateComponents = Calendar.current.dateComponents([.hour, .minute, .second], from: Date())
            
            dateComponents.second = dateComponents.second! + 1
            
            print(dateComponents)
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
            
            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
                if let error = error{
                    print(error)
                }
            })
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            
            // ios 9
            let notification = UILocalNotification()
            notification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
            notification.alertBody = body
            notification.alertAction = title
            notification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.shared.scheduleLocalNotification(notification)
            
        }
        
        print("WILL DISPATCH LOCAL NOTIFICATION AT ", Date())
        
    }
}
//
public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
       // let output = items.map { "\($0)" }.joined(separator: separator)
       // Swift.print(output, terminator: terminator)
}
