//
//  CalenderDateCell.swift
//  kashtahPORT
//
//  Created by mac on 28/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import FSCalendar

class CalenderDateCell: FSCalendarCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
}
