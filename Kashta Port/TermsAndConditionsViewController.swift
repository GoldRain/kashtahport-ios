
//
//  TermsAndConditionsViewController.swift
//  kashtahPORT
//
//  Created by alienbrainz on 09/07/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = TermsandConditions
        self.setBackButton(withText: "")
        if appDel.currentAppleLanguage() == "ar" {
            self.webView.loadRequest(URLRequest(url: URL(string: kTermsAndConditionAr)!))
        }else{
            self.webView.loadRequest(URLRequest(url: URL(string: kTermsAndConditionEn)!))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.view.addWaitView()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.view.removeWaitView()
    }
    
    
}
