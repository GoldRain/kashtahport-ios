//
//  LoginView.swift
//  kashtahPORT
//
//  Created by alienbrainz on 20/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    @IBOutlet weak var lblLoginTitle: UILabel!
    @IBOutlet weak var lblLoginDescription: UILabel!
    
    override func awakeFromNib() {
        lblLoginDescription.text = LoginText
    }
    
    @IBAction func onLoginClick(_ sender: Any) {
        let vc = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        vc.type = .Other
        let nvc = UINavigationController(rootViewController: vc)
        appDel.topViewController?.navigationController?.present(nvc, animated: true, completion: nil)
        
    }
}
