//
//  MyProfile.swift
//  kashtahPORT
//
//  Created by mac on 30/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit

class MyProfile {
    
    internal static func set(rawData: [String: Any]) {
        
        if let value = rawData["_id"] as? String {
            userId = value
        }
        
        if let value = rawData["name"] as? String {
            userName = value
        }
        
        if let value = rawData["email"] as? String {
            userEmail = value
        }
        
        if let value = rawData["password"] as? String {
            password = value
        }
        
        if let value = rawData["phone"] as? String {
            mobile = value
        }
        
        if let value = rawData["profile_image_url"] as? String {
            profilePhotoUrl = value
        }
        
        if let value = rawData["device_id"] as? String {
            deviceId = value
        }
        
        if let value = rawData["access_token"] as? String {
            accessToken = value
        }
        
        if let value = rawData["socket_id"] as? String {
            socketId = value
        }
        
        if let value = rawData["online_status"] as? Bool {
            onlineStatus = value
        }
        
        if let value = rawData["is_login"] as? Bool {
            isLogin = true
        }
        
        if let value = rawData["is_blocked"] as? Bool {
            isBlocked = value
        }
        
        if let value = rawData["last_login"] as? String {
            lastLoginTime = value
        }
        
        if let value = rawData["can_sale"] as? Bool {
            canSale = value
        }
        
        if let value = rawData["country_code"] as? String {
            countryCode = value
        }
        
        if let value = rawData["created_at"] as? String {
            createdAt = value
        }
        
        if let value = rawData["device_type"] as? String {
            deviceType = "IOS"
        }
        
        if let value = rawData["favourites"] as? [String] {
            favourites = value
        }

        
        if let value = rawData["profession"] as? String {
            profession = value
        }
        
        if let value = rawData["description"] as? String {
            description = value
        }
        
        if let value = rawData["notification_enable"] as? Bool {
            isNotificationEnabled = value
        }
        
        if let value = rawData["IBAN"] as? [String:Any] {
            IBANInfo = IBANModel(data: value)
        }
    }
    
    static func logOutUser(){
        userId = nil
        userName = nil
        userEmail = nil
        password = nil
        mobile = nil
        emailVerification = nil
        mobileVerification = nil
        profilePhotoUrl = nil
        deviceId = nil
        socketId = nil
        accessToken = nil
        onlineStatus = false
        isLogin = false
        isBlocked = false
        profileDesc = nil
        lastLoginTime = nil
        canSale = false
        countryCode = nil
        createdAt = nil
        deviceType = nil
        favourites = nil
        profession = nil
        description = nil
        isNotificationEnabled = false
        
        if AccessToken.current != nil{
            let loginManager = LoginManager()
            loginManager.logOut()
            AccessToken.current = nil
        }
        badge = nil
    }
    
    internal static var userId: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kId)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kId.rawValue) as? String
        }
    }
    
    internal static var userName: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kUserName)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kUserName.rawValue) as? String
        }
    }
    
    internal static var userEmail: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kUserEmail)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kUserEmail.rawValue) as? String
        }
    }
    
    internal static var password: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kPassword)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kPassword.rawValue) as? String
        }
    }
    
    internal static var mobile: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kMobile)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kMobile.rawValue) as? String
        }
    }
    
    internal static var emailVerification: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kEmailVerification)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kEmailVerification.rawValue) as? String
        }
    }
    
    internal static var mobileVerification: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kMobileVerification)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kMobileVerification.rawValue) as? String
        }
    }
    
    internal static var profilePhotoUrl: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kProfilePhotoUrl)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kProfilePhotoUrl.rawValue) as? String
        }
    }
    
    internal static var deviceId: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kDeviceId)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kDeviceId.rawValue) as? String
        }
    }
    
    internal static var socketId: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kSocketId)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kSocketId.rawValue) as? String
        }
    }
    
    internal static var accessToken: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kAccessToken)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kAccessToken.rawValue) as? String
        }
    }
    
    internal static var onlineStatus: Bool {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kOnlineStatus)
            preferences.synchronize()
        }
        
        get{
            if let value = UserDefaults.standard.object(forKey: Key.kOnlineStatus.rawValue) as? Bool{
                return value
            }
            return false
        }
    }
    
    internal static var isLogin: Bool {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kIsLogin)
            preferences.synchronize()
        }
        
        get{
            if let value = UserDefaults.standard.object(forKey: Key.kIsLogin.rawValue) as? Bool{
                return value
            }
            return false
        }
    }
    
    internal static var isBlocked: Bool {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kIsBlocked)
            preferences.synchronize()
        }
        
        get{
            if let value = UserDefaults.standard.object(forKey: Key.kIsBlocked.rawValue) as? Bool{
                return value
            }
            return false
        }
    }
    
    internal static var profileDesc: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kProfileDesc)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kProfileDesc.rawValue) as? String
        }
    }
    
    internal static var lastLoginTime: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kLastLoginTime)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kLastLoginTime.rawValue) as? String
        }
    }
    
    internal static var canSale: Bool {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kCanSale)
            preferences.synchronize()
        }
        
        get{
            if let value = UserDefaults.standard.object(forKey: Key.kCanSale.rawValue) as? Bool{
                return value
            }
            return false
        }
    }
    
    internal static var countryCode: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kCountryCode)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kCountryCode.rawValue) as? String
        }
    }
    
    internal static var createdAt: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kCreatedAt)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kCreatedAt.rawValue) as? String
        }
    }
    
    internal static var deviceType: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kDeviceType)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kDeviceType.rawValue) as? String
        }
    }
    
    internal static var favourites: [String]? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kFavourites)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kFavourites.rawValue) as? [String]
        }
    }
    
    internal static var notificationToken: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kNotificationToken)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kNotificationToken.rawValue) as? String
        }
    }
    
    internal static var profession: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kProfession)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kProfession.rawValue) as? String
        }
    }
    
    internal static var description: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kDescription)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kDescription.rawValue) as? String
        }
    }
    
    internal static var isNotificationEnabled: Bool {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: Key.kIsNotificationEnabled.rawValue)
            preferences.synchronize()
        }
        
        get{
            if let value = UserDefaults.standard.object(forKey: Key.kIsNotificationEnabled.rawValue) as? Bool{
                return value
            }
            return false
        }
    }
    
    internal static var IBANInfo:IBANModel? {
        
        set{
            if let value = newValue{
                let userDefaults = UserDefaults.standard
                do {
                    userDefaults.setValue( try NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: true), forKey: Key.kIBANInfo.rawValue)
                }catch {
                    print("unable to archiver data [1]")
                }
                userDefaults.synchronize()
            }
            else{
                let userDefaults = UserDefaults.standard
                userDefaults.set(nil, forKey: Key.kIBANInfo.rawValue)
                userDefaults.synchronize()
            }
        }
        get{
            if let data = (UserDefaults.standard.object(forKey: Key.kIBANInfo.rawValue) as? Data){
                do {
                    return try NSKeyedUnarchiver.unarchivedObject(ofClasses: [IBANModel.self], from: data) as? IBANModel
                }catch{
                    print("unable to unarchive data [2]")
                    return nil
                }
            }
            else{
                return nil
            }
        }
    }
    
    internal static var badge: Int? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kBadge)
            preferences.synchronize()
            if newValue == nil {
                NotificationCenter.default.post(name: .UpdateBadge, object: nil, userInfo: nil)
            }
        }
        get{
            return UserDefaults.standard.value(forKey: Key.kBadge.rawValue) as? Int
        }
    }
}

public enum Key: String {
    
    case kId                     = "Id"
    case kUserName               = "Name"
    case kUserEmail              = "Email"
    case kPassword               = "Password"
    case kMobile                 = "Mobile"
    case kEmailVerification      = "EmailVerification"
    case kMobileVerification     = "MobileVerification"
    case kProfilePhotoUrl        = "ProfilePhotoUrl"
    case kDeviceId               = "DeviceId"
    case kSocketId               = "SocketId"
    case kAccessToken            = "AccessToken"
    case kOnlineStatus           = "OnlineStatus"
    case kIsLogin                = "IsLogin"
    case kIsBlocked              = "IsBlocked"
    case kProfileDesc            = "profileDescription"
    case kLastLoginTime          = "LastLoginTime"
    case kCanSale                = "CanSale"
    case kCountryCode            = "CountryCode"
    case kCreatedAt              = "CreatedAt"
    case kDeviceType             = "DeviceType"
    case kNotificationToken      = "NotificationToken"
    case kFavourites             = "Favourites"
    case kProfession             = "Profession"
    case kDescription            = "Description"
    case kIsNotificationEnabled  = "isNotificationEnabled"
    case kIBANInfo               = "ibanInfo"
    case kBadge                  = "badge"
}

fileprivate extension UserDefaults{
    
    fileprivate func set(_ value: Any?, forKey key: Key){
        self.set(value, forKey: key.rawValue)
    }
    
    fileprivate func string(forKey key: Key) -> String{
        return self.string(forKey: key.rawValue) ?? ""
    }
    
    fileprivate func nullableString(forKey key: Key) -> String?{
        return self.string(forKey: key.rawValue)
    }
    
    fileprivate func bool(forKey key: Key) -> Bool{
        return self.bool(forKey: key.rawValue)
    }
}
