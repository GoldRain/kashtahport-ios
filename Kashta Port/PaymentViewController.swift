//
//  PaymentViewController.swift
//  kashtahPORT
//
//  Created by mac on 15/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.

import Foundation

import UIKit
import WebKit
import goSellSDK

let processURL = "\(apiBaseUrl)/public/"

protocol PayementViewControllerDelegate {
    
    func paymentWebViewDidCancel(_ paymentViewController:PaymentViewController)
    
    func paymentWebViewDidFinish(_ paymentViewController:PaymentViewController, paymentInfo:PaymentData)
    
    func paymentWebViewDidFail(_ paymentViewController:PaymentViewController)
    
}

class PaymentViewController: UIViewController {
    
    
    @IBOutlet weak var webView:WKWebView!
    
    var delegate:PayementViewControllerDelegate!
    
    override func viewDidLoad() {
        
        self.webView.allowsBackForwardNavigationGestures = false
        self.webView.allowsLinkPreview = false
        self.webView.navigationDelegate = self
        self.webView.load(URLRequest(url: URL(string: processURL)!))
        
    }
    
    
    
    func createCharge(amount:Decimal, currency:String, customerFirstName:String, customerLastName:String, customerEmail:String, customerPhone:String){
        
        let redirect = CreateChargeRedirect(returnURL: URL(string: processURL)!)

        let request = CreateChargeRequest(amount: amount, currency: currency, customer: CreateChargeCustomer(firstName: customerFirstName, lastName: customerLastName, email: customerEmail, phone: customerPhone), redirect: redirect, source: nil)

        ChargeClient.createCharge(with: request) { (charge, error) in

            DispatchQueue.main.async {

                if let charge = charge {

                    if let url = charge.redirect?.url{
                        NSLog("Charge successfully created. Loading UI..")
                        self.webView.load(URLRequest(url: url))
                    }
                }
                else {
                    //in case fail to create charge
                    print("Failed to Create charge => \(error)")
                    self.delegate.paymentWebViewDidFail(self)
                }
            }
        }
    }
    
    @IBAction func onCancelClick(_ sender:Any){
        
        let alert = getAlertController(title: "", message: AlertCancel)
        
        alert.addAction(UIAlertAction(title: Yes, style: .default, handler: { (_) in
            
            self.delegate.paymentWebViewDidCancel(self)
            
        }))
        
        alert.addAction(UIAlertAction(title: No, style: .destructive, handler: nil))
        
        appDel.topViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    public static func showPaymentWithData(amount:Decimal, currency:String, customerFirstName:String, customerLastName:String, customerEmail:String, customerPhone:String, delegate:PayementViewControllerDelegate) -> PaymentViewController?{
        
        if let nvc = UIStoryboard(name: "Payment", bundle: nil).instantiateViewController(withIdentifier: "PaymentNavigationController") as? UINavigationController{
            
            if let vc = nvc.viewControllers.first as? PaymentViewController{
                if let top = appDel.topViewController{
                    
                    vc.delegate = delegate
                    
                    top.present(nvc, animated: true, completion: nil)
                    
                    vc.createCharge(amount: amount, currency: currency, customerFirstName: customerFirstName, customerLastName: customerLastName, customerEmail: customerEmail, customerPhone: customerPhone)
                    
                    return vc
                }
            }
            
        }
        return nil
    }
    
}

//MARK: WKNavigationDelegate
extension PaymentViewController:WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        
        print("Failed to load")
        
        print(error.localizedDescription)
        
        self.delegate.paymentWebViewDidFail(self)
        
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        print("Start to load")
        
        if let url = webView.url?.absoluteString{
            print("url = \(url)")
        }
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        print("finish to load")
        
        if let url = webView.url?.absoluteString{
            
            print("url = \(url)")
            
            if let dict = webView.url?.queryParameters{
                
                if let _ = dict["result"] as? String{

                    DispatchQueue.main.asyncAfter(deadline: .now()+3.0) {
                        self.delegate.paymentWebViewDidFinish(self, paymentInfo: PaymentData(fromDictionary: dict))
                    }
                    
                }
            }
        }
    }
}

//MARK: PaymentData class to represt the payment data
class PaymentData{
    
    var chargeid : String!
    var crd : String!
    var crdtype : String!
    var hash : String!
    var payid : String!
    var ref : String!
    var result : String!
    var trackid : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        chargeid = dictionary["chargeid"] as? String
        crd = dictionary["crd"] as? String
        crdtype = dictionary["crdtype"] as? String
        hash = dictionary["hash"] as? String
        payid = dictionary["payid"] as? String
        ref = dictionary["ref"] as? String
        result = dictionary["result"] as? String
        trackid = dictionary["trackid"] as? String
    }
    
}

extension URL {
    
    public var queryParameters: [String: String]? {
        guard let components = URLComponents(url: self, resolvingAgainstBaseURL: true), let queryItems = components.queryItems else {
            return nil
        }
        
        var parameters = [String: String]()
        for item in queryItems {
            parameters[item.name] = item.value
        }
        
        return parameters
    }
}

