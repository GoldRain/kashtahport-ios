//
//  SelectImageViewController.swift
//  kashtahPORT
//
//  Created by mac on 14/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class SelectImageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    let imagePicker = UIImagePickerController()
    
    var callback:((UIImage?)->())!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let view = touches.first?.view {
            if view == self.view{
                self.dismiss(animated: true) {
                    self.callback(nil)
                }
            }
        }
    }
    
    @IBAction func onGalleryClick(_ sender:UIButton) {
        imagePicker.delegate = self as UIImagePickerControllerDelegate as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.navigationBar.barStyle = .blackOpaque
        imagePicker.navigationBar.tintColor = APP_PRIMARY_COLOR
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage{
            picker.dismiss(animated: true) {
                self.dismiss(animated: true, completion: {
                    self.callback(image)
                })
            }
        }
        if let image = info[.editedImage] as? UIImage{
            picker.dismiss(animated: true) {
                self.dismiss(animated: true, completion: {
                    self.callback(image)
                })
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: {
            self.callback(nil)
        })
    }
    
    @IBAction func onCameraClick(_ sender:UIButton) {
        self.openCamera()
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }else {
            let alert  = getAlertController(title: "Warning", message: "You don't have camera")
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
