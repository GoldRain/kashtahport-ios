//
//  ForgotPasswordViewController.swift
//  Kashta Port
//
//  Created by alienbrainz on 27/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var tfEmail: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onBackClick(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSendClick(_ sender: Any) {
        if validate(){
            
            let parmas = ["email":tfEmail.text!]
            
            self.view.addWaitView()
            
            JSONRequest.makeRequest(kCheckEmail, parameters: parmas) { (data, error) in
                if error == nil{
                    if let data = data as? [String:Any]{
                        if let err = data["error"] as? Bool, err{
                            self.view.removeWaitView()
                            self.displayMessage(EmailNotReg)
                        }else{
                            if let _ = data["message"] as? String{
                               
                                JSONRequest.makeRequest(kSendEmailForChangePassword, parameters: parmas) { (data, error) in
                                    if error == nil{
                                        if let data = data as? [String:Any]{
                                            if let err = data["error"] as? Bool, err{
                                                self.view.removeWaitView()
                                            }else{
                                                if let _ = data["message"] as? String{
                                                    self.view.removeWaitView()
                                                    self.displayMessage(LinkSend)
                                                }
                                            }
                                        }
                                    }else{
                                        self.view.removeWaitView()
                                    }
                                }
                                
                            }
                        }
                    }
                }else{
                    self.view.removeWaitView()
                }
            }
            
            
        }
    }
    
    func validate() -> Bool{
    
        if let email = tfEmail.text, email.trim() == ""{
            self.displayMessage(EnterEmail)
            return false
        }
        
        if !isValidEmailId(tfEmail.text){
            self.displayMessage(ValidEmail)
            return false
        }
        
        return true
    }
    
}

