//
//  FilterViewController.swift
//  Kashta Port
//
//  Created by mac on 23/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import MARKRangeSlider
import DropDown

class FilterViewController: UIViewController {

    let dropDownRoom = DropDown()
    let dropDownWC = DropDown()
    let dropDownPersons = DropDown()
    let dropDownType = DropDown()
    let dropDownCountry = DropDown()
    let dropDownCity = DropDown()
    let dropDownChild = DropDown()
    
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewRoom: UIView!
    @IBOutlet weak var viewWC: UIView!
    @IBOutlet weak var viewPersons: UIView!
    @IBOutlet weak var viewChildren: UIView!
    @IBOutlet weak var lblRoomCount: UILabel!
    @IBOutlet weak var lblWCCount: UILabel!
    @IBOutlet weak var lblPersonsCount: UILabel!
    @IBOutlet weak var lblChildrenCount: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "E, MMM d"
        return formatter
    }()
    
    var country = [CityCountry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewRoom.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleRoomTap)))
        self.viewWC.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleWCTap)))
        self.viewPersons.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlePersonsTap)))
        self.viewType.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTypeTap)))
        self.viewCountry.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleCountryTap)))
        
        if self.lblCountry.text != "Select Country" {
            self.viewCity.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleCityTap)))
        }
        
        self.viewChildren.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleChildrenTap)))
        
       self.setupData()
    }
    
    func setupData(){
        
        self.getCitynCountry()
        
        if SearchHotel.type != "" {
            self.lblType.text = SearchHotel.type
        }else{
            self.lblType.text = SelectPropertyType
        }
//        if SearchHotel.city != "" {
//            self.lblCity.text = SearchHotel.city
//        }else{
//            self.lblCity.text = SelectCity
//        }
        if SearchHotel.country != "" {
            self.lblCountry.text = SearchHotel.country
        }else{
            self.lblCountry.text = SelectCountry
        }
        if SearchHotel.startDate != "" {
            self.lblFrom.text = self.formatter.string(from: Date(milliseconds: Int(SearchHotel.startDate)!))
        }else{
            self.lblFrom.text = SelectStartDate
        }
        if SearchHotel.endDate != "" {
            self.lblTo.text = self.formatter.string(from: Date(milliseconds: Int(SearchHotel.endDate)!))
        }else{
            self.lblTo.text = SelectEndDate
        }
        if SearchHotel.room != "" {
            self.lblRoomCount.text = SearchHotel.room
        }else{
            self.lblRoomCount.text = SelectRoomNumber
        }
        if SearchHotel.child != "" {
            self.lblChildrenCount.text = SearchHotel.child
        }else{
            self.lblChildrenCount.text = SelectChildrenNumber
        }
        if SearchHotel.wc != "" {
            self.lblWCCount.text = SearchHotel.wc
        }else{
            self.lblWCCount.text = SelectWCNumber
        }
        if SearchHotel.person != "" {
            self.lblPersonsCount.text = SearchHotel.person
        }else{
            self.lblPersonsCount.text = SelectPersonsNumber
        }
    }
    
    @IBAction func onShowResultClick(_ sender: Any) {
        self.setItemsInSearchHotel()
        NotificationCenter.default.post(name: .SearchHotel, object: nil, userInfo: ["type" : SearchHotel.type])
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onResetClick(_ sender: Any) {
        self.displayMessage(Reset) { () -> (Void) in
            SearchHotel.reset()
            self.lblType.text = SelectPropertyType
            self.lblCountry.text = SelectCountry
            self.lblCity.text = SelectCity
            self.lblFrom.text = SelectStartDate
            self.lblTo.text = SelectEndDate
            self.lblRoomCount.text = SelectRoomNumber
            self.lblPersonsCount.text = SelectPersonsNumber
            self.lblChildrenCount.text = SelectChildrenNumber
            self.lblWCCount.text = SelectWCNumber
            NotificationCenter.default.post(name: .SearchHotel, object: nil, userInfo: ["type" : SearchHotel.type])
            self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func onToDateClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CalenderViewController") as! CalenderViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        vc.type = .Add
        vc.callback = { dateRange in
            self.lblFrom.text = self.formatter.string(from: dateRange.last!)
            self.lblTo.text = self.formatter.string(from: dateRange.first!)
            
            SearchHotel.startDateWithDateObj = dateRange.last!
            SearchHotel.endDateWithDateObj = dateRange.first!
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @objc func handleTypeTap() {
        dropDownType.anchorView = viewType
        dropDownType.dataSource = [Camp,Farm,Chalet]
        dropDownType.direction = .any
        dropDownType.width = viewType.frame.width
        dropDownType.bottomOffset = CGPoint(x: 0, y:(dropDownType.anchorView?.plainView.bounds.height)!)
        dropDownType.topOffset = CGPoint(x: 0, y:-(dropDownType.anchorView?.plainView.bounds.height)!)
        dropDownType.show()
        dropDownType.backgroundColor = .white
        dropDownType.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblType.text = item
            
        }
    }
    
    @objc func handleCountryTap() {
        
        var country = [String]()
        
        for item in self.country.enumerated(){
            country.append(item.element.country!)
        }
        
        dropDownCountry.anchorView = viewCountry
        dropDownCountry.dataSource = country
        dropDownCountry.direction = .any
        dropDownCountry.width = viewCountry.frame.width
        dropDownCountry.bottomOffset = CGPoint(x: 0, y:(dropDownCountry.anchorView?.plainView.bounds.height)!)
        dropDownCountry.topOffset = CGPoint(x: 0, y:-(dropDownCountry.anchorView?.plainView.bounds.height)!)
        dropDownCountry.show()
        dropDownCountry.backgroundColor = .white
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblCountry.text = item
            self.lblCity.text = SelectCity
            self.viewCity.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleCityTap)))
        }
    }
    
    @objc func handleCityTap() {
        
        let country = lblCountry.text!
        
        if let index = self.country.firstIndex(where: { (cityCountry) -> Bool in
            if let c = cityCountry.country{
                return c == country
            }
            return false
        }){
            let city = self.country[index].city
            
            dropDownCity.anchorView = viewCity
            dropDownCity.dataSource = city
            dropDownCity.direction = .any
            dropDownCity.width = viewCity.frame.width
            dropDownCity.bottomOffset = CGPoint(x: 0, y:(dropDownCity.anchorView?.plainView.bounds.height)!)
            dropDownCity.topOffset = CGPoint(x: 0, y:-(dropDownCity.anchorView?.plainView.bounds.height)!)
            dropDownCity.show()
            dropDownCity.backgroundColor = .white
            dropDownCity.selectionAction = { [unowned self] (index: Int, item: String) in
                self.lblCity.text = item
                
            }
        }
    }
    
    @objc func handleChildrenTap() {
        dropDownChild.anchorView = viewChildren
        dropDownChild.dataSource = [One, Two,Three,Four,Five]
        dropDownChild.direction = .any
        dropDownChild.width = viewChildren.frame.width
        dropDownChild.bottomOffset = CGPoint(x: 0, y:(dropDownChild.anchorView?.plainView.bounds.height)!)
        dropDownChild.topOffset = CGPoint(x: 0, y:-(dropDownChild.anchorView?.plainView.bounds.height)!)
        dropDownChild.show()
        dropDownChild.backgroundColor = .white
        dropDownChild.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblChildrenCount.text = item
        }
    }
    
    @objc func handleRoomTap() {
        dropDownRoom.anchorView = viewRoom
        dropDownRoom.dataSource = [One, Two,Three,Four,Five]
        dropDownRoom.direction = .any
        dropDownRoom.width = viewRoom.frame.width
        dropDownRoom.bottomOffset = CGPoint(x: 0, y:(dropDownRoom.anchorView?.plainView.bounds.height)!)
        dropDownRoom.topOffset = CGPoint(x: 0, y:-(dropDownRoom.anchorView?.plainView.bounds.height)!)
        dropDownRoom.show()
        dropDownRoom.backgroundColor = .white
        dropDownRoom.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblRoomCount.text = item
        }
    }
    
    @objc func handleWCTap() {
        dropDownWC.anchorView = viewWC
        dropDownWC.dataSource = [One, Two,Three,Four,Five]
        dropDownWC.direction = .any
        dropDownWC.width = viewWC.frame.width
        dropDownWC.bottomOffset = CGPoint(x: 0, y:(dropDownWC.anchorView?.plainView.bounds.height)!)
        dropDownWC.topOffset = CGPoint(x: 0, y:-(dropDownWC.anchorView?.plainView.bounds.height)!)
        dropDownWC.show()
        dropDownWC.backgroundColor = .white
        dropDownWC.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblWCCount.text = item
            
        }
    }
    
    @objc func handlePersonsTap() {
        dropDownPersons.anchorView = viewPersons
        dropDownPersons.dataSource = [One, Two,Three,Four,Five]
        dropDownPersons.direction = .any
        dropDownPersons.width = viewPersons.frame.width
        dropDownPersons.bottomOffset = CGPoint(x: 0, y:(dropDownPersons.anchorView?.plainView.bounds.height)!)
        dropDownPersons.topOffset = CGPoint(x: 0, y:-(dropDownPersons.anchorView?.plainView.bounds.height)!)
        dropDownPersons.show()
        dropDownPersons.backgroundColor = .white
        dropDownPersons.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblPersonsCount.text = item
            
        }
    }
    
    func setItemsInSearchHotel() {
        
        if self.lblType.text != SelectPropertyType{
            SearchHotel.type = self.lblType.text!
        }
        if self.lblCountry.text != SelectCountry{
            SearchHotel.country = self.lblCountry.text!
        }
        if self.lblCity.text != SelectCity{
            SearchHotel.city = self.lblCity.text!
        }else{
            SearchHotel.city = ""
        }
        if self.lblChildrenCount.text != SelectChildrenNumber{
            SearchHotel.child = self.lblChildrenCount.text!
        }
        if self.lblRoomCount.text != SelectRoomNumber{
            SearchHotel.room = self.lblRoomCount.text!
        }
        if self.lblWCCount.text != SelectWCNumber{
            SearchHotel.wc = self.lblWCCount.text!
        }
        if self.lblPersonsCount.text != SelectPersonsNumber{
            SearchHotel.person = self.lblPersonsCount.text!
        }
        
        if let startDate = SearchHotel.startDateWithDateObj{
            SearchHotel.startDate = "\(startDate.millisecondsSince1970)"
        }
        if let endDate = SearchHotel.endDateWithDateObj{
            SearchHotel.endDate = "\(endDate.millisecondsSince1970)"
        }
    }
    
    func getCitynCountry(){
        
        let wait = self.addWaitSpinner()
        
        JSONRequest.makeRequest(kGetCountries, parameters: [:]) { (data, error) in
            
            self.removeWaitSpinner(waitView: wait)
            
            if error == nil {
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        print("Error")
                    }else{
                        if let _ = data["message"] as? String{
                            if let arr = data["data"] as? [Any]{
                                for item in arr.enumerated(){
                                    if let rawData = item.element as? [String:Any]{
                                        let cityCountry = CityCountry(rawData: rawData)
                                        self.country.append(cityCountry)
                                        
                                        self.checkForCity()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func checkForCity() {
        self.lblCity.text = SelectCity
        if SearchHotel.city != "" {
            self.country.forEach { (cityCoun) in
                if cityCoun.city.contains(where: { (city) -> Bool in
                    return city == SearchHotel.city
                }){
                    self.lblCity.text = SearchHotel.city
                }
            }
        }else{
            self.lblCity.text = SelectCity
        }
    }
}

class CityCountry{
    
    var country:String?
    var city = [String]()

    init(rawData:[String:Any]){
        
        if let value = rawData["country"] as? String{
            self.country = value
        }
        
        if let arr = rawData["city"] as? [String]{
            for value in arr.enumerated(){
                self.city.append(value.element)
            }
        }
        
        
    }
    
}
