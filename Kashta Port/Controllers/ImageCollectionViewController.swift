//
//  ImageCollectionViewController.swift
//  Kashta Port
//
//  Created by mac on 23/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import ImageViewer

class ImageCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, GalleryItemsDataSource, GalleryItemsDelegate, UICollectionViewDelegateFlowLayout {
    
    var Images:[Image]?
    
    var callback:(([Image]?)->())!
    
    var dataImage: [GalleryItem] = [GalleryItem]()
    var galleryController:GalleryViewController?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = TImages
        self.setBackButton(withText: "")
        
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (self.view.frame.width-48) / 3
        return CGSize(width: size, height: size)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let images = self.Images{
            if !images.isEmpty {
                self.callback(images)
            }else{
                self.callback(nil)
            }
        }else{
            self.callback(nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Images!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        cell.imgViewImage.image = Images![indexPath.row].image
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageOptionViewController") as! ImageOptionViewController
        
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        
        vc.callback = { result in
            
            if let res = result {
                if res { // true == viewImage
                    self.showImagesPreview(index: indexPath.row)
                    
                }else{ // false == deleteImage
                    
                    let obj = self.Images![indexPath.row]
                    let url = obj.imgUrl
                    
                    deleteAwsFile(folderName: appDel.hotelImagesFolder, fullPath: url)
                    
                    self.Images?.remove(at: indexPath.row)
                    
                    collectionView.reloadData()
                    
                    if let images = self.Images {
                        if images.isEmpty{
                            self.navigationController?.popViewController(completion: {
                                self.callback(nil)
                            }, animation: true)
                        }
                    }
                }
            }
        }
        
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func itemCount() -> Int {
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    func showImagesPreview(index:Int){
        
        self.dataImage.removeAll()
        
        for image in self.Images!{
            let galleryItem = GalleryItem.image { imageCompletion in
                imageCompletion(image.image)//.image)
            }
            self.dataImage.append(galleryItem)
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
        //self.dismiss(animated: true, completion: nil)
    }

}
