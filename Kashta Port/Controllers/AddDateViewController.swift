//
//  AddDateViewController.swift
//  Kashta Port
//
//  Created by mac on 24/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class AddDateViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var tfNightRate: CustomTextField!
    @IBOutlet weak var dateview: UIView!
    
    var startDate:Date?
    var endDate:Date?
    
    var Dates = [HotelDate]()
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "E, MMM d"
        return formatter
    }()
    
    var callback:((HotelDate) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tfNightRate.delegate = self
        // Do any additional setup after loading the view.
        self.tfNightRate.keyboardType = .asciiCapableNumberPad
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            if touch.view == self.view{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("Hello")
    }
    
    @IBAction func onEndDateClick(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CalenderViewController") as! CalenderViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        vc.dates = self.Dates
        vc.callback = { dateRange in
            self.startDate = dateRange.first
            self.endDate = dateRange.last
            self.lblStartDate.text = self.formatter.string(from: dateRange.first!)
            self.lblEndDate.text = self.formatter.string(from: dateRange.last!)
            
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onAddClick(_ sender: Any) {
        if validate(){
            self.callback!(HotelDate(startDate: (startDate?.millisecondsSince1970)!, endDate: (endDate?.millisecondsSince1970)!, nRate: tfNightRate.text!))
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func validate() -> Bool{
        if startDate == nil{
            self.displayMessage(SelectStartDate)
            return false
        }
        
        if endDate == nil{
            self.displayMessage(SelectEndDate)
            return false
        }
        
        if let rate = self.tfNightRate.text{
            if Double(rate.trim()) == nil || rate.trim() == ""{
                    self.displayMessage(EnterNightRate)
                    return false
            }
        }
        
        return true
    }
}
