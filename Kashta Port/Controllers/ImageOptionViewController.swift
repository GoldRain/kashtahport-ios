//
//  ImageOptionViewController.swift
//  Kashta Port
//
//  Created by alienbrainz on 27/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class ImageOptionViewController: UIViewController {
    
    @IBOutlet weak var btnMainView:UIView!
    
    var callback:((Bool?)->())!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let view = touches.first?.view as? UIView {
            if view != self.btnMainView{
                self.dismiss(animated: true) {
                    self.callback(nil)
                }
            }
        }
    }
    
    @IBAction func onViewClick(_ sender: Any) {
        self.dismiss(animated: true) {
            self.callback(true)
        }
    }
    
    
    @IBAction func onDeleteClick(_ sender: Any) {
        self.dismiss(animated: true) {
            self.callback(false)
        }
    }
}
