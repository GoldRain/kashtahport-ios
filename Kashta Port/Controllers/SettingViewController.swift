//
//  SettingViewController.swift
//  Kashta Port
//
//  Created by mac on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import OneSignal

class SettingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var tableView: UITableView!
    //"Transaction History"
    var Settings = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupData()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setupData(){
        self.tableView.tableFooterView = UIView()
        Settings.removeAll()
        Settings.append(Profile)
        if MyProfile.canSale{
            Settings.append(ManageProperty)
            Settings.append(BookingRequest)
            Settings.append(IBANInformation)
        }
        if MyProfile.isLogin {
            Settings.append(TransactionHistory)
            Settings.append(Notifications)
        }
        Settings.append(Languages)
        Settings.append(AboutUs)
        Settings.append(PrivacyPolicy)
        Settings.append(TermsandConditions)
        Settings.append(ContactUs)
        
        if MyProfile.isLogin{
            Settings.append(Logout)
        }else{
            Settings.append(Login)
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Settings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell", for: indexPath) as! SettingTableViewCell
        cell.lblSettingName.text = Settings[indexPath.row]
        cell.selectionStyle = .none
        if Settings[indexPath.row] == Notifications{
            cell.onOffSwitch.isHidden = false
        }else{
            cell.onOffSwitch.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let wait = self.addWaitSpinner()

        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            self.removeWaitSpinner(waitView: wait)
        }
        
        switch Settings[indexPath.row] {
        case Profile:
            TabBarVC?.selectedIndex = 3
        case ManageProperty:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagePropertyViewController") as! ManagePropertyViewController
            self.navigationController?.pushViewController(vc , animated: true)
        case BookingRequest:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingsViewController") as! BookingsViewController
            self.navigationController?.pushViewController(vc , animated: true)
        case TransactionHistory:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionHistoryViewController") as? TransactionHistoryViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        case Notifications:
            print("notification")
        case Languages:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "LanguageViewController") as? LanguageViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case IBANInformation:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "IBANInfoViewController") as? IBANInfoViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case AboutUs:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
            self.navigationController?.pushViewController(vc! , animated: true)
        case PrivacyPolicy:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as? PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc! , animated: true)
        case TermsandConditions:
            let vc = UIStoryboard(name: "AddProperty", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController
            self.navigationController?.pushViewController(vc! , animated: true)
        case Logout:
            if MyProfile.isLogin{
                let optionMenu = getAlertController(title: nil, message: AlertLogout)
                
                let logoutAction = UIAlertAction(title: Logout, style: .destructive) { (action) in
                    let wait = self.addWaitSpinner()
                    MessageCenter.logout() {
                        self.removeWaitSpinner(waitView: wait)
                        MyProfile.logOutUser()
                        MessageCenter.logOutFacebook()
                        self.setupData()
                        NotificationCenter.default.post(name: .UserLoginLogout, object: nil, userInfo: nil)
                    }
                }
                
                let cancelAction = UIAlertAction(title: Cancel, style: .cancel, handler: nil)
                
                optionMenu.addAction(logoutAction)
                optionMenu.addAction(cancelAction)
                
                appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
            }
        case Login:
            
            let vc = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            vc.type = .Other
            let nvc = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nvc, animated: true, completion: nil)
        case ContactUs:
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            break;
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
}
