//
//  LanguageViewController.swift
//  kashtahPORT
//
//  Created by mac on 18/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController {

    @IBOutlet weak var viewEnglish: UIView!
    @IBOutlet weak var viewArabic: UIView!
    @IBOutlet weak var btnEng:UIButton!
    @IBOutlet weak var btnArb:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = Languages
        setBackButton(withText: "")
        viewEnglish.addShadow()
        viewArabic.addShadow()
        
        if appDel.currentAppleLanguage() == "ar"{
            self.btnEng.setImage(UIImage(named: "verified (5)"), for: .normal)
            self.btnArb.setImage(UIImage(named: "verified (5)"), for: .normal)
            self.btnArb.setImage(UIImage(named: "verified (2)"), for: .normal)
        }else{
            self.btnEng.setImage(UIImage(named: "verified (5)"), for: .normal)
            self.btnArb.setImage(UIImage(named: "verified (5)"), for: .normal)
            self.btnEng.setImage(UIImage(named: "verified (2)"), for: .normal)
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onEnglishClick(_ sender: UIButton) {
        self.changeLanguage(sender: sender) { flag in
            if flag{
                appDel.setAppleLAnguageTo(lang: "en")
            }
        }
    }
    
    @IBAction func onArabicClick(_ sender: UIButton) {
        
        self.changeLanguage(sender: sender) { flag in
            if flag{
                appDel.setAppleLAnguageTo(lang: "ar")
            }
        }
    }
    
    func changeLanguage(sender:UIButton,callback:@escaping ((Bool)->())){
        
        if sender.image(for: .normal) == UIImage(named: "verified (5)") {
            let optionMenu = appDel.topViewController!.getAlertController(title: ChangeLanguage, message: AlertLanguage)
            
            let logoutAction = UIAlertAction(title: Yes, style: .destructive) { (action) in
                
                let message = "In order to change the language, the App must be closed and reopened by you."
                let confirmAlertCtrl = UIAlertController(title: "App restart required", message: message, preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "Close now", style: .destructive) { _ in
                    callback(true)
                    self.btnEng.setImage(UIImage(named: "verified (5)"), for: .normal)
                    self.btnArb.setImage(UIImage(named: "verified (5)"), for: .normal)
                    sender.setImage(UIImage(named: "verified (2)"), for: .normal)
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                        exit(EXIT_SUCCESS)
                    })
                }
                confirmAlertCtrl.addAction(confirmAction)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                confirmAlertCtrl.addAction(cancelAction)
                
                self.present(confirmAlertCtrl, animated: true, completion: nil)
            }
            let cancelAction = UIAlertAction(title: No, style: .cancel) { (_) in
                callback(false)
            }
            
            optionMenu.addAction(logoutAction)
            optionMenu.addAction(cancelAction)
            
            appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
        }
    }
    
}
