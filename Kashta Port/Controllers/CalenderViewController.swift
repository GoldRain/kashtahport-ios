//
//  CalenderViewController.swift
//  Kashta Port
//
//  Created by mac on 24/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import FSCalendar

class CalenderViewController: UIViewController,FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance {
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var btnAdd: UIButton!
    
    var dates = [HotelDate]()
    
    var allDate = [CalendarDate]()

    private var datesRange: [Date]?
    
    var callback:(([Date]) -> ())!
    
    private var firstDate: Date?
    private var lastDate: Date?
    
    var rate = 0
    
    var type:CalenderType = .Add
    
    let c = NSCalendar(calendarIdentifier: .gregorian)
    
    var currency = "$ "
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter
    }()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            if touch.view == self.view{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        self.calendar.dataSource = self
        self.calendar.delegate = self
        calendar.placeholderType = .none
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        firstDate = nil
        lastDate = nil
        setupData()
        datesRange = nil
    }
    
    func setupData(){
        if !dates.isEmpty{
            for item in dates{
            
                var date = Date(milliseconds: item.startDate) // first date
                let endDate = Date(milliseconds: item.endDate) // last date
                
                while date.compare(endDate) != .orderedDescending {
                    print(date)
                    print(date.millisecondsSince1970)
                    self.allDate.append(CalendarDate(date: date, rate: item.nightRate))
                    date = c!.date(byAdding: .day, value: 1, to: date, options: [])!
                }
            }
        }
    }
    
    @IBAction func onAddClick(_ sender: Any) {
        if firstDate != nil && lastDate != nil{
            self.callback(datesRange!)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func isDateSeries(date:Date,callback:@escaping ((Date,Date)->())) {
   
        for item in dates{
            
            var startDate = Date(milliseconds: item.startDate) // first date
            let endDate = Date(milliseconds: item.endDate) // last date
            
            var newDate = startDate
            
            while newDate.compare(endDate) != .orderedDescending {
                if newDate == date{
                    if startDate < calendar.today!{ startDate = calendar.today! }
                    print("\(startDate) and \(endDate)")
                    callback(startDate, endDate)
                    return
                }
                newDate = c!.date(byAdding: .day, value: 1, to: newDate, options: [])!
            }
        }
    }
    
    // MARK:- FSCalendarDelegate
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        
        if type == .Book{
            
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }

            self.btnAdd.setTitle(Add, for: .normal)
            
            if !allDate.contains(where: { (CalendarDate) -> Bool in return CalendarDate.date! == date }){
                calendar.deselect(date)
                return
            }
            
            self.isDateSeries(date: date) { (startDate, endDate) in
                
                var start = startDate
                
                if let startD = self.datesRange?.first{
                    start = startD
                }
                
                let range = self.datesRange(from: start, to: endDate)
                
                self.lastDate = range.last
                
                self.datesRange = []
                
                for d in range {
                    if self.allDate.contains(where: { (dates) -> Bool in dates.date! == d }){
                        calendar.select(d)
                        self.datesRange?.append(d)
                    }else{
                        break
                    }
                }
                
                if self.datesRange?.count != 0 {
                    self.btnAdd.setTitle("\((self.datesRange?.count)!) \(NightSelected)", for: .normal)
                }else{
                    self.btnAdd.setTitle(Add, for: .normal)
                }
                
                
                self.firstDate = self.datesRange!.first
                self.lastDate = self.datesRange!.last
                print("datesRange contains 3 : \(self.datesRange!)")
                
                return
            }
    
        }else{
            // nothing selected:
            if firstDate == nil {
//                if type == .Book{
//                    if !allDate.contains(where: { (CalendarDate) -> Bool in return CalendarDate.date! == date }){
//                        calendar.deselect(date)
//                        return
//                    }
//                }
                firstDate = date
                datesRange = [firstDate!]
                print("datesRange contains 0: \(datesRange!)")
                return
            }
            
            // only first date is selected:
            if firstDate != nil && lastDate == nil {
                // handle the case of if the last date is less than the first date:
                if date == firstDate! {
                    calendar.deselect(firstDate!)
                    firstDate = nil
                    datesRange = []
                    print("datesRange contains 1: \(datesRange!)")
                    return
                }
                
//                if type == .Book {
//                    if date < firstDate!{
//                        if !allDate.contains(where: { (CalendarDate) -> Bool in return CalendarDate.date == date }){
//                            calendar.deselect(date)
//                            return
//                        }
//                    }
//                }
                
                
                if date <= firstDate! {
                    calendar.deselect(firstDate!)
                    firstDate = date
                    datesRange = [firstDate!]
                    print("datesRange contains 2: \(datesRange!)")
                    return
                }
                
                let range = datesRange(from: firstDate!, to: date)
                
                lastDate = range.last
                
                datesRange = []
                
                for d in range {
//                    if type == .Book{
//                        if allDate.contains(where: { (dates) -> Bool in dates.date! == d }){
//                            calendar.select(d)
//                            datesRange?.append(d)
//                        }else{
//                            calendar.deselect(lastDate!)
//                            calendar.select(d)
//                            datesRange?.append(d)
//                            break
//                        }
//                    }else{
                        if self.allDate.contains(where: { (CalendarDate) -> Bool in return CalendarDate.date! == d }){
                            calendar.deselect(lastDate!)
                            break
                        }else{
                            calendar.select(d)
                            datesRange?.append(d)
                        }
                        
//                    }
                }
                
//                if type == .Book {
//                    self.btnAdd.setTitle("\((datesRange?.count)! - 1) \(NightSelected)", for: .normal)
//                }
                
                
                lastDate = datesRange!.last
                print("datesRange contains 3 : \(datesRange!)")
                
                return
            }
            
            // both are selected:
            if firstDate != nil && lastDate != nil {
                for d in calendar.selectedDates {
                    calendar.deselect(d)
                }
                
                lastDate = nil
                firstDate = nil
                
                datesRange = []
                self.btnAdd.setTitle(Add, for: .normal)
                print("datesRange contains: \(datesRange!)")
            }
        }
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        if type == .Book{
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            self.btnAdd.setTitle(Add, for: .normal)
            print("datesRange contains: \(datesRange!)")
            
        }else{
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            self.btnAdd.setTitle(Add, for: .normal)
            print("datesRange contains: \(datesRange!)")
        }
    }
    
    func datesRange(from: Date, to: Date) -> [Date] {
        
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    
    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
        return configureCell(date: date)?.subTitle
    }

    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return configureCell(date: date)!.isEnabled!
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor?{
        return configureCell(date: date)?.titleColor
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, subtitleDefaultColorFor date: Date) -> UIColor? {
        return configureCell(date: date)?.subColor
    }
    
    func configureCell(date:Date) -> Cell?{
        
            if type == .Book {
                print("calender date \(date) and in mili \(date.millisecondsSince1970)")
                if let avdate = allDate.first(where: { (dates) -> Bool in
                
                    return dates.date!.millisecondsSince1970 == date.millisecondsSince1970
                    
                }){
                    if calendar.today! <= date{
                        
                        return Cell(titleColor: UIColor.black, subColor: APP_PRIMARY_COLOR, subTitle: "\(self.currency) \(avdate.rate!)", enabled: true)
                    }
                }else{
                    let c = NSCalendar(calendarIdentifier: .gregorian)
                    let lastdate = c!.date(byAdding: .day, value: -1, to: date, options: [])!
                    if allDate.contains(where: { (CalendarDate) -> Bool in return CalendarDate.date! == lastdate }){
                        return Cell(titleColor: UIColor.black, subColor: UIColor.red, subTitle: "Closed", enabled: true)
                    }
                }
                return Cell(titleColor: UIColor.gray, subColor: nil, subTitle: nil, enabled: false)
            }
            else{
                if calendar.today! <= date{
                    if allDate.contains(where: { (CalendarDate) -> Bool in  return CalendarDate.date! == date }){
                        return Cell(titleColor: UIColor.gray, subColor: UIColor.gray, subTitle: nil, enabled: false)
                    }
                    return Cell(titleColor: UIColor.black, subColor: UIColor.gray, subTitle: nil, enabled: true)
                }
                return Cell(titleColor: UIColor.gray, subColor: UIColor.gray, subTitle: nil, enabled: false)
            }
        }
    
    func countprice() -> Int{
        self.rate = 0
        datesRange?.popLast()
        for item in datesRange!.enumerated(){
            if let date = self.allDate.first(where: { (CalendarDate) -> Bool in return CalendarDate.date! == item.element }), date != nil{
                self.rate += Int(date.rate!)!
            }
        }
        return rate
    }
}

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
    
    func convertToLocalTime(fromTimeZone timeZoneAbbreviation: String) -> Date? {
        if let timeZone = TimeZone(abbreviation: timeZoneAbbreviation) {
            let targetOffset = TimeInterval(timeZone.secondsFromGMT(for: self))
            let localOffeset = TimeInterval(TimeZone.autoupdatingCurrent.secondsFromGMT(for: self))
            print("\(targetOffset)   and \(localOffeset)")
            return self.addingTimeInterval(targetOffset - localOffeset)
        }
        
        return nil
    }
    
    
}

enum CalenderType{
    case Book
    case Add
}

class CalendarDate {
    var date:Date?
    var rate:String?
    
    init(date:Date,rate:String) {
        self.date = date
        self.rate = rate
    }
}

class Cell {
    
    var titleColor:UIColor?
    var subColor:UIColor?
    var subTitle:String?
    var isEnabled:Bool?

    init(titleColor:UIColor?,subColor:UIColor?,subTitle:String?,enabled:Bool?) {
        self.titleColor = titleColor
        self.subColor  = subColor
        self.subTitle = subTitle
        self.isEnabled = enabled
    }
    
}
