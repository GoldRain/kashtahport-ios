//
//  DescriptionViewController.swift
//  kashtahPORT
//
//  Created by mac on 11/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class DescriptionViewController: UIViewController {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var textArea: UITextView!
    
    var descriptionText:String?
    @IBOutlet weak var titleText:UILabel!
    var tiltetext:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleText.text = tiltetext!
        if descriptionText != nil {
            self.textArea.text = descriptionText!
        }
        
        // Do any additional setup after loading the view.
    }

    @IBAction func onCloseClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
