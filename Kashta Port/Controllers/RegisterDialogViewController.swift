//
//  RegisterDialogViewController.swift
//  Kashta Port
//
//  Created by mac on 22/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class RegisterDialogViewController: UIViewController {

    @IBOutlet weak var registerView:UIView!
    @IBOutlet weak var lblLoginText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblLoginText.text = LoginText
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            if touch.view == self.view{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onRegisterClick(_ sender:UIButton) {
        let vc = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        vc.type = .Other
        let nvc = UINavigationController(rootViewController: vc)
        self.dismiss(animated: true) {
            appDel.topViewController?.navigationController?.present(nvc, animated: true, completion: nil)
        }
    }
}
