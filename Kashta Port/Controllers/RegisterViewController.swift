//
//  RegisterViewController.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import CountryPickerView

var preferredCountry = [Country(name: "Bahrain", code: "BH", phoneCode: "+973"),Country(name: "Cyprus", code: "CY", phoneCode: "+357"),Country(name: "Egypt", code: "EG", phoneCode: "+20"),Country(name: "Iran", code: "IR", phoneCode: "+98"),Country(name: "Iraq", code: "IQ", phoneCode: "+964"),Country(name: "Israel", code: "IL", phoneCode: "+972"),Country(name: "Jordan", code: "JO", phoneCode: "+962"),Country(name: "Kuwait", code: "KW", phoneCode: "+965"),Country(name: "Lebanon", code: "LB", phoneCode: "+961"),Country(name: "Oman", code: "OM", phoneCode: "+968"),Country(name: "Palestine", code: "PS", phoneCode: "+970"),Country(name: "Qatar", code: "QA", phoneCode: "+974"),Country(name: "Saudi Arabia", code: "SA", phoneCode: "+966"),Country(name: "Syria", code: "SY", phoneCode: "+963"),Country(name: "Turkey", code: "TR", phoneCode: "+90"),Country(name: "United Arab Emirates", code: "AE", phoneCode: "+971"),Country(name: "Yemen", code: "YE", phoneCode: "+967")]

class RegisterViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate,CountryPickerViewDataSource,CountryPickerViewDelegate{
    

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var tfName: CustomTextField!
    @IBOutlet weak var tfPhoneNumber: CustomTextField!
    @IBOutlet weak var tfEmail: CustomTextField!
    @IBOutlet weak var tfPassword: CustomTextField!
    @IBOutlet weak var tfConfirmPassword: CustomTextField!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var btnAccpet: UIButton!
    @IBOutlet weak var countryView: CountryPickerView!
    
    @IBOutlet weak var lblTermsAndCondition: UILabel!
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = Register
        
        self.setBackButton(withText: "")

        self.countryView.delegate = self
        self.countryView.dataSource = self
        
        self.countryView.setCountryByCode("KW")
        
        self.tfPhoneNumber.keyboardType = .asciiCapableNumberPad
        
        let attriButtedStr1 = NSMutableAttributedString(string: NSLocalizedString("By Clicking this button you agree our ", comment: "By Clicking this button you agree our "), attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor : UIColor(red:0.29, green:0.33, blue:0.38, alpha:1.0)])
        
        let attriButtedStr2 = NSMutableAttributedString(string: NSLocalizedString("terms & conditions", comment: "terms & conditions"), attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor : UIColor(red:0.01, green:0.66, blue:0.96, alpha:1.0)])
        
        let combineStr = NSMutableAttributedString()
        combineStr.append(attriButtedStr1)
        combineStr.append(attriButtedStr2)
        
        self.lblTermsAndCondition.attributedText = combineStr
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK:- IBActions
    
    @IBAction func onProfileImageAddClick(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectImageViewController") as? SelectImageViewController {
            
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            
            vc.callback = { img in
                if let image = img {
                    self.imgProfile.image = image
                    self.selectedImage = image
                    self.imgProfile.contentMode = .scaleAspectFill
                    self.imgProfile.clipsToBounds = true
                }
            }
            
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK:- CountryPickerView
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print("\(country.code)  == \(country.phoneCode)")
    }
    
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country]{
        return preferredCountry
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String?{
        return "Countrys"
    }
    
    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool{
        return true
    }
    //
    
    
    var selectedImage:UIImage!
    
    var fromScreen = ""
    
    @IBAction func onRegisterClick(_ sender: Any) {
        if validate(){
            self.checkEmailAndPhoneNumberThenRegister()
        }
    }
    
    func checkEmailAndPhoneNumberThenRegister() {
        
        let params = ["email":self.tfEmail.text!,
                      "phone":self.tfPhoneNumber.text!]
        
        let wait = self.addWaitSpinner()
        
        JSONRequest.makeRequest(kCheckEmailAndPhone, parameters: params) { (data, error) in
            
            self.removeWaitSpinner(waitView: wait)
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    DispatchQueue.main.async {
                        self.registerUser()
                    }
                }else if let msg = data["message"] as? String{
                    self.displayMessage(msg)
                }
            }
        }
    }
    
    @IBAction func onAcceptTermsAndConditionClick(_ sender:UIButton) {
        if sender.image(for: .normal) == UIImage(named:"success (4)") {
            sender.setImage(#imageLiteral(resourceName: "success (3)") , for: .normal)
        }else{
            sender.setImage(UIImage(named:"success (4)") , for: .normal)
        }
    }
    
    @IBAction func onTermsAndConditionClick(_ sender:UIButton) {
        let vc = UIStoryboard(name: "AddProperty", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController
        self.navigationController?.pushViewController(vc! , animated: true)
    }
    
    func registerUser() {
        
        let regParmas = [
            "name" : tfName.text!,
            "email": tfEmail.text!,
            "country_code": self.countryView.selectedCountry.phoneCode.replacingOccurrences(of: "+", with: ""),
            "phone": tfPhoneNumber.text!,
            "type": "Seller",
            "password": tfPassword.text!,
            "profile_image_url": ""
        ]
        
        let parmas = ["phone":tfPhoneNumber.text!,
                      "country_code":self.countryView.selectedCountry.phoneCode.replacingOccurrences(of: "+", with: "")]
        
        let wait = self.addWaitSpinner()
        
        JSONRequest.makeRequest(kGetOTP, parameters: parmas) { (data, error) in
            
            self.removeWaitSpinner(waitView: wait)
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    print("Error")
                }else{
                    if let _ = data["message"] as? String{
                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPViewController") as? VerifyOTPViewController{
                            vc.phoneNumber = self.tfPhoneNumber.text!
                            vc.countryCode = self.countryView.selectedCountry.phoneCode
                            if let image = self.selectedImage {
                                vc.profileImage = image
                            }
                            vc.registerParams = regParmas
                            vc.fromScreen = self.fromScreen
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onLoginClick(_ sender: Any) {
        
        if fromScreen == "Booking" {
            
            self.navigationController?.popViewController(completion: {
                
                let vc = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                vc.type = .Other
                let nvc = UINavigationController(rootViewController: vc)
                self.navigationController?.present(nvc, animated: true, completion: nil)
                
            }, animation: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func validate() -> Bool{
        
        if let name = tfName.text,name.trim() == ""{
            self.displayMessage(EnterName)
            return false
        }
        
        if let phone = tfPhoneNumber.text,phone.trim() == ""{
            self.displayMessage(EnterNumber)
            return false
        }
        
        if !isValidMobileNumber(tfPhoneNumber.text){
            self.displayMessage(ValidPhone)
            return false
        }
        
        if let email = tfEmail.text, email.trim() == ""{
            self.displayMessage(EnterEmail)
            return false
        }
        
        if !isValidEmailId(tfEmail.text){
            self.displayMessage(ValidEmail)
            return false
        }
        
        if let password = tfPassword.text,password.trim() == ""{
            self.displayMessage(EnterPass)
            return false
        }
        
        if let password = tfConfirmPassword.text,password.trim() == ""{
            self.displayMessage(EnterConPass)
            return false
        }
        
        if tfPassword.text != tfConfirmPassword.text{
            self.displayMessage(MissMatchPassword)
            return false
        }
        
        if btnAccpet.image(for: .normal) == UIImage(named: "success (4)") {
            self.displayMessage(SelectTerms)
            return false
        }
        
        return true
    }
    
    private func clearFields(){
        tfName.text = ""
        tfPhoneNumber.text = ""
        tfEmail.text = ""
        tfPassword.text = ""
        tfConfirmPassword.text = ""
    }
}

