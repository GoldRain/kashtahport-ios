//
//  NotificationViewController.swift
//  Kashta Port
//
//  Created by mac on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import DropDown

class NotificationViewController: UIViewController {
    
    let BookingNotificationCellId = "BookingNotification"
    let FavouriteNotificationCellId = "FavouriteNotification"
    let FeedbackNotificationCellId = "FeedbackNotification"
    
    var notifications = [NotificationModel]()
    
    var emptyText = Loading
    
    let dropDownSort = DropDown()
    
    var workItem:DispatchWorkItem?
    
    var refreshControl:UIRefreshControl!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var lblSortType: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        setupData()
        
        self.getNotificationsFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        workItem?.cancel()
    }
    
    let backgroundView = UIView()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if !MyProfile.isLogin {
            // add loginView
            addLoginView(backgroundMainView: self.backgroundView, view: self.view, owner: self)
            self.notifications.removeAll()
            self.tableView.reloadData()
        }else{
            removeLoginView(view: self.view)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    @IBAction func onClearAllClick(_ sender: Any) {
        self.deleteAllActivity()
    }
    
    func setupData(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let attrStr = NSAttributedString(string: Refreshing)
        
        refreshControl = getRefreshControl(attrStr: attrStr)
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        self.navigationController?.isNavigationBarHidden = true
        
        self.sortView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleRoomTap)))
    }
    
    @objc func handleRoomTap() {
        dropDownSort.anchorView = sortView
        dropDownSort.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.96, alpha:1.0)
        dropDownSort.dataSource = [DateStr]
        dropDownSort.direction = .any
        dropDownSort.width = sortView.frame.width
        dropDownSort.bottomOffset = CGPoint(x: 0, y:(dropDownSort.anchorView?.plainView.bounds.height)!)
        dropDownSort.topOffset = CGPoint(x: 0, y:-(dropDownSort.anchorView?.plainView.bounds.height)!)
        dropDownSort.show()
//        dropDownSort.backgroundColor = .white
        dropDownSort.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblSortType.text = item
        }
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getNotificationsFromServer()
    }
    
    func getNotificationsFromServer() {
        
        guard let userId = MyProfile.userId else{
            self.notifications.removeAll()
            self.tableView.reloadData()
            return;
        }
        
        let params = ["user_id":userId]
        
        self.tableView.addWaitView()
        
        
        workItem = DispatchWorkItem{
            JSONRequest.makeRequest(kGetActivity, parameters: params) { (data, error) in
                
                self.emptyText = NotificationNotFound
                
                self.tableView.removeWaitView()
                if error == nil {
                    if let data = data as? [String:Any]{
                        if let err = data["error"] as? Bool, !err {
                            if let notifications = data["data"] as? [[Any]] {
                                self.notifications.removeAll()
                                
                                for not in notifications{
                                    if let notificationData = not.first as? [String:Any]{
                                        self.notifications.append(NotificationModel(data: notificationData))
                                    }
                                }
                                
                                self.notifications.sort(by: { (first, second) -> Bool in
                                    return Int(first.created_at)! > Int(second.created_at)!
                                })
                            }
                        }
                    }
                    self.tableView.reloadData()
                    self.allRead()
                }else{
                    self.tableView.reloadData()
                }
            }
        }
        
         DispatchQueue.main.asyncAfter(deadline: .now(), execute: workItem!)

    }
    
    func deleteSingleActivity(id:String){
        
        guard let userId = MyProfile.userId else {
            return
        }
        
        let params = ["user_id":userId,
                      "activity_id":id]
        
        JSONRequest.makeRequest(kDeleteSingleActivity, parameters: params) { (data, error) in
            if error == nil{
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool,err{
                        if let msg = data["message"] as? String{
                            print(msg)
                        }
                    }else{
                        
                    }
                }
            }else{
                print(Error)
            }
        }
    }
    
    
    
    func deleteAllActivity(){
        
        guard let userId = MyProfile.userId else {
            return
        }
        
        let params = ["user_id":userId]
        
        JSONRequest.makeRequest(kDeleteAllActivity, parameters: params) { (data, error) in
            if error == nil{
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool,err{
                        if let msg = data["message"] as? String{
                            print(msg)
                        }
                    }else{
                        self.notifications.removeAll()
                        self.tableView.reloadData()
                    }
                }
            }else{
                print(Error)
            }
        }
    }
    
    func allRead(){
        var arr = [String]()
        for item in notifications{
            arr.append(item.id)
        }
        readActivitys = arr
//        badge = 0
        MyProfile.badge = 0
        NotificationCenter.default.post(name: .UpdateBadge, object: nil)
    }
    
}


var readActivitys: [String]? {
    set{
        let preferences = UserDefaults.standard
        preferences.set(newValue, forKey: "read")
        preferences.synchronize()
    }
    
    get{
        return UserDefaults.standard.value(forKey: "read") as? [String]
    }
}

//MARK:- TableView DataSource Methods

extension NotificationViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count > 0 ? self.notifications.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.notifications.isEmpty {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = self.emptyText
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
        let notification = self.notifications[indexPath.row]
        
        if notification.hint == "Book_Buyer" || notification.hint == "Book_Seller" || notification.hint == "Approved" {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: self.BookingNotificationCellId, for: indexPath) as! BookingNotificationTVC
            cell.bookingNotification = notification
            cell.selectionStyle = .none
            return cell
            
        }else if notification.hint == "Feedback_Buyer" || notification.hint == "Feedback_Seller" {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: self.FeedbackNotificationCellId, for: indexPath) as! FeedbackNotificationTVC
            cell.bookingNotification = notification
            cell.selectionStyle = .none
            return cell
            
        }else { // Like_Seller
            
            let cell = tableView.dequeueReusableCell(withIdentifier: self.FavouriteNotificationCellId, for: indexPath) as! FavouriteNotificationTVC
            cell.bookingNotification = notification
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if self.notifications.count != 0 {
            return true
        }else{
            return false
        }
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if self.notifications.count > indexPath.row{
            let activityId = self.notifications[indexPath.row].id
            if activityId != ""{
                self.deleteSingleActivity(id: activityId)
                if self.notifications.count >= 2 {
                    self.notifications.remove(at: indexPath.row)
                    self.tableView.beginUpdates()
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                    self.tableView.endUpdates()
                }else{
                    self.notifications.remove(at: indexPath.row)
                    self.tableView.reloadData()
                }
                
            }
        }

    }
    
}

//MARK:- TableView Delegate Methods

extension NotificationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.notifications.isEmpty {
            return;
        }
        
        let notification = self.notifications[indexPath.row]
        
        if notification.hint.contains("Book") || notification.hint.contains("Feedback") {
            
            var bookingId:String = ""
            
            if notification.hint.contains("Book") {
                bookingId = notification.hint_id
            }else if notification.hint.contains("Feedback") {
                bookingId = notification.bookingId
            }
         
            let params = ["booking_id":bookingId]
            
            self.tableView.addWaitView()
            
            JSONRequest.makeRequest(kSingleBookingRequest, parameters: params) { (data, error) in
                
                self.tableView.removeWaitView()
                
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, !err {
                        if let arr = data["data"] as? [Any]{
                            if let rawData = arr.first as? [String:Any]{
                                let booking = BookingModel(data: rawData)
                                if let isDeleted = booking.hotel?.isDeleted, !isDeleted {
                                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelDetailsViewController") as? HotelDetailsViewController {
                                        vc.bookings = booking
                                        vc.property = booking.hotel
                                        if notification.hint == "Book_Buyer" || notification.hint == "Feedback_Buyer" {
                                            vc.userType = .Buyer
                                        }else if notification.hint == "Book_Seller" || notification.hint == "Feedback_Seller"{
                                            vc.userType = .Seller
                                        }
                                        vc.fromVC = .OtherVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }else{
                                    self.displayMessage(AlertNotFound)
                                }
                            }
                        }
                    }else if let _ = data["message"] as? String{
                        self.displayMessage(AlertNotFound)
                    }
                }
            }
        }else if notification.hint == "Approved"{
            if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ManagePropertyViewController") as? ManagePropertyViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
//        if notification.hint == "Book"{
//
//            let wait = self.addWaitSpinner()
//
//            let params = ["booking_id":notification.hint_id]
//
//            JSONRequest.makeRequest(kSingleBookingRequest, parameters: params) { (data, error) in
//
//                self.removeWaitSpinner(waitView: wait)
//
//                if let data = data as? [String:Any]{
//                    if let err = data["error"] as? Bool, !err {
//                        if let arr = data["data"] as? [Any]{
//                            if let rawData = arr.first as? [String:Any]{
//                                let booking = BookingModel(data: rawData)
//                                if let isDeleted = booking.hotel?.isDeleted, !isDeleted {
//                                    if notification.message == "You have got a new booking" || notification.message == "You have rejected a booking" || notification.message == "The booking has been cancelled" || notification.message == "You have confirmed a booking"{
//                                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelDetailsViewController") as? HotelDetailsViewController {
//                                            vc.bookings = booking
//                                            vc.property = booking.hotel
//                                            vc.userType = .Seller
//                                            vc.fromVC = .OtherVC
//                                            self.navigationController?.pushViewController(vc, animated: true)
//                                        }
//                                    }else{
//                                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelDetailsViewController") as? HotelDetailsViewController {
//                                            vc.bookings = booking
//                                            vc.property = booking.hotel
//                                            vc.userType = .Buyer
//                                            vc.fromVC = .OtherVC
//                                            self.navigationController?.pushViewController(vc, animated: true)
//                                        }
//                                    }
//                                }else{
//                                    self.displayMessage(AlertNotFound)
//                                }
//                            }
//                        }
//                    }else if let _ = data["message"] as? String{
//                        self.displayMessage(AlertNotFound)
//                    }
//                }
//            }
//        }
//        else if notification.hint == "Feedback" {
//            if notification.message == "You gives feedback to a property"{
//
//            }else{
//
//                let params = ["booking_id":notification.bookingId]
//
//                JSONRequest.makeRequest(kSingleBookingRequest, parameters: params) { (data, error) in
//
//                    self.tableView.removeWaitView()
//
//                    if let data = data as? [String:Any]{
//                        if let err = data["error"] as? Bool, !err {
//                            if let arr = data["data"] as? [Any]{
//                                if let rawData = arr.first as? [String:Any]{
//                                    let booking = BookingModel(data: rawData)
//                                    if let isDeleted = booking.hotel?.isDeleted, !isDeleted {
//                                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelDetailsViewController") as? HotelDetailsViewController {
//                                            vc.userType = .Seller
//                                            vc.fromVC =  .OtherVC
//                                            vc.bookings = booking
//                                            vc.property = booking.hotel
//                                            self.navigationController?.pushViewController(vc, animated: true)
//                                        }
//                                    }else{
//                                        self.displayMessage(AlertNotFound)
//                                    }
//                                }
//                            }
//                        }else if let _ = data["message"] as? String{
//                            self.displayMessage(AlertNotFound)
//                        }
//                    }
//                }
//            }
//        }
//        else if notification.hint == "Approved"{
////            notification.message == "Your hotel is approved by admin"
//
//            if notification.message == "Your property is approved by admin" {
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelDetailsViewController") as! HotelDetailsViewController
//                vc.property = notification.hotelData
//                self.navigationController?.pushViewController(vc, animated: true)
//            }else{
//
//            }
//        }
    }
}
