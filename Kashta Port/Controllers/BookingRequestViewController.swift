//
//  BookingViewController.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class BookingRequestViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    var Bookings = [BookingModel]()

    var emptyText = Loading
    
    var type:Type = .Request
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.getBookingList()
    }
    
    func setupData(){
        self.navigationController?.isNavigationBarHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCell(notification:)), name: .BookingRequestStatusUpdate, object: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Bookings.count != 0{
            return Bookings.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if Bookings.isEmpty{
            let cell = UITableViewCell()
            cell.textLabel?.text = emptyText
            cell.textLabel?.textAlignment = .center
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTableViewCell", for: indexPath) as! BookingTableViewCell
            cell.bookRequest = Bookings[indexPath.row]
            cell.backgroundColor = UIColor.white
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.Bookings.isEmpty{
            return;
        }
        
        if let isDeleted = self.Bookings[indexPath.row].hotel?.isDeleted, !isDeleted{
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelDetailsViewController") as? HotelDetailsViewController {
                vc.bookings = Bookings[indexPath.row]
                vc.property = Bookings[indexPath.row].hotel
                vc.fromVC = .OtherVC
                vc.userType = .Seller
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            self.displayMessage(AlertNotFound)
        }
    }
    
    @objc func getBookingList(){
        
        guard MyProfile.isLogin else {
            tableView.reloadData()
            return
        }
        
        let params = ["user_id":MyProfile.userId!]
        
        self.tableView.addWaitView()
        
        JSONRequest.makeRequest(kBookingRequest, parameters: params) { (data, error) in
            self.emptyText = BookingReqNotFound
            self.tableView.removeWaitView()
            if error == nil{
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        print("Error")
                    }else{
                        if let _ = data["message"] as? String{
                            if let rawData = data["data"] as? [Any]{
                                
                                self.Bookings.removeAll()
                                
                                for item in rawData{
                                    if let data = item as? [String:Any]{
                                        let booking = BookingModel(data: data)
                                        if self.type == .Request {
                                            if Date().millisecondsSince1970 < Int(booking.dateTo!)!{
                                                 self.Bookings.append(booking)
                                            }
                                        }else{
                                            if Date().millisecondsSince1970 > Int(booking.dateTo!)! && booking.status?.lowercased() != "pending"{
                                                self.Bookings.append(booking)
                                            }
                                        }
                                       
                                    }
                                }
                            }
                        }
                        self.Bookings = self.Bookings.reversed()
                    }
                }
                self.tableView.reloadData()
            }else{
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func reloadCell(notification:NSNotification){
        if let data = notification.userInfo as? [String:Any]{
            if let Id = data["id"] as? String{
                if let index = self.Bookings.firstIndex(where: { (p) -> Bool in
                    return p.bookingId == Id
                }){
                    let indexPath = IndexPath(item: index, section: 0)
                    self.tableView.beginUpdates()
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                    self.tableView.endUpdates()
                }
            }
        }
    }
}


enum Type:String{
    
    case Request = "Request"
    case History = "History"
    
}
