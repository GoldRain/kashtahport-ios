//
//  HotelBookDateViewController.swift
//  Kashta Port
//
//  Created by mac on 24/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class HotelBookDateViewController: UIViewController {

    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    var dateTo:Date?
    var dateFrom:Date?
    
    @IBOutlet weak var dateMainView:UIView!
    
    var callback:((Date,Date,String,String) -> ())?
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "E, MMM d"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let view = touches.first?.view {
//            if view != self.dateMainView {
//                self.dismiss(animated: true, completion: nil)
//            }
            if view == self.mainView{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onFromDateClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CalenderViewController") as! CalenderViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
//        vc.fromDate = self.dateFrom
//        vc.callback = { date in
//            self.lblFrom.text = self.formatter.string(from: date)
//            self.dateFrom = date
//        }
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    @IBAction func onToDateClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CalenderViewController") as! CalenderViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
//        vc.toDate = self.dateTo
//        vc.callback = { date in
//            self.lblTo.text = self.formatter.string(from: date)
//            self.dateTo = date
//            
//        }
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    @IBAction func onDoneClick(_ sender: Any) {
        if validate(){
            self.callback!(dateFrom!,dateTo!,lblFrom.text!, lblTo.text!)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func validate() -> Bool{
        
        if dateFrom == nil {
            self.displayMessage(SelectStartDate)
            return false
        }
        
        if dateTo == nil {
            self.displayMessage(SelectEndDate)
            return false
        }
        return true    }
    
    
}
