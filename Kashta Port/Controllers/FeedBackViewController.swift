//
//  FeedBackViewController.swift
//  Kashta Port
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class FeedBackViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var ImgViewHotel: UIImageView!
    @IBOutlet weak var btnProvideFeedback: UIButton!
    @IBOutlet weak var tvProvideFeedback: UITextView!
    @IBOutlet weak var starCommunication: CosmosView!
    @IBOutlet weak var starProficiency: CosmosView!
    @IBOutlet weak var starDelivery: CosmosView!
    @IBOutlet weak var starPrice: CosmosView!
    
    var booking:BookingModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
        // Do any additional setup after loading the view.
    }
    
    
    func setupData(){
        self.navigationController?.isNavigationBarHidden = false
        self.title = ProvideFeedback
        self.setBackButton(withText: "")
        self.tvProvideFeedback.delegate = self
        
        if let imgUrl = booking?.hotel?.pictures?.first{
            self.ImgViewHotel.addWaitView()
            self.ImgViewHotel.sd_setImage(with: URL(string: imgUrl)!) { (img, err, _, _) in
                self.ImgViewHotel.removeWaitView()
                if let image = img {
                    self.ImgViewHotel.image = image
                }else{// placeholder image
                    self.ImgViewHotel.image = #imageLiteral(resourceName: "user-1")
                }
            }
        }else{// placeholder image
            self.ImgViewHotel.image = #imageLiteral(resourceName: "user-1")
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write a Review" {
            textView.text = ""
            textView.textAlignment = .left
            textView.textColor = .black
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trim() == "" {
            textView.text = "Write a Review"
            textView.textAlignment = .left
            textView.textColor = .black
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func onProvideFeedBackClick(_ sender: Any) {
        
        guard let hotelId = booking!.hotel?.propertyId else { return }
        
        
        let params = ["user_id":MyProfile.userId!,
                      "hotel_id":hotelId,
                      "communication":"\(starCommunication.rating)",
            "proficiency":"\(starProficiency.rating)",
            "price":"\(starPrice.rating)",
            "delivery":"\(starDelivery.rating)",
            "comment":tvProvideFeedback.text!,
            "booking_id":booking!.bookingId!
        ]
        
        JSONRequest.makeRequest(kFeedback, parameters: params) { (data, error) in
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    print("Error")
                }else{
                    if let msg = data["message"] as? String{
                        appDel.topViewController?.displayMessage(msg, callback: { () -> (Void) in
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SucessFeedBackViewController") as! SucessFeedBackViewController
                            vc.isFeedBack = true
                            self.navigationController?.present(vc, animated: true, completion: nil)
                        })
                    }
                }
            }
        }
    }
}

