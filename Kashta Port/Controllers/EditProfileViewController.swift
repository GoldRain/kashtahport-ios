//
//  EditProfileViewController.swift
//  kashtahPORT
//
//  Created by mac on 09/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import CountryPickerView

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var btnAddProfileImg: UIButton!
    @IBOutlet weak var tfName: CustomTextField!
    @IBOutlet weak var tfPhoneNumber: CustomTextField!
    @IBOutlet weak var tfEmail: CustomTextField!
    @IBOutlet weak var tfProfession: CustomTextField!
    @IBOutlet weak var textAreaDec: UITextView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var countryView: CountryPickerView!
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
        // Do any additional setup after loading the view.
    }
    
    func setupData(){
        
        self.navigationController?.isNavigationBarHidden = false
        self.title = EditProfile
        
        self.setBackButton(withText: "")
        self.descriptionView.addShadow()
        if let imgUrl = MyProfile.profilePhotoUrl, imgUrl != ""{
            self.profileImgView.addWaitView()
            self.profileImgView.sd_setImage(with: URL(string: imgUrl)!) { (img, err, _, _) in
                self.profileImgView.removeWaitView()
                if let image = img {
                    self.profileImgView.image = image
                    self.profileImgView.contentMode = .scaleAspectFill
                    self.profileImgView.clipsToBounds = true
                }else{// placeholder image
                    self.profileImgView.image = #imageLiteral(resourceName: "2019-05-18")
                }
            }
        }else{// placeholder image
            self.profileImgView.image = #imageLiteral(resourceName: "2019-05-18")
        }
        
        self.countryView.setCountryByPhoneCode("+\(MyProfile.countryCode!)")
        self.countryView.isUserInteractionEnabled = false
        self.tfName.text = MyProfile.userName
        self.tfPhoneNumber.text = MyProfile.mobile
        self.tfPhoneNumber.isEnabled = false
        self.tfEmail.text = MyProfile.userEmail
        self.tfEmail.isEnabled = false
        self.tfProfession.text = MyProfile.profession ?? ""
        self.textAreaDec.text = MyProfile.description ?? ""
    }
    
    var selectedImage:UIImage!
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage{
            imagePicker.dismiss(animated: true) {
                self.profileImgView.image = image
                self.selectedImage = image
                self.profileImgView.contentMode = .scaleAspectFill
                self.profileImgView.clipsToBounds = true
            }
        }
    }

    @IBAction func onAddProfileImg(_ sender: Any) {
        imagePicker.delegate = self as UIImagePickerControllerDelegate as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.navigationBar.barStyle = .blackOpaque
        imagePicker.navigationBar.tintColor = APP_PRIMARY_COLOR
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onUpdateClick(_ sender: Any) {
        
        guard let userId = MyProfile.userId else{
            return
        }
        
        if let image = self.selectedImage {
            
            let wait = self.addWaitSpinner()
            
            let image = Image(image: image, url: "")
            
            uploadImagesToAws([image], folderName: appDel.userImageFolder, randomName: randomString(length: 20)) { (urls) in
                
                self.removeWaitSpinner(waitView: wait)
                
                if let urls = urls {
                    if let url = urls.first {
                        self.uploadProfile(imageUrl: url, userId: userId)
                    }
                }
            }
        }else{
            self.uploadProfile(imageUrl: MyProfile.profilePhotoUrl ?? "", userId: userId)
        }
    }
    
    func uploadProfile(imageUrl:String, userId: String){
        
        if let name = tfName.text, name != ""{
            
            let parmas = ["user_id":userId,
                          "name" : name,
                          "email":tfEmail.text!,
                          "countryCode":self.countryView.selectedCountry.phoneCode.replacingOccurrences(of: "+", with: ""),
                          "phone":tfPhoneNumber.text!,
                          "description":textAreaDec.text!,
                          "profession":tfProfession.text!,
                          "profile_image_url":imageUrl
                        ]
            
            let wait = self.addWaitSpinner()
            
            JSONRequest.makeRequest(kUpdateProfile, parameters: parmas) { (data, error) in
                
                self.removeWaitSpinner(waitView: wait)
                
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        
                        deleteAwsFile(folderName: appDel.userImageFolder, fullPath: imageUrl) // deleting current uploaded image
                        
                        if let msg = data["message"] as? String{
                            self.displayMessage(msg)
                        }
                    }else{
                        if let msg = data["message"] as? String{
                            if let rawData = data["data"] as? [String:Any]{
                                
                                if let url = MyProfile.profilePhotoUrl{ // deleting old user image
                                    deleteAwsFile(folderName: appDel.userImageFolder, fullPath: url)
                                }
                                
                                MyProfile.set(rawData: rawData)
                                self.displayMessage(msg, callback: { () -> (Void) in
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                    }
                }
            }
        }
        
    }
}
