//
//  BookingsViewController.swift
//  kashtahPORT
//
//  Created by mac on 13/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import CarbonKit

class BookingsViewController: UIViewController,CarbonTabSwipeNavigationDelegate {

    @IBOutlet weak var swipeView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = Bookings
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.setBackButton(withText: "")
        setupCarbonSwipe()
        // Do any additional setup after loading the view.
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingRequestViewController") as! BookingRequestViewController
            vc.type = .Request
            return vc
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingRequestViewController") as! BookingRequestViewController
            vc.type = .History
            return vc
        }
    }
    
    
    
    func setupCarbonSwipe() {
        
        let font = UIFont(name: "Roboto-Regular", size: 16)
        let items = [Requests, History]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: swipeView)
        let widthOfTabIcons = self.view.frame.width/2
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.backgroundColor = UIColor.white
        carbonTabSwipeNavigation.setNormalColor(UIColor.gray,font: UIFont(name: "Roboto-Regular", size: 16)!)
        carbonTabSwipeNavigation.setIndicatorColor(APP_PRIMARY_COLOR)
        carbonTabSwipeNavigation.setSelectedColor(APP_PRIMARY_COLOR,font: UIFont(name: "Roboto-Regular", size: 16)!)
    }

}
