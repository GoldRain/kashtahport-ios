//
//  TimePickerViewController.swift
//  Kashta Port
//
//  Created by mac on 25/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class TimePickerViewController: UIViewController {
    
    @IBOutlet var mainView: UIView!
    
    var callback:((Date)->())?
    
    

    @IBOutlet weak var timePicker: UIDatePicker!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            if touch.view == mainView{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let calendar = Calendar.current
        var components = DateComponents()
        components.hour = 12
        timePicker.setDate(calendar.date(from: components)!, animated: true)
    }

    @IBAction func onOkClick(_ sender: Any) {
        self.callback!(timePicker.date)
        self.dismiss(animated: true, completion: nil)
    }
}
