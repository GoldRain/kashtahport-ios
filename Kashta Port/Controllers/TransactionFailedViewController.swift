//
//  TransactionFailedViewController.swift
//  kashtahPORT
//
//  Created by mac on 21/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class TransactionFailedViewController: UIViewController {

    @IBOutlet weak var lblTxnId: UILabel!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var lblTime: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func onBackClick(_ sender: Any) {
    }
    
    @IBAction func onContactUsClick(_ sender: Any) {
    }
    
}
