//
//  PhoneNumberViewController.swift
//  kashtahPORT
//
//  Created by alienbrainz on 17/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import CountryPickerView
class PhoneNumberViewController: UIViewController {
    
    var callback:((Bool)->())!
    
    var regParams = [String:String]()
    
    @IBOutlet weak var countryView:CountryPickerView!
    @IBOutlet weak var tfPhoneNumber: CustomTextField!
    
    var type:ControllerFrom = .Other
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setBackButton(withText: "")
        tfPhoneNumber.keyboardType = .asciiCapableNumberPad
        
        self.countryView.delegate = self
        self.countryView.dataSource = self
        
        self.countryView.setCountryByCode("KW")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func onSendClick(_ sender: Any) {
       
        if validate() {
            
            regParams["country_code"] = self.countryView.selectedCountry.phoneCode.replacingOccurrences(of: "+", with: "")
            regParams["phone"] = tfPhoneNumber.text!
            
            let parmas = ["phone":tfPhoneNumber.text!,
                          "country_code":self.countryView.selectedCountry.phoneCode.replacingOccurrences(of: "+", with: "")]
            
            let wait = self.addWaitSpinner()
            
            JSONRequest.makeRequest(kGetOTP, parameters: parmas) { (data, error) in

                self.removeWaitSpinner(waitView: wait)

                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        print("Error")
                        if let msg = data["message"] as? String{
                            self.displayMessage(msg)
                        }
                    }else{
                        if let _ = data["message"] as? String{
                            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPViewController") as? VerifyOTPViewController{
                                vc.phoneNumber = self.tfPhoneNumber.text!
                                vc.registerParams = self.regParams
                                vc.screenType = .facebookRegister
                                vc.type = self.type
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func validate() -> Bool {
        
        let phone = self.countryView.selectedCountry.phoneCode
        
        if phone.trim() == ""{
            self.displayMessage(EnterCountry)
            return false
        }
        
        if let phone = tfPhoneNumber.text,phone.trim() == ""{
            self.displayMessage(EnterNumber)
            return false
        }
        
        
        return true
    }
    
}
extension PhoneNumberViewController: CountryPickerViewDataSource, CountryPickerViewDelegate {
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print("\(country.code)  == \(country.phoneCode)")
    }
    
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country]{
        return preferredCountry
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String?{
        return "Countrys"
    }
    
    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool{
        return true
    }
}
