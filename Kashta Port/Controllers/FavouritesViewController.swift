//
//  FavouritesViewController.swift
//  Kashta Port
//
//  Created by mac on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class FavouritesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    //MARK:- Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK:- Variable
    var refreshControl:UIRefreshControl!
    var Favourites = [PropertyModel]()
    
    var emptyText = Loading
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getFavouritesHotel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if !MyProfile.isLogin {
            // add loginView
            addLoginView(backgroundMainView: self.backgroundView, view: self.view, owner: self)
        }else{
            removeLoginView(view: self.view)
        }
    }
    
    let backgroundView = UIView()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
//        removeLoginView(view: self.view)
    }
    
    func setupData(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        
        let attrStr = NSAttributedString(string: Refreshing)
        
        refreshControl = getRefreshControl(attrStr: attrStr)
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getFavouritesHotel()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Favourites.count == 0 {
            return 1
        }else{
            return Favourites.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if Favourites.count != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteTableViewCell", for: indexPath) as! FavouriteTableViewCell
            cell.favHotel = self.Favourites[indexPath.row]
            cell.btnDelete.restorationIdentifier = "\(indexPath.row)"
            if indexPath.row % 2 == 1 {
                cell.backgroundColor = UIColor.white
            }else{
                cell.backgroundColor = LIGHT_GREY
            }
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = emptyText
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.Favourites.isEmpty{
            return;
        }
        
        if let isDeleted = self.Favourites[indexPath.row].isDeleted, !isDeleted{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelDetailsViewController") as! HotelDetailsViewController
            vc.property = Favourites[indexPath.row]
            vc.fromVC = .HomeVC
            vc.userType = .Buyer
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.displayMessage(AlertNotFound)
        }
    }

    @IBAction func onDeleteClcik(_ sender: UIButton) {
        if let index = Int(sender.restorationIdentifier!) {
            if index < self.Favourites.count{
                if let hotelId = self.Favourites[index].propertyId{
                    MessageCenter.removeFavouritesHotel(hotelId: Favourites[index].propertyId!) {
                        DispatchQueue.main.async {
                            self.Favourites.remove(at: index)
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    @objc func getFavouritesHotel(){
        
        guard let userId = MyProfile.userId else {
            self.Favourites.removeAll()
            self.tableView.reloadData()
            return
        }
        
        let params = ["user_id":userId]
        
        self.tableView.addWaitView()
        
        JSONRequest.makeRequest(kFavouriteHotelList, parameters: params) { (data, error) in
            
            self.emptyText = FavouritesNotFound
            
            self.Favourites.removeAll()
            
            self.tableView.removeWaitView()
            if error == nil{
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, !err{
                        if let array = data["data"] as? [[String:Any]]{
                            for item in array{
                                self.Favourites.append(PropertyModel(data: item))
                            }
                        }
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
}
