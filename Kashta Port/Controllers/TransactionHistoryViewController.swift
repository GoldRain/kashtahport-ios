//
//  TransactionHistoryViewController.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import DropDown

class TransactionHistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate {
    
    var transcations = [TransactionModel]()
    
    var emptyText = Loading
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblSort: UILabel!
    @IBOutlet weak var lblSortType: UILabel!
    @IBOutlet weak var viewPrice: UIView!
    
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getTransactionList()
    }
    
    func setupData(){
        self.viewPrice.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        self.title = TransactionHistory
        self.navigationController?.isNavigationBarHidden = false
        self.setBackButton(withText: "")
    }
    
    @objc func handleTap() {
        dropDown.anchorView = viewPrice
        dropDown.dataSource = [Time, Price]
        dropDown.direction = .any
        dropDown.width = 60
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            if item == Time{
                self.transcations.sort(by: { (t1, t2) -> Bool in
                    return Int(t1.createdAt!)! > Int(t2.createdAt!)!
                })
            }else{
                self.transcations.sort(by: { (t1, t2) -> Bool in
                    return t1.amount! < t2.amount!
                })
            }
            self.tableView.reloadData()
            self.lblSortType.text = item
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if transcations.count != 0{
            return transcations.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if transcations.isEmpty{
            let cell = UITableViewCell()
            cell.textLabel?.text = emptyText
            cell.textLabel?.textAlignment = .center
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TranscationTableViewCell", for: indexPath) as! TranscationTableViewCell
            cell.transcation = transcations[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = UIStoryboard(name: "Transactions", bundle: nil).instantiateViewController(withIdentifier: "TransactionDetailsViewController") as? TransactionDetailsViewController {
            vc.transaction = transcations[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getTransactionList(){
        
        self.transcations.removeAll()
        
        guard let id = MyProfile.userId else {
            tableView.reloadData()
            return
        }
        
        let parmas = ["buyer_id":id]
        self.emptyText = TranscationNotFound
        JSONRequest.makeRequest(kTransactionList, parameters: parmas) { (data, error) in
            if error == nil{
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        print("Error")
                    }else{
                        if let _ = data["message"] as? String{
                            if let arr = data["data"] as? [Any]{
                                for item in arr.enumerated(){
                                    if let rawData = item.element as? [String:Any]{
                                        let transaction = TransactionModel(data: rawData)
                                        self.transcations.append(transaction)
                                    }
                                }
                            }
                        }
                    }
                }
                self.transcations.sort(by: { (t1, t2) -> Bool in
                    return t1.createdAt! > t2.createdAt!
                })
                self.tableView.reloadData()
            }else{
                self.tableView.reloadData()
            }
        }
        
    }
    
}
