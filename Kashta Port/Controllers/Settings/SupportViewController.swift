//
//  SupportViewController.swift
//  kashtahPORT
//
//  Created by alienbrainz on 03/07/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import MessageUI

class SupportViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var txtViewMain: UIView!
    @IBOutlet weak var txtView: UITextView!
    
    @IBOutlet weak var btnSubmit:UIButton!
    
    var message:String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        txtViewMain.addShadow()
        
        if let msg = message {
            self.txtView.text = msg
        }
        
        self.btnSubmit.setTitle(Submit, for: .normal)
        
        self.title = ContactUs
        self.setBackButton(withText: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onSendButtonClick(_ sender:Any) {
        
        self.view.endEditing(true)
        
        let text = self.txtView.text.trim()
        
        if text == ""{
            self.displayMessage("Enter comments")
            return
        }
        
        let mailComposeViewController = configureMailComposer()
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposeViewController, animated: true, completion: nil)
        }else{
            print("Can't send email. You can still connect to us through call")
        }
    }
    
    func configureMailComposer() -> MFMailComposeViewController{
        
        let text = self.txtView.text.trim()
        
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        mailComposeVC.setToRecipients([SupportMail])
        mailComposeVC.setSubject("Support Request - \(MyProfile.userId ?? "")")
        mailComposeVC.setMessageBody(text, isHTML: false)
        return mailComposeVC
    }
    
    //MARK: - MFMail compose method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        if result == MFMailComposeResult.cancelled{
            
            controller.dismiss(animated: true, completion: nil)
            
        }
        else if result == MFMailComposeResult.failed{
            
            controller.dismiss(animated: true, completion: {
                
                self.displayMessage("Failed. Please try again.")
                
            })
            
        }
        else{
            
            self.navigationController?.popViewController(animated: true)
            
            controller.dismiss(animated: true, completion: nil)
            
        }
    }
    
}
