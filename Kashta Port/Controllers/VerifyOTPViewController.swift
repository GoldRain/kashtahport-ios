//
//  VerifyOTPViewController.swift
//  kashtahPORT
//
//  Created by mac on 29/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import WXImageCompress

enum verifyOTPScreenType {
    case normalRegister
    case facebookRegister
}

class VerifyOTPViewController: UIViewController {

    @IBOutlet weak var tfPincode: CustomTextField!
    @IBOutlet weak var lblPhoneNumber:UILabel!
    
    var type:ControllerFrom = .Other
    
    var registerParams:[String:String]?
    
    var phoneNumber:String?
    var countryCode:String?
    var profileImage:UIImage?
    
    var fromScreen:String?
    
    var screenType:verifyOTPScreenType = .normalRegister
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tfPincode.keyboardType = .asciiCapableNumberPad
        if let number = self.phoneNumber {
            self.lblPhoneNumber.text = number
            if let countryCode = self.countryCode {
                self.lblPhoneNumber.text = "\(countryCode) \(number)"
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func onBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onDidNotGetOTPClick(_ sender: Any) {
        let optionMenu = getAlertController(title: nil, message: AlertOTP)
        
        let resendOtp = UIAlertAction(title: ResendOTP, style: .default) { (action) in
            self.getOTP()
        }
        
        let callOtp = UIAlertAction(title: CallOTP, style: .default) { (action) in
            self.getCallOTP()
        }
        
        let cancelAction = UIAlertAction(title: Cancel, style: .destructive, handler: nil)
//        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        optionMenu.addAction(resendOtp)
        optionMenu.addAction(callOtp)
        optionMenu.addAction(cancelAction)
        
        appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
    }
    
    func getOTP(){
        
        let parmas = ["phone":self.phoneNumber!,
                      "country_code":self.countryCode!.replacingOccurrences(of: "+", with: "")]
        
        let wait = self.addWaitSpinner()
        
        JSONRequest.makeRequest(kGetOTP, parameters: parmas) { (data, error) in
            
            self.removeWaitSpinner(waitView: wait)
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    print("Error")
                }else{
                    if let _ = data["message"] as? String{
                        self.displayMessage(NSLocalizedString("OTP has been sent", comment: "OTP has been sent"))
                    }
                }
            }
        }
    }
    
    func getCallOTP(){
        
        let parmas = ["phone":self.phoneNumber!,
                      "country_code":self.countryCode!.replacingOccurrences(of: "+", with: "")]
        
        let wait = self.addWaitSpinner()
        
        JSONRequest.makeRequest(kGetCallOTP, parameters: parmas) { (data, error) in
            
            self.removeWaitSpinner(waitView: wait)
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    print("Error")
                }else{
                    if let _ = data["message"] as? String{
                        
                    }
                }
            }
        }
        
    }
    
    
    @IBAction func onVerifyClick(_ sender: Any) {
        if validate(){
            
            let parmas = ["phone":phoneNumber!,
                          "otp":tfPincode.text!]
            
            let wait = self.addWaitSpinner()
            
            JSONRequest.makeRequest(kVerifyOTP, parameters: parmas) { (data, error) in
                
                self.removeWaitSpinner(waitView: wait)
                
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        if let msg = data["message"] as? String{
                            self.displayMessage(msg)
                        }
                    }else{
                        if let image = self.profileImage{
                            
                            self.uploadProfileImage(image: image, callback: { (urlString) in
 
                                self.register(imageUrl: urlString)
                            })
                        }else{
                            self.register(imageUrl: "")
                        }
                    }
                }
            }
        }
    }
    
    func uploadProfileImage(image:UIImage ,callback:@escaping ((String)->())){
        
        let image = Image(image: image, url: "")
        
        uploadImagesToAws([image], folderName: appDel.userImageFolder, randomName: randomString(length: 20)) { (urls) in
            
            if let urls = urls {
                if let url = urls.first {
                    callback(url)
                }
            }
        }
    }
    
    func register(imageUrl:String){
        
        let wait = self.addWaitSpinner()
        
        registerParams!["profile_image_url"] = imageUrl
        
        JSONRequest.makeRequest(kRegister, parameters: registerParams!) { (data, error) in
            
            self.removeWaitSpinner(waitView: wait)
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    if let msg = data["message"] as? String{
                        self.displayMessage(msg)
                    }
                }else{
                    
                    if self.screenType == .normalRegister {
                        self.displayMessage(RegSuccess, callback: { () -> (Void) in
                            if let vcs = self.navigationController?.viewControllers{
                                self.navigationController?.popToViewController(vcs[0], animated: true)
                            }
                        })
                    }else {
                        if let user = data["data"] as? [String:Any]{
                            MyProfile.set(rawData: user)
                            MessageCenter.updateNotificationToken()
                            MyProfile.isLogin = true
                            NotificationCenter.default.post(name: .UserLoginLogout, object: nil)
                            
                            if self.type == .Other{
                                self.dismiss(animated: true, completion: nil)
                            }else{
                                self.dismiss(animated: true, completion: {
                                    if let vc = UIStoryboard(name: "AddProperty", bundle: nil).instantiateViewController(withIdentifier: "AddPropertyViewController") as? AddPropertyViewController{
                                        appDel.topViewController?.navigationController?.pushViewController(vc, animated: true)
                                    }
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    func validate() -> Bool{
        
        if let pincode = tfPincode.text, pincode.trim() == ""{
            self.displayMessage(EnterOTP)
            return false
        }
        
        return true
    }
}
