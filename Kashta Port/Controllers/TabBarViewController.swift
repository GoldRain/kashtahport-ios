//
//  TabBarViewController.swift
//  Kashta Port
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

var TabBarVC:TabBarViewController?

class TabBarViewController: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        TabBarVC = self
        NotificationCenter.default.addObserver(self, selector: #selector(updatebadge), name: .UpdateBadge, object: nil)
    }
    
    @objc func updatebadge(){
//        if badge != 0{
        if let badge = MyProfile.badge{
            if badge != 0 {
                if let items = self.tabBar.items{
                    let not = items[2]
                    not.badgeValue = "\(badge)"
                    not.badgeColor = UIColor.red
                }
            }else{
                if let items = self.tabBar.items{
                    let not = items[2]
                    not.badgeValue = ""
                    not.badgeColor = UIColor.clear
                }
            }
        }else{
            if let items = self.tabBar.items{
                let not = items[2]
                not.badgeValue = ""
                not.badgeColor = UIColor.clear
            }
        }
    }
}
