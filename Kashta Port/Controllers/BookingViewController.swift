//
//  BookingViewController.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class BookingRequestViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    var Bookings = [BookingModel]()

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Booking Request"
        self.setBackButton(withText: "")
        NotificationCenter.default.addObserver(self, selector: #selector(getBookingList), name: .BookingRequestStatusUpdate, object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getBookingList()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Bookings.count != 0{
            return Bookings.count
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if Bookings.count != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTableViewCell", for: indexPath) as! BookingTableViewCell
            cell.bookRequest = Bookings[indexPath.row]
            cell.backgroundColor = UIColor.white
            return cell
        }else{
            let cell = UITableViewCell()
            cell.textLabel?.text = "No Booking Request Found!!"
            cell.textLabel?.textAlignment = .center
            return cell
        }
        
    }
    
    @objc func getBookingList(){
        
        self.Bookings.removeAll()
        
        guard MyProfile.isLogin else {
            return
        }
        
        let params = ["user_id":MyProfile.userId!]
        
        self.tableView.addWaitView()
        
        JSONRequest.makeRequest(kBookingRequest, parameters: params) { (data, error) in
            
            self.tableView.removeWaitView()
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    print("Error")
                }else{
                    if let _ = data["message"] as? String{
                        if let rawData = data["data"] as? [Any]{
                            for item in rawData{
                                if let data = item as? [String:Any]{
                                    let booking = BookingModel(data: data)
                                    self.Bookings.append(booking)
                                }
                            }
                        }
                    }
                    
                    self.Bookings = self.Bookings.filter({ (booking) -> Bool in
                        return booking.status == "pending"
                    })
                    
                    self.tableView.reloadData()
                }
            }
        }
    }
}
