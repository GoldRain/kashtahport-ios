//
//  CampViewController.swift
//  Kashta Port
//
//  Created by mac on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class CampViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
   
    //MARK:- Outlet
    @IBOutlet weak var tableView: UITableView! // Table View
    @IBOutlet weak var lblTitle: UILabel!      // Title Label
    
    //MARK:- Variable
    var controllerType:ControllerType = .Camp  // For controller Type
    let refreshControl = UIRefreshControl()    // Top Refresh View
    
    var Hotels = [PropertyModel]()
    
    //MARK:- Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(getHotelList), name: .SearchHotel, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getHotelList), name: .UserLoginLogout, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCell(not:)), name: .HotelsFavouritesStatusUpdate, object: nil)
        
        self.setupData()
    }
    
    func setupData(){
        
        switch controllerType {
        case .Camp:
            lblTitle.text = NSLocalizedString("Popular Camp", comment: "Popular Camp")
        case .Farm :
            lblTitle.text = NSLocalizedString("Popular Farm", comment: "Popular Camp")
        case .Chalet :
            lblTitle.text = NSLocalizedString("Popular Chalet", comment: "Popular Camp")
        }
        
        self.addRefreshView()
        
        self.getHotelList()
    }
    
    @objc func getHotelList(){
        
        self.lblTitle.isHidden = true
        
        Hotels.removeAll()
        
        let waitView = self.addWaitSpinner()
        
        var type = ""
        
        if SearchHotel.type != "" {
            type = SearchHotel.type
        }else{
            type = controllerType.rawValue
        }
        
        let params = ["type": type ,//controllerType.rawValue,
                      "city":SearchHotel.city,
                      "country":SearchHotel.country,
                      "rooms":SearchHotel.room,
                      "children":SearchHotel.child,
                      "person":SearchHotel.person,
                      "wc_number":SearchHotel.wc,
                      "date_from":SearchHotel.startDate,
                      "date_to":SearchHotel.endDate]
        
        JSONRequest.makeRequest(kSearchHotel, parameters: params) { (data, error) in
            
            self.removeWaitSpinner(waitView: waitView)
            
            if error == nil {
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        print("Error")
                    }else{
                        if let _ = data["message"] as? String{
                            if let array = data["data"] as? [Any]{
                                for item in array{
                                    if let itemData = item as? [String:Any]{
                                        let favHotel = PropertyModel(data: itemData)
                                        self.Hotels.append(favHotel)
                                    }
                                }
                                self.Hotels = self.Hotels.filter({ (hotel) -> Bool in
                                    return hotel.isEnabled == true
                                })
                            }
                        }
                        if self.Hotels.isEmpty {
                            self.lblTitle.isHidden = true
                        }else{
                            self.Hotels = self.Hotels.reversed()
                            self.lblTitle.isHidden = false
                        }
                    }
                }
                self.tableView.reloadData()
            }else{
                self.tableView.reloadData()
            }
            
        }
    }
    
    func addRefreshView(){
        refreshControl.tintColor = APP_PRIMARY_COLOR
        refreshControl.attributedTitle = NSAttributedString(string: Loading)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    @objc func refresh(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.getHotelList()
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc func reloadCell(not:NSNotification){
        if let data = not.userInfo as? [String:Any]{
            if let Id = data["id"] as? String{
                if let index = self.Hotels.firstIndex(where: { (h) -> Bool in
                    return h.propertyId == Id
                }){
                    let indexPath = IndexPath(item: index, section: 0)
                    self.tableView.beginUpdates()
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                    self.tableView.endUpdates()
                }
            }
        }
    }
    
    //MARK:- Table view Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Hotels.count != 0{
            return Hotels.count
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if Hotels.count != 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PopularHotelTableViewCell", for: indexPath) as? PopularHotelTableViewCell
            cell?.hotel = Hotels[indexPath.row]
            cell?.selectionStyle = .none
            return cell!
        }else{
            let cell = UITableViewCell()
            cell.textLabel?.text = NSLocalizedString("No \(controllerType.rawValue) Found!!", comment: "No \(controllerType.rawValue) Found!!")
            cell.textLabel?.textAlignment = .center
            cell.isUserInteractionEnabled = false
            cell.textLabel?.textColor = .gray
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelDetailsViewController") as! HotelDetailsViewController
        vc.property = Hotels[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- Extra
enum ControllerType:String{
    case Camp = "Camp"
    case Farm = "Farm"
    case Chalet = "Chalet"
}


class SearchHotel{
    
    static var city = ""
    static var type = ""
    static var country = ""
    static var room = ""
    static var child = ""
    static var person = ""
    static var wc = ""
    static var startDate = ""
    static var endDate = ""
    
    static var startDateWithDateObj:Date!
    static var endDateWithDateObj:Date!
    
    static func reset(){
        city = ""
        type = ""
        country = ""
        room = ""
        child = ""
        person = ""
        wc = ""
        startDate = ""
        endDate = ""
        
        startDateWithDateObj = nil
        endDateWithDateObj = nil
    }
}
