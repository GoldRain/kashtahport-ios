//
//  TransactionDetailsViewController.swift
//  kashtahPORT
//
//  Created by mac on 17/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import Cosmos

class TransactionDetailsViewController: UIViewController {

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var imgViewHotelImage: UIImageView!
    @IBOutlet weak var lblTransactionId: UILabel!
    @IBOutlet weak var lblDisplayId: UILabel!
    @IBOutlet weak var imgViewPayType: UIImageView!
    @IBOutlet weak var imgSuccess: UIImageView!
    @IBOutlet weak var viewHotel: UIView!
    @IBOutlet weak var viewDetails: UIView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var btnFeedBack: UIButton!
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM YYYY hh:mm a"
        return formatter
    }()
    
    
    var transaction:TransactionModel?
    
    var booking:BookingModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupData(){
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (t) in
            UIView.animate(withDuration: 1, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                // HERE
                self.imgSuccess.transform = CGAffineTransform.identity.scaledBy(x: 1/2, y: 1/2) // Scale your image
                
            }) { (finished) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.imgSuccess.transform = CGAffineTransform.identity // undo in 1 seconds
                })
            }
        }
        
        self.title = TransactionDetail
        self.navigationController?.isNavigationBarHidden = false
        self.setBackButton(withText: "")
        if let transaction = self.transaction{
            self.btnFeedBack.isHidden = true
            self.viewHotel.addShadow()
            self.viewDetails.addShadow()
            self.getHotel()
//            self.lblAmount.text = "\(CurrentCurrency) \(transaction.amount!)"
            self.lblTime.text = formatter.string(from: Date(milliseconds: Int(transaction.createdAt!)!))
            self.lblTransactionId.text = transaction.transactionId!
            self.lblDisplayId.text = "\(transaction.displayId ?? 1)"
            if transaction.cardType?.lowercased().capitalized == "Mastercard" {
                self.imgViewPayType.image = #imageLiteral(resourceName: "MasterCard")
            }else if transaction.cardType?.lowercased().capitalized == "Visa"{
                self.imgViewPayType.image = #imageLiteral(resourceName: "visa")
            }else if transaction.cardType?.lowercased().capitalized == "American Express"{
                self.imgViewPayType.image = #imageLiteral(resourceName: "american")
            }
        }
       
    }
    @IBAction func onFeedbackClick(_ sender: Any) {
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedBackViewController") as? FeedBackViewController{
            vc.booking = booking
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    func getHotel(){
        
        let params = ["booking_id":transaction!.bookingId!]
        self.imgViewHotelImage.addWaitView()
        JSONRequest.makeRequest(kSingleBookingRequest, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err {
                    if let arr = data["data"] as? [Any]{
                        if let rawData = arr.first as? [String:Any]{
                            self.booking = BookingModel(data: rawData)
                            if let booking = self.booking{
                                if let name = booking.hotel?.name,let imgUrl = booking.hotel?.pictures?.first,let city = booking.hotel?.city,let country = booking.hotel?.country, let dec = booking.hotel?.description,let rating = booking.hotel?.rating, let checkout = booking.dateTo{
                                    self.lblHotelName.text = name
                                    self.lblPlace.text = city.capitalized + ", " + country.capitalized
                                    self.lblDesc.text = dec
                                    self.rating.rating = rating
                                    self.imgViewHotelImage.sd_setImage(with: URL(string: imgUrl), completed: { (image, error, _, nil) in
                                        if let image = image{
                                            self.imgViewHotelImage.removeWaitView()
                                            self.imgViewHotelImage.image = image
                                        }
                                    })
                                    if let checkoutTime = Int(checkout){
                                        if Date().millisecondsSince1970 > checkoutTime{
                                            self.btnFeedBack.isHidden = false
                                        }
                                    }
                                    var currency = CurrentCurrency
                                    if let value = self.booking?.hotel?.currency, value != ""{
                                        if value.lowercased() == "usd"{
                                        }else{
                                            currency = value
                                        }
                                    }
                                    self.lblAmount.text = "\(currency.uppercased()) \(self.transaction!.amount!)"
                                }
                            }
                        }
                    }
                }else if let message = data["message"] as? String{
                    self.displayMessage(message)
                }
            }
        }
    }

}
