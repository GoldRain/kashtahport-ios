//
//  ProfileViewController.swift
//  Kashta Port
//
//  Created by mac on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var loginAccountView: UIView!
    @IBOutlet weak var lblLoginText: UILabel!
    
    var superView = UIView()
    var LatestBookings = [BookingModel]()
    
    var paginationNumber:Int = 0
    
    var isLoadMore = false
    
    let refreshControl = UIRefreshControl()
   
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblLoginText.text = LoginText
        self.setupData()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCell(notification:)), name: .BookingRequestStatusUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(not), name: .UserLoginLogout, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
        if !MyProfile.isLogin {
            kashtahPORT.addLoginView(backgroundMainView: self.superView, view: self.view, owner: self)
        }else{
            removeLoginView(view: self.view)
        }
    }
    
    @objc func not(){
        self.paginationNumber = 0
        self.getProfileData()
    }
    
    func setupData(){
        self.navigationController?.isNavigationBarHidden = true
        self.addRefreshView()
        getProfileData()
    }
    
    func addLoginView(){
        
        self.view.addSubview(self.superView)
        self.superView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        self.superView.translatesAutoresizingMaskIntoConstraints = false
        self.superView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        self.superView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        self.superView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        self.superView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        
        self.superView.addSubview(loginAccountView)
        self.loginAccountView.translatesAutoresizingMaskIntoConstraints = false
        self.loginAccountView.centerXAnchor.constraint(equalTo: self.superView.centerXAnchor).isActive = true
        self.loginAccountView.centerYAnchor.constraint(equalTo: self.superView.centerYAnchor).isActive = true
        self.loginAccountView.heightAnchor.constraint(equalToConstant: 240).isActive = true
        self.loginAccountView.leadingAnchor.constraint(equalTo: self.superView.leadingAnchor, constant: 20).isActive = true
        self.loginAccountView.leadingAnchor.constraint(equalTo: self.superView.trailingAnchor, constant: 20).isActive = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if LatestBookings.count == 0 {
            return 2
        }else{
            return LatestBookings.count + 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            cell.setupData()
            cell.selectionStyle = .none
            return cell
        }else{
            if LatestBookings.count != 0 {
                let cell =  tableView.dequeueReusableCell(withIdentifier: "LatestBookingTableViewCell", for: indexPath) as! LatestBookingTableViewCell
                cell.bookHotel = LatestBookings[indexPath.row - 1]
                if indexPath.row % 2 == 1 {
                    cell.backgroundColor = UIColor.white
                }else{
                    cell.backgroundColor = LIGHT_GREY
                }
                cell.selectionStyle = .none
                return cell
            }else{
                let cell = UITableViewCell()
                cell.textLabel?.text = BookingNotFound
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .lightGray
                cell.isUserInteractionEnabled = false
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0 {
            return;
        }
        
        if self.LatestBookings.isEmpty{
            return;
        }
        
        if let hotel = self.LatestBookings[indexPath.row - 1].hotel {
            if let isDeleted = hotel.isDeleted, !isDeleted{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelDetailsViewController") as! HotelDetailsViewController
                vc.property = LatestBookings[indexPath.row - 1].hotel
                vc.bookings = LatestBookings[indexPath.row - 1]
                vc.fromVC = .OtherVC
                vc.userType = .Buyer
                
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                self.displayMessage(AlertNotFound)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastIndex = self.LatestBookings.count - 1
        if indexPath.row == lastIndex{
            if isLoadMore {
                self.loadMore()
            }
        }
    }
    
    func loadMore(){
        self.paginationNumber = self.paginationNumber + 1
        self.getProfileData()
    }
    
   
    @IBAction func onEditClick(_ sender: Any) {
        if let vc = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onLoginClick(_ sender: Any) {
        let vc = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        vc.type = .Other
        let nvc = UINavigationController(rootViewController: vc)
        self.navigationController?.present(nvc, animated: true, completion: nil)
    }
    
    @objc func getProfileData(){
        
        print("Notification revice in profile")
        
        let number = self.paginationNumber
        
        if number == 0 {
            self.LatestBookings.removeAll()
            self.paginationNumber = 0
        }
        
        if !MyProfile.isLogin {
            tableView.reloadData()
            return
        }
        
        let params = ["user_id":MyProfile.userId!,
                      "number":"\(number)"]
        
        JSONRequest.makeRequest(kViewProfile, parameters: params) { (data, error) in
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    self.isLoadMore = false
                    if number == 0 {
                        self.tableView.reloadData()
                    }
                }else{
                    if let _ = data["message"] as? String{
                        if let rawData = data["data"] as? [Any]{
                            var indexPaths = [IndexPath]()
                            for item in rawData{
                                if let data = item as? [String:Any]{
                                    let booking = BookingModel(data: data)
                                    self.LatestBookings.append(booking)
                                    if number != 0 {
                                        indexPaths.append(IndexPath(row: self.LatestBookings.count - 1, section: 0))
                                    }
                                }
                            }
                            if number != 0{
                                self.tableView.beginUpdates()
                                self.tableView.insertRows(at: indexPaths, with: .bottom)
                                self.tableView.endUpdates()
                                let contentOffset = self.tableView.contentOffset
                                self.tableView.layoutIfNeeded()
                                self.tableView.setContentOffset(contentOffset, animated: false)
                            }
                        }
                    }
                    if number == 0 {
                        self.tableView.reloadData()
                    }
                    self.isLoadMore = true
                }
            }
        }
        
    }
    
    func addRefreshView(){
        refreshControl.tintColor = APP_PRIMARY_COLOR
        refreshControl.attributedTitle = NSAttributedString(string: "loading...")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    @objc func refresh(){
        self.paginationNumber = 0
        self.getProfileData()
        self.refreshControl.endRefreshing()
    }
    
    @objc func reloadCell(notification:NSNotification){
        print("Notification recivie")
        if let data = notification.userInfo as? [String:Any]{
            if let Id = data["id"] as? String{
                if let index = self.LatestBookings.firstIndex(where: { (p) -> Bool in
                    return p.bookingId == Id
                }){
                    print("Notification Cell update",index + 1)
                    let indexPath = IndexPath(item: index + 1, section: 0)
                    self.tableView.beginUpdates()
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                    self.tableView.endUpdates()
                }
            }
        }
    }
    
}

