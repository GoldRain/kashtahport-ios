//
//  AddPropertyViewController.swift
//  Kashta Port
//
//  Created by alienbrainz on 22/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import DropDown
import YPImagePicker
import ImageViewer
import GoogleMaps
import GooglePlaces
import CoreLocation
import Alamofire
import SDWebImage

class AddPropertyViewController: UIViewController, GalleryItemsDataSource, GalleryItemsDelegate, UITextViewDelegate {
    
    var titleType:AddPropertyVCType = AddPropertyVCType.add
    
    @IBOutlet var btnAmenities: [UIButton]!
    @IBOutlet weak var viewRoomDropDown: UIView!
    @IBOutlet weak var viewWCDropDown: UIView!
    @IBOutlet weak var viewPersonsDropDown: UIView!
    @IBOutlet weak var viewchildernDropDown: UIView!
    @IBOutlet weak var lblRoomCount: UILabel!
    @IBOutlet weak var lblWCCount: UILabel!
    @IBOutlet weak var lblPersonsCount: UILabel!
    @IBOutlet weak var lblChildernCount: UILabel!
    @IBOutlet weak var lblImagesCount: UILabel!
    @IBOutlet weak var viewImageBlur: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var tfName: CustomTextField!
    @IBOutlet weak var tfCity: CustomTextField!
    @IBOutlet weak var tfCountry: CustomTextField!
    @IBOutlet weak var tfPropertyType: UILabel!
    @IBOutlet weak var btnAddLocation: UIButton!
    @IBOutlet weak var txtAreaDescription: UITextView!
    @IBOutlet weak var txtAreaTerms: UITextView!
    @IBOutlet weak var btnAccpet: UIButton!
    @IBOutlet weak var tfRate: CustomTextField!
    @IBOutlet weak var heightConstraintOfTableView: NSLayoutConstraint!
    @IBOutlet weak var propertyTypeTopConstraint: NSLayoutConstraint!
    @IBOutlet var imgsUploadPictures:[UIImageView]!
    @IBOutlet weak var uploadPictureMainView1: UIView!
    @IBOutlet weak var uploadPictureMainView2: UIView!
    @IBOutlet weak var uploadPictureMainView3: UIView!
    @IBOutlet weak var uploadPictureMainView4: UIView!
    @IBOutlet weak var uploadPictureMainView5: UIView!
    @IBOutlet weak var uploadPictureMainView6: UIView!
    @IBOutlet weak var addLocationView: UIView!
    @IBOutlet weak var propertyTermsMainView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var tfPhoneNumber:CustomTextField!
    @IBOutlet weak var tfCurrencyType:UILabel!
    @IBOutlet weak var lblTermsAndCondition:UILabel!
    
    //MARK:-  Variables
    var hotelID:String?
    
    var propertyTypeStr:String = ""
    
    var Dates = [HotelDate]()
    var Images = [Image]()
    
    var dataImage: [GalleryItem] = [GalleryItem]()
    var galleryController:GalleryViewController?
    
    var btnAmenitiesTag = 1
    
    let dropDownRoom = DropDown()
    let dropDownWC = DropDown()
    let dropDownPersons = DropDown()
    let dropDownChildern = DropDown()
    let dropDownPropertyType = DropDown()
    let dropDownCurrencyType = DropDown()
    
    let ROWHEIGHT1:CGFloat = 50
    let ROWHEIGHT2:CGFloat = 240
    
    var location:(String,String,String,String,String)?
    var Address = ""
    
    var lat:Double?
    var long:Double?
    
    var property:PropertyModel?
    
    let amenitiesJsonArr:NSMutableArray = NSMutableArray()
    let amenitiesArr = ["wifi","desk","drinking","parking","spa","kitchen","gym","smartTv","pool","bonfire"]
    var amenitiesStatus = [false, false, false, false, false, false, false, false, false, false]
    
    //MARK:- Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        self.tfRate.keyboardType = .asciiCapableNumberPad
        self.tfPhoneNumber.keyboardType = .asciiCapableNumberPad
        
        mainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleOnMainViewTap)))

        let attriButtedStr1 = NSMutableAttributedString(string: NSLocalizedString("By Clicking this button you agree our ", comment: "By Clicking this button you agree our "), attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor : UIColor(red:0.29, green:0.33, blue:0.38, alpha:1.0)])
        
        let attriButtedStr2 = NSMutableAttributedString(string: NSLocalizedString("terms & conditions", comment: "terms & conditions"), attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor : UIColor(red:0.01, green:0.66, blue:0.96, alpha:1.0)])
        
        let combineStr = NSMutableAttributedString()
        combineStr.append(attriButtedStr1)
        combineStr.append(attriButtedStr2)
        
        self.lblTermsAndCondition.attributedText = combineStr
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc func handleOnMainViewTap(){
        self.view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == self.txtAreaDescription{
            if textView.text == DescriptionHere {
                textView.text = ""
                textView.textAlignment = .left
                textView.textColor = LIGHT_BLACK
            }
        }else{
            if textView.text == TermsandConditions {
                textView.text = ""
                textView.textAlignment = .left
                textView.textColor = LIGHT_BLACK
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == self.txtAreaDescription{
            if textView.text == "" {
                textView.text = DescriptionHere
                textView.textAlignment = .left
                textView.textColor = LIGHT_BLACK
            }
        }else{
            if textView.text == "" {
                textView.text = TermsandConditions
                textView.textAlignment = .left
                textView.textColor = LIGHT_BLACK
            }
        }
    }
    
    func setupData(){
        
        self.viewImageBlur.backgroundColor = UIColor.clear
        self.btnAmenities.forEach { (btn) in
            btn.layer.cornerRadius = btn.bounds.size.height / 2
            btn.backgroundColor = Am_Grey
            btn.addShadow1()
            btn.tag = btnAmenitiesTag
            btnAmenitiesTag += 1
        }
        
        txtAreaDescription.text = DescriptionHere
        txtAreaTerms.text = TermsandConditions
        
        self.txtAreaDescription.delegate = self
        self.txtAreaTerms.delegate = self
        self.viewRoomDropDown.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleRoomTap)))
        self.viewWCDropDown.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleWCTap)))
        self.viewPersonsDropDown.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlePersonsTap)))
        self.viewchildernDropDown.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleChildernTap)))
        self.lblImagesCount.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewImages)))
        self.tfPropertyType.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlePropertyTypeTap)))
        self.tfCurrencyType.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleCurrenyTypeTap)))
        self.navigationController?.isNavigationBarHidden = false
        
        if self.titleType == .add {
            self.navigationItem.title = AddProperty
        }else{
            self.navigationItem.title = EditProperty
            let item = UIButton(type: .custom)
            //item.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            item.setImage(#imageLiteral(resourceName: "rubbish-bin-delete-button (3)"), for: .normal)
            item.addTarget(self, action: #selector(hotelDelete), for: .touchUpInside)
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: item)
        }
        
//        self.setBackButton(withText: "")
        
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.heightConstraintOfTableView.constant = 0.0
        
        self.tableView.estimatedRowHeight = ROWHEIGHT1
        
        self.addingTapGestureToUploadPictures()
        
        self.btnAddLocation.setTitle(AddLocation, for: .normal)
        self.btnAddLocation.setTitleColor(APP_PRIMARY_COLOR, for: .normal)
        
        self.lblLocation.font = UIFont.boldSystemFont(ofSize: 13)
        
        self.addLocationView.addShadow()
        self.uploadPictureMainView1.addShadow()
        self.uploadPictureMainView2.addShadow()
        self.uploadPictureMainView3.addShadow()
        self.uploadPictureMainView4.addShadow()
        self.uploadPictureMainView5.addShadow()
        self.uploadPictureMainView6.addShadow()
        self.propertyTermsMainView.addShadow()
        self.descriptionView.addShadow()
        
        
        if let property = property{
            
            self.propertyTypeStr = property.type!
            
            self.tfName.text = property.name
            self.tfCity.text = property.city
            self.tfCountry.text = property.country
            
            switch self.propertyTypeStr{
            case "Camp":
                self.tfPropertyType.text = Camp
            case "Farm":
                self.tfPropertyType.text = Farm
            case "Chalet":
                self.tfPropertyType.text = Chalet
            default:
               self.tfPropertyType.text = Camp
            }
            self.tfCurrencyType.text = property.currency == "kwd" ? "KWD" : "USD"
            self.lblRoomCount.text = property.roomsNumber
            self.lblWCCount.text = property.wcNumber
            self.lblPersonsCount.text = property.personNumber
            self.lblChildernCount.text = property.childernNumber
            self.Dates = property.availableDate
            self.tfRate.text = property.rate
            self.txtAreaDescription.text  = property.description
            self.lat = property.latitude!
            self.long = property.longitude!
            self.lblLocation.text = property.address!
            
            if let conditions = property.conditions {
                self.txtAreaTerms.text = conditions
            }
            
            tableView.reloadData()
            
            for item in (property.pictures?.enumerated())!{
                SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string: item.element), options: [], progress: nil, completed: { (img, data, err, _) in
                    if let image = img {
                        self.Images.append(Image(image: image, url: item.element))
                        self.setupImage()
                    }
                })
            }
            
            for item in property.amenities.enumerated(){
                self.amenitiesStatus[item.offset] = item.element.status
            }
            
            self.setupAmenities()
            
            if self.Dates.count != 0 {
//                self.heightConstraintOfTableView.constant = (self.ROWHEIGHT1*CGFloat(self.Dates.count)) + 180.0
                self.tableView.constraints.forEach { (constraint) in
                    if constraint.firstAttribute == .height {
                        constraint.constant = self.ROWHEIGHT1*CGFloat(self.Dates.count)
                    }
                }
            }
            if let contact = property.contactInfo{
                tfPhoneNumber.text = contact
            }
        }
        
        self.txtAreaDescription.textColor = LIGHT_BLACK
    }
    
    @objc func hotelDelete(){
        
        let optionMenu = appDel.topViewController!.getAlertController(title: nil, message: AlertDelete)
        
        let deleteAction = UIAlertAction(title: Yes, style: .destructive) { (action) in
            guard let hotelId = self.property?.propertyId  else { return }
            
            let params = ["hotel_id":hotelId]
            
            let wait = self.addWaitSpinner()
            
            JSONRequest.makeRequest(kDeleteHotel, parameters: params) { (data, error) in
                if error == nil {
                    if let data = data as? [String:Any]{
                        if let err = data["error"] as? Bool, err{
                            self.removeWaitSpinner(waitView: wait)
                        }else{
                            if let _ = data["message"] as? String{
                                self.removeWaitSpinner(waitView: wait)
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                }else{
                    self.removeWaitSpinner(waitView: wait)
                }
                
            }
        }
        
        let cancelAction = UIAlertAction(title: No, style: .cancel, handler: nil)
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        
        appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func addingTapGestureToUploadPictures() {
        
        self.imgsUploadPictures.forEach { (img) in
            img.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onUploadPicturesClick(_:))))
            img.superview?.bringSubviewToFront(img)
            
            img.isUserInteractionEnabled = true
        }
        
        self.viewImageBlur.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onUploadPicturesClick(_:))))
        self.lblImagesCount.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onUploadPicturesClick(_:))))
    }
    
    @objc func onUploadPicturesClick(_ sender:UITapGestureRecognizer) {
        
        if let imgView = sender.view as? UIImageView {
            
            if imgView.image == #imageLiteral(resourceName: "photo-camera.png") {
                self.showPicker()
            }else{
                self.viewImages()
            }
        }else if let _ = sender.view as? UILabel {
            self.viewImages()
        }else if let view = sender.view, view == self.viewImageBlur {
            self.viewImages()
        }
    }
    
    @objc func viewImages(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageCollectionViewController") as! ImageCollectionViewController
        vc.Images = Images
        vc.callback = { imgs in
            
            if let images = imgs{
                
                self.Images.removeAll()
                self.Images = images
                
                for i in 1...5 { //where i > images.count {
                    if let imageView = self.view.viewWithTag(i) as? UIImageView{
                        imageView.contentMode = .center
                        imageView.image = #imageLiteral(resourceName: "photo-camera.png")
                    }
                }
                if images.count < 6 {
                    self.lblImagesCount.text = ""
                    if let imageView = self.view.viewWithTag(5) as? UIImageView{
                        imageView.sendSubviewToBack(self.viewImageBlur)
                    }
                    self.lblImagesCount.superview?.sendSubviewToBack(self.lblImagesCount)
                }
                self.setupImage()
            }else{
                self.Images.removeAll()
                
                for i in 1...5 {
                    if let imageView = self.view.viewWithTag(i) as? UIImageView{
                        imageView.contentMode = .center
                        imageView.image = #imageLiteral(resourceName: "photo-camera.png")
                    }
                }
                
                self.lblImagesCount.text = ""
                if let imageView = self.view.viewWithTag(5) as? UIImageView{
                    imageView.sendSubviewToBack(self.viewImageBlur)
                }
                self.lblImagesCount.superview?.sendSubviewToBack(self.lblImagesCount)
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
//        showImagesPreview(index: 0)
    }
    
    func validate() -> Bool{
        
        if let name = tfName.text, name.trim() == ""{
            self.displayMessage(EnterName)
            return false
        }
        
        if let address = lblLocation.text, address.trim() == ""{
            self.displayMessage(EnterLocation)
            return false
        }
        
        if let city = tfCity.text, city.trim() == ""{
            self.displayMessage(EnterCity)
            return false
        }
        
        if let country = tfCountry.text, country.trim() == ""{
            self.displayMessage(EnterCountry)
            return false
        }
        
        if let phoneNumber = self.tfPhoneNumber.text{
            if Int(phoneNumber.trim()) == nil || phoneNumber.trim() == ""{
                self.displayMessage(EnterNumber)
                return false
            }
        }
        
        if let type = tfPropertyType.text, type.trim() == ""{
            self.displayMessage(EnterProType)
            return false
        }
        
        if Images.count < 2 {
            self.displayMessage(SelectImage)
            return false
        }
        
        
        if let txt = txtAreaDescription.text{
            if txt.trim() == "" || txt.trim() == DescriptionHere{
                self.displayMessage(EnterDes)
                return false
            }
        }
        
        
        if let txt = txtAreaTerms.text{
            if txt.trim() == "" || txt.trim() == TermsandConditions{
                self.displayMessage(TermsandConditions)
                return false
            }
        }
        
        
        if let type = tfRate.text{
            if type.trim() == "" || Double(type.trim()) == nil{
                self.displayMessage(EnterPrice)
                return false
            }
        }
        
        if let value = tfCurrencyType.text {
            if value.trim() == ""{
                self.displayMessage(SelectCurrency)
                return false
            }
        }
        
        if btnAccpet.image(for: .normal) == UIImage(named: "success (4)") {
            self.displayMessage(SelectTerms)
            return false
        }
        
        if Dates.count < 1 {
            self.displayMessage(SelectDates)
            return false
        }
        
        return true
    }
    
    
    
    func setupAmenities(){
        for item in btnAmenities.enumerated(){
            let sender = item.element
            switch sender.tag {
            case 1:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "ic_wifi_gray"), for: .normal)
                    sender.backgroundColor = Am_Grey
                }else{
                    sender.setImage(UIImage(named: "ic_wifi_white"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
            case 2:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "desk_gray"), for: .normal)
                    sender.backgroundColor = Am_Grey
                }else{
                    sender.setImage(UIImage(named: "desk_white"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
            case 3:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "drink_gray"), for: .normal)
                    sender.backgroundColor = Am_Grey
                }else{
                    sender.setImage(UIImage(named: "drink_white"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 4:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "ic_parking_gray"), for: .normal)
                    sender.backgroundColor = Am_Grey
                    
                }else{
                    sender.setImage(UIImage(named: "ic_parking_white"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 5:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "ic_spa_gray"), for: .normal)
                    sender.backgroundColor = Am_Grey
                    
                }else{
                    sender.setImage(UIImage(named: "ic_spa_white"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                    
                }
                
            case 6:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "kitGrey"), for: .normal)
                    sender.backgroundColor = Am_Grey
                }else{
                    sender.setImage(UIImage(named: "Kit"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 7:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "gymGrey"), for: .normal)
                    sender.backgroundColor = Am_Grey
                    
                }else{
                    sender.setImage(UIImage(named: "gym"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 8:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "smartTVGrey"), for: .normal)
                    sender.backgroundColor = Am_Grey
                }else{
                    sender.setImage(UIImage(named: "smartTV"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 9:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "poolGrey"), for: .normal)
                    sender.backgroundColor = Am_Grey
                }else{
                    sender.setImage(UIImage(named: "pool"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 10:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "bonfireGrey"), for: .normal)
                    sender.backgroundColor = Am_Grey
                }else{
                    sender.setImage(UIImage(named: "bonfire"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
                
            default:
                print("Hello world")
                
            }
        }
    }
    
    func addPropertyToServer(isEnable:Bool){
        
        guard let userId = MyProfile.userId else{
            self.displayMessage("User Id not Found!")
            return
        }
        
        let wait = self.addWaitSpinner()
        
        // Uploading Images to AWS
        uploadImagesToAws(self.Images, folderName: appDel.hotelImagesFolder, randomName: randomString(length: 20)) { (urls) in
            
            self.removeWaitSpinner(waitView: wait)
            
            if let urls = urls {
                self.createHotel(imageUrls: urls, userId: userId, isEnable:isEnable)
            }
        }
    }
    
    func createHotel(imageUrls:[String], userId:String, isEnable:Bool) {
        
        for i in 0..<amenitiesStatus.count {
            let prod: NSMutableDictionary = NSMutableDictionary()
            prod.setValue(amenitiesArr[i], forKey: "name")
            prod.setValue(amenitiesStatus[i], forKey: "status")
            amenitiesJsonArr.add(prod)
        }
        
        
        //for dates
        let datesJsonArr:NSMutableArray = NSMutableArray()
        
        for item in Dates.enumerated() {
            let prod: NSMutableDictionary = NSMutableDictionary()
            prod.setValue("\( item.element.startDate + (TimeZone.current.secondsFromGMT() * 1000) )", forKey: "start_date")
            prod.setValue("\( item.element.endDate + (TimeZone.current.secondsFromGMT() * 1000) )", forKey: "end_date")
            prod.setValue("\( item.element.nightRate )", forKey: "night_rate")
            datesJsonArr.add(prod)
        }
        
        let api:String
        var params = [String:Any]()
        
        if self.titleType == .add{
            
            api = kAddProperty
            
            params = ["name": tfName.text!.capitalized,
                      "type": self.propertyTypeStr,
                      "city": tfCity.text!.capitalized,
                      "country": tfCountry.text!.capitalized,
                      "rooms_number": lblRoomCount.text!,
                      "wc_number":lblWCCount.text!,
                      "person_number":lblPersonsCount.text!,
                      "children":lblChildernCount.text!,
                      "description":txtAreaDescription.text!,
                      "rate":tfRate.text!,
                      "available_date": datesJsonArr,
                      "owner_id": userId,
                      "longitude":long!,
                      "latitude":lat!,
                      "pictures":imageUrls.joined(separator: ","),
                      "amenities": amenitiesJsonArr,
                      "conditions":txtAreaTerms.text!,
                      "is_enable": isEnable,
                      "is_approved": false,
                      "address":lblLocation.text!,
                      "contact_info":tfPhoneNumber.text!,
                      "api_key":"T0BAnEzf6CaWwIMulImtA05nT6fDVjXKHoJM136S",
                      "currency":tfCurrencyType.text!.lowercased()
                ] as [String:Any]
            
        }else{
            
            guard let propertyId = property?.propertyId else { return }
            
            api = kUpdateProperty
            params = ["name": tfName.text!.capitalized,
                      "type": self.propertyTypeStr,
                      "city": tfCity.text!.capitalized,
                      "country": tfCountry.text!.capitalized,
                      "rooms_number": lblRoomCount.text!,
                      "wc_number":lblWCCount.text!,
                      "person_number":lblPersonsCount.text!,
                      "children":lblChildernCount.text!,
                      "rate":tfRate.text!,
                      "available_date": datesJsonArr,
                      "longitude":long!,
                      "latitude":lat!,
                      "pictures":imageUrls.joined(separator: ","),
                      "amenities": amenitiesJsonArr,
                      "conditions":txtAreaTerms.text!,
                      "is_enable": isEnable,
                      "is_approved": false,
                      "hotel_id":propertyId,
                      "address":lblLocation.text!,
                      "contact_info":tfPhoneNumber.text!,
                      "api_key":"T0BAnEzf6CaWwIMulImtA05nT6fDVjXKHoJM136S",
                      "currency":tfCurrencyType.text!.lowercased()
                ] as [String:Any]
        }
        
        print(params)
        
        let data = try? JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        var request = URLRequest(url: URL(string: api)!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = data
        
        let waitView = self.addWaitSpinner()
        
        Alamofire.request(request).responseJSON { (dataResponse) in
            print(dataResponse)
            self.removeWaitSpinner(waitView: waitView)
            if let data = dataResponse.result.value as? [String:Any]{
                if let error = data["error"] as? Bool, error{
                    if let message = data["message"] as? String{
                        print(message)
                    }
                }else{
                    if let message = data["message"] as? String{
                        self.displayMessage(message, callback: { () -> (Void) in
                            self.navigationController?.popViewController(animated: true)
                            MyProfile.canSale = true
                        })
                    }
                }
            }else{
                print("1")
                
            }
        }
    }
    
    
    //MARK:- Drop Down Hanlder
    @objc func handleRoomTap() {
        dropDownRoom.anchorView = viewRoomDropDown
        dropDownRoom.dataSource = [One,Two,Three,Four,Five]
        dropDownRoom.direction = .any
        dropDownRoom.width = viewRoomDropDown.frame.width
        dropDownRoom.bottomOffset = CGPoint(x: 0, y:(dropDownRoom.anchorView?.plainView.bounds.height)!)
        dropDownRoom.topOffset = CGPoint(x: 0, y:-(dropDownRoom.anchorView?.plainView.bounds.height)!)
        dropDownRoom.show()
        dropDownRoom.backgroundColor = .white
        dropDownRoom.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblRoomCount.text = item
        }
    }
    
    @objc func handleWCTap() {
        dropDownWC.anchorView = viewWCDropDown
        dropDownWC.dataSource = [One,Two,Three,Four,Five]
        dropDownWC.direction = .any
        dropDownWC.width = viewWCDropDown.frame.width
        dropDownWC.bottomOffset = CGPoint(x: 0, y:(dropDownWC.anchorView?.plainView.bounds.height)!)
        dropDownWC.topOffset = CGPoint(x: 0, y:-(dropDownWC.anchorView?.plainView.bounds.height)!)
        dropDownWC.show()
        dropDownWC.backgroundColor = .white
        dropDownWC.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblWCCount.text = item
        }
    }
    
    @objc func handlePersonsTap() {
        dropDownPersons.anchorView = viewPersonsDropDown
        dropDownPersons.dataSource = [One,Two,Three,Four,Five]
        dropDownPersons.direction = .any
        dropDownPersons.width = viewPersonsDropDown.frame.width
        dropDownPersons.bottomOffset = CGPoint(x: 0, y:(dropDownPersons.anchorView?.plainView.bounds.height)!)
        dropDownPersons.topOffset = CGPoint(x: 0, y:-(dropDownPersons.anchorView?.plainView.bounds.height)!)
        dropDownPersons.show()
        dropDownPersons.backgroundColor = .white
        dropDownPersons.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblPersonsCount.text = item
        }
    }
    
    @objc func handleChildernTap() {
        dropDownChildern.anchorView = viewchildernDropDown
        dropDownChildern.dataSource = [One,Two,Three,Four,Five]
        dropDownChildern.direction = .any
        dropDownChildern.width = viewchildernDropDown.frame.width
        dropDownChildern.bottomOffset = CGPoint(x: 0, y:(dropDownChildern.anchorView?.plainView.bounds.height)!)
        dropDownChildern.topOffset = CGPoint(x: 0, y:-(dropDownChildern.anchorView?.plainView.bounds.height)!)
        dropDownChildern.show()
        dropDownChildern.backgroundColor = .white
        dropDownChildern.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblChildernCount.text = item
        }
    }
    
    @objc func handlePropertyTypeTap() {
        dropDownPropertyType.anchorView = self.tfPropertyType.superview
        dropDownPropertyType.dataSource = [Camp, Farm, Chalet]
        dropDownPropertyType.direction = .any
        dropDownPropertyType.width = self.tfPropertyType.superview?.frame.width
        dropDownPropertyType.bottomOffset = CGPoint(x: 0, y:(dropDownPropertyType.anchorView?.plainView.bounds.height)!)
        dropDownPropertyType.topOffset = CGPoint(x: 0, y:-(dropDownPropertyType.anchorView?.plainView.bounds.height)!)
        dropDownPropertyType.show()
        dropDownPropertyType.backgroundColor = .white
        dropDownPropertyType.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfPropertyType.text = item
            if index == 0{
                self.propertyTypeStr =  "Camp"
            }else if index == 1{
                self.propertyTypeStr = "Farm"
            }else{
                self.propertyTypeStr = "Chalet"
            }
        }
    }
    
    @objc func handleCurrenyTypeTap() {
        dropDownCurrencyType.anchorView = self.tfCurrencyType.superview
        dropDownCurrencyType.dataSource = ["USD", "KWD"]
        dropDownCurrencyType.direction = .any
        dropDownCurrencyType.width = self.tfCurrencyType.superview?.frame.width
        dropDownCurrencyType.bottomOffset = CGPoint(x: 0, y:(dropDownCurrencyType.anchorView?.plainView.bounds.height)!)
        dropDownCurrencyType.topOffset = CGPoint(x: 0, y:-(dropDownCurrencyType.anchorView?.plainView.bounds.height)!)
        dropDownCurrencyType.show()
        dropDownCurrencyType.backgroundColor = .white
        dropDownCurrencyType.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfCurrencyType.text = item
        }
    }
    
    
    //MARK:- IBAction
    @IBAction func onAddImageClick(_ sender: Any) {
        if Images.count < 10{
            self.showPicker()
        }else{
            self.viewImages()
        }
    }
    
    @IBAction func onAmenitiesClick(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            if sender.backgroundColor == APP_PRIMARY_COLOR {
                sender.setImage(UIImage(named: "ic_wifi_gray"), for: .normal)
                sender.backgroundColor = Am_Grey
                self.amenitiesStatus[sender.tag - 1] = false
            }else{
                sender.setImage(UIImage(named: "ic_wifi_white"), for: .normal)
                sender.backgroundColor = APP_PRIMARY_COLOR
                self.amenitiesStatus[sender.tag - 1] = true
            }
        case 2:
            if sender.backgroundColor == APP_PRIMARY_COLOR {
                sender.setImage(UIImage(named: "desk_gray"), for: .normal)
                sender.backgroundColor = Am_Grey
                self.amenitiesStatus[sender.tag - 1] = false
            }else{
                sender.setImage(UIImage(named: "desk_white"), for: .normal)
                sender.backgroundColor = APP_PRIMARY_COLOR
                self.amenitiesStatus[sender.tag - 1] = true
            }
        case 3:
            if sender.backgroundColor == APP_PRIMARY_COLOR {
                sender.setImage(UIImage(named: "drink_gray"), for: .normal)
                sender.backgroundColor = Am_Grey
                self.amenitiesStatus[sender.tag - 1] = false
            }else{
                sender.setImage(UIImage(named: "drink_white"), for: .normal)
                sender.backgroundColor = APP_PRIMARY_COLOR
                self.amenitiesStatus[sender.tag - 1] = true
            }
            
        case 4:
            if sender.backgroundColor == APP_PRIMARY_COLOR {
                sender.setImage(UIImage(named: "ic_parking_gray"), for: .normal)
                sender.backgroundColor = Am_Grey
                self.amenitiesStatus[sender.tag - 1] = false
                
            }else{
                sender.setImage(UIImage(named: "ic_parking_white"), for: .normal)
                sender.backgroundColor = APP_PRIMARY_COLOR
                self.amenitiesStatus[sender.tag - 1] = true
            }
            
        case 5:
            if sender.backgroundColor == APP_PRIMARY_COLOR {
                sender.setImage(UIImage(named: "ic_spa_gray"), for: .normal)
                sender.backgroundColor = Am_Grey
                self.amenitiesStatus[sender.tag - 1] = false
                
            }else{
                sender.setImage(UIImage(named: "ic_spa_white"), for: .normal)
                sender.backgroundColor = APP_PRIMARY_COLOR
                self.amenitiesStatus[sender.tag - 1] = true
            }
            
        case 6:
            if sender.backgroundColor == APP_PRIMARY_COLOR {
                sender.setImage(UIImage(named: "kitGrey"), for: .normal)
                sender.backgroundColor = Am_Grey
                self.amenitiesStatus[sender.tag - 1] = false
                
            }else{
                sender.setImage(UIImage(named: "Kit"), for: .normal)
                sender.backgroundColor = APP_PRIMARY_COLOR
                self.amenitiesStatus[sender.tag - 1] = true
            }
            
        case 7:
            if sender.backgroundColor == APP_PRIMARY_COLOR {
                sender.setImage(UIImage(named: "gymGrey"), for: .normal)
                sender.backgroundColor = Am_Grey
                self.amenitiesStatus[sender.tag - 1] = false
                
            }else{
                sender.setImage(UIImage(named: "gym"), for: .normal)
                sender.backgroundColor = APP_PRIMARY_COLOR
                self.amenitiesStatus[sender.tag - 1] = true
            }
            
        case 8:
            if sender.backgroundColor == APP_PRIMARY_COLOR {
                sender.setImage(UIImage(named: "smartTVGrey"), for: .normal)
                sender.backgroundColor = Am_Grey
                self.amenitiesStatus[sender.tag - 1] = false
            }else{
                sender.setImage(UIImage(named: "smartTV"), for: .normal)
                sender.backgroundColor = APP_PRIMARY_COLOR
                self.amenitiesStatus[sender.tag - 1] = true
            }
            
        case 9:
            if sender.backgroundColor == APP_PRIMARY_COLOR {
                sender.setImage(UIImage(named: "poolGrey"), for: .normal)
                sender.backgroundColor = Am_Grey
                self.amenitiesStatus[sender.tag - 1] = false
            }else{
                sender.setImage(UIImage(named: "pool"), for: .normal)
                sender.backgroundColor = APP_PRIMARY_COLOR
                self.amenitiesStatus[sender.tag - 1] = true
            }
            
        case 10:
            if sender.backgroundColor == APP_PRIMARY_COLOR {
                sender.setImage(UIImage(named: "bonfireGrey"), for: .normal)
                sender.backgroundColor = Am_Grey
                self.amenitiesStatus[sender.tag - 1] = false
            }else{
                sender.setImage(UIImage(named: "bonfire"), for: .normal)
                sender.backgroundColor = APP_PRIMARY_COLOR
                self.amenitiesStatus[sender.tag - 1] = true
            }
            
            
        default:
            print("Hello world")
            
        }
    }
    
    @IBAction func onAddDateClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddDateViewController") as! AddDateViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        vc.Dates = self.Dates
        vc.callback = { hoteldate in
            self.Dates.append(hoteldate)
            self.tableView.reloadData()
//            self.heightConstraintOfTableView.constant = (self.ROWHEIGHT1*CGFloat(self.Dates.count)) + 180.0
            self.tableView.constraints.forEach { (constraint) in
                if constraint.firstAttribute == .height {
                    constraint.constant = self.ROWHEIGHT1*CGFloat(self.Dates.count)
                }
            }
        }
        let nvc = UINavigationController(rootViewController: vc)
        nvc.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        nvc.modalTransitionStyle = .crossDissolve
        nvc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(nvc, animated: true, completion: nil)
    }
    
    @IBAction func onSavedClick(_ sender: Any) {
        if validate(){
            self.addPropertyToServer(isEnable: false)
        }
    }
    
    @IBAction func onPostClick(_ sender: Any) {
        if validate(){
            self.addPropertyToServer(isEnable: true)
        }
    }
    
    @IBAction func onAddLocationClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GoogleMapsViewController") as! GoogleMapsViewController
        vc.callback = {  lat,long,address,city,country in
            print("\(lat)    \(long)   \(address)")
            self.lat = lat
            self.long = long
            self.lblLocation.text = address
            self.tfCity.text = city
            self.tfCity.showTitleLable(isHide: false)
            self.tfCountry.text = country
            self.tfCountry.showTitleLable(isHide: false)
            self.lblLocation.setNeedsLayout()
            self.lblLocation.layoutIfNeeded()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onAcceptTermsAndConditionClick(_ sender:UIButton) {
        if sender.image(for: .normal) == UIImage(named:"success (4)") {
            sender.setImage(#imageLiteral(resourceName: "success (3)") , for: .normal)
        }else{
            sender.setImage(UIImage(named:"success (4)") , for: .normal)
        }
    }
    
    @IBAction func onTermsAndConditionClick(_ sender:UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController
        self.navigationController?.pushViewController(vc! , animated: true)
    }
    
    //MARK:- Image Piker
    func showPicker() {
        
        var config = YPImagePickerConfiguration()
        
        /* Choose what media types are available in the library. Defaults to `.photo` */
        config.library.mediaType = .photo
        
        /* Enables selecting the front camera by default, useful for avatars. Defaults to false */
        config.usesFrontCamera = true
        
        config.shouldSaveNewPicturesToAlbum = false
        
        /* Defines which screen is shown at launch. Video mode will only work if `showsVideo = true`.
         Default value is `.photo` */
        config.startOnScreen = .library
        
        /* Defines which screens are shown at launch, and their order.
         Default value is `[.library, .photo]` */
        config.screens = [.library, .photo ] //, .video]
        
        /* Defines the time limit for recording videos.
         Default is 30 seconds. */
        //        config.video.recordingTimeLimit = 120.0
        
        /* Defines the time limit for videos from the library.
         Defaults to 60 seconds. */
        //        config.video.libraryTimeLimit = 120.0
        
        /* Adds a Crop step in the photo taking process, after filters. Defaults to .none */
        config.showsCrop = .rectangle(ratio: (16/9))
        
        /* Customize wordings */
        config.wordings.libraryTitle = NSLocalizedString("Select Media", comment: "Select Media")
        config.wordings.save = NSLocalizedString("Next", comment: "Next")
        
        config.colors.tintColor = APP_PRIMARY_COLOR //.white
        
        config.colors.multipleItemsSelectedCircleColor = APP_PRIMARY_COLOR
        
        /* Defines if the status bar should be hidden when showing the picker. Default is true */
        config.hidesStatusBar = false
        
        config.library.maxNumberOfItems = 10 - Images.count
        
        config.library.minNumberOfItems = 1
        
        /* Skip selection gallery after multiple selections */
        config.library.skipSelectionsGallery = true
        
        config.showsPhotoFilters = false
        
        config.isScrollToChangeModesEnabled = false
        config.showsCrop = YPCropType.none
        
        let picker = YPImagePicker(configuration: config)
        
        /* Multiple media implementation */
        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            if cancelled {
                print("Picker was canceled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
            _ = items.map { print("🧀 \($0)") }
            
            let selectedItems = items
            
            var selectedImages = [Image]()
            
            for photos in selectedItems {
                
                switch photos {
                    
                case .photo(let photo):
                    let image = photo.image
                    selectedImages.append(Image(image: image, url: ""))
                    
                default:
                    print("Hello")
                }
            }
            
            if self.Images.isEmpty {
                self.Images = selectedImages
            }else{
                self.Images += selectedImages
            }
            
            self.setupImage() // (images: selectedImages)
            picker.dismiss(animated: true, completion: nil)
        }
        
        present(picker, animated: true, completion: nil)
        
    }
    
    func itemCount() -> Int {
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    func showImagesPreview(index:Int) {
        
        self.dataImage.removeAll()
        
        for image in self.Images{
            let galleryItem = GalleryItem.image { imageCompletion in
                imageCompletion(image.image)//.image)
            }
            self.dataImage.append(galleryItem)
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
        //self.dismiss(animated: true, completion: nil)
    }
    
    func setupImage() {
        if Images.count != 0{
            for i in 1...Images.count {
                if let imageView = self.view.viewWithTag(i) as? UIImageView{
                    imageView.contentMode = .scaleAspectFill
                    
                    if imageView.image != #imageLiteral(resourceName: "photo-camera") {
                        continue
                    }
                    
                    imageView.image = self.Images[i-1].image
                    
                    self.viewImageBlur.backgroundColor = UIColor.clear
                    if let image = self.imgsUploadPictures.first(where: { (img) -> Bool in
                        return img.tag == 5
                    }){
                        image.superview?.bringSubviewToFront(image)
                    }
                }
                else {
                    lblImagesCount.text = "+\(Images.count - 5)"
                    self.viewImageBlur.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 0.4)
                    //
                    self.viewImageBlur.superview?.bringSubviewToFront(viewImageBlur)
                    self.lblImagesCount.superview?.bringSubviewToFront(lblImagesCount)
                }
            }
        }
        
    }
}

//MARK:- Table View
extension AddPropertyViewController: UITableViewDataSource, UITableViewDelegate, DateCellExpandDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Dates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DateTableViewCell", for: indexPath) as! DateTableViewCell
        cell.date = Dates[indexPath.row]
        
        cell.indexPath = indexPath
        cell.delegate = self
        
        cell.dateExpandableView.fd_collapsed = !cell.isExpand
        
        cell.selectionStyle = .none
//        if cell.isExpand {
//            self.tableView.constraints.forEach { (constraint) in
//                if constraint.firstAttribute == .height {
//                    constraint.constant = self.ROWHEIGHT1*CGFloat(self.Dates.count) + 110
//                }
//            }
//        }else{
//            self.tableView.constraints.forEach { (constraint) in
//                if constraint.firstAttribute == .height {
//                    constraint.constant = self.ROWHEIGHT1*CGFloat(self.Dates.count)
//                }
//            }
//        }
        return cell
    }
    
    func expandDateTableViewCell(indexPath: IndexPath, isExpand: Bool) {
        let cell = self.tableView.cellForRow(at: indexPath) as! DateTableViewCell
        
        cell.isExpand = isExpand
        
        if cell.isExpand {
            self.tableView.separatorStyle = .none
            UIView.animate(withDuration: 0.3) {
                self.tableView.constraints.forEach { (constraint) in
                    if constraint.firstAttribute == .height {
                        constraint.constant = self.ROWHEIGHT1*CGFloat(self.Dates.count) + 110
                    }
                }
                self.view.setNeedsLayout()
            }
        }else{
            self.tableView.separatorStyle = .singleLine
            UIView.animate(withDuration: 0.3) {
                self.tableView.constraints.forEach { (constraint) in
                    if constraint.firstAttribute == .height {
                        constraint.constant = self.ROWHEIGHT1*CGFloat(self.Dates.count)
                    }
                }
                self.view.setNeedsLayout()
            }
        }
        
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        self.Dates.removeFirst()
        
        if self.Dates.count <= 0 { // need to chnage this
            self.heightConstraintOfTableView.constant = 0.0
        }
        
        self.tableView.reloadData()
    }
    
}

//MARK:- Extra
class Amenities{
    var name:String
    var status:Bool
    init(name:String,status:Bool) {
        self.name = name
        self.status = status
    }
}

class Image{
    var image:UIImage
    var imgUrl:String
    
    init(image:UIImage,url:String) {
        self.image = image
        self.imgUrl = url
    }
}


enum AddPropertyVCType {
    case edit
    case add
}
