//
//  IBANInfoViewController.swift
//  kashtahPORT
//
//  Created by mac on 18/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class IBANInfoViewController: UIViewController {

    @IBOutlet weak var tfCountry: CustomTextField!
    @IBOutlet weak var tfIBANNo: CustomTextField!
    @IBOutlet weak var tfBankName: CustomTextField!
    @IBOutlet weak var tfPhoneNumber: CustomTextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = IBANInformation
        self.setBackButton(withText: "")
        // Do any additional setup after loading the view.
        
        self.setUpData()
        tfPhoneNumber.keyboardType = .asciiCapableNumberPad
    }
    
    func setUpData() {
        if let ibanInfo = MyProfile.IBANInfo {
            self.tfCountry.text = ibanInfo.country
            self.tfIBANNo.text = ibanInfo.bankNumber
            self.tfBankName.text = ibanInfo.bankName
            self.tfPhoneNumber.text = ibanInfo.phoneNumber
            
            self.tfCountry.showTitleLable(isHide: false)
        }
    }
    
    @IBAction func onSaveClick(_ sender: Any) {
        if validate(){
            guard let userId = MyProfile.userId else { return }
            
            let params = ["user_id":userId,
                          "country_code":tfCountry.text!,
                          "phone":tfPhoneNumber.text!,
                          "bank_name":tfBankName.text!,
                          "iban":tfIBANNo.text!]
            
            JSONRequest.makeRequest(kUpdateIBAN, parameters: params) { (data, error) in
                if error == nil{
                    if let data = data as? [String:Any]{
                        if let err = data["error"] as? Bool, err{
                            self.displayMessage(Error)
                        }else{
                            if let data1 = data["data"] as? [String:Any] {
                                if let iban = data1["IBAN"] as? [String:Any] {
                                    MyProfile.IBANInfo = IBANModel(data: iban)
                                }
                                if let msg = data["message"] as? String{
                                    self.displayMessage(msg, callback: { () -> (Void) in
                                        self.navigationController?.popViewController(animated: true)
                                    })
                                }
                            }
                        }
                    }
                }else{
                    self.displayMessage(Error)
                }
            }
        }
    }
    
    func validate() -> Bool{
        
        if let value = self.tfCountry.text,value.trim() == ""{
            self.displayMessage(EnterCountryCode)
            return false
        }
        
        if let value = self.tfPhoneNumber.text,value.trim() == ""{
            self.displayMessage(EnterNumber)
            return false
        }
        
        if !isValidMobileNumber(tfPhoneNumber.text){
            self.displayMessage(ValidPhone)
            return false
        }
        
        if let value = self.tfBankName.text,value.trim() == ""{
            self.displayMessage(EnterBankName)
            return false
        }
        
        
        if let value = self.tfIBANNo.text,value.trim() == ""{
            self.displayMessage(EnterIBAN)
            return false
        }
        
        return true
    }
    
}
