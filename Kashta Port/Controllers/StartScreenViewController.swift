//
//  StartScreenViewController.swift
//  Kashta Port
//
//  Created by alienbrainz on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

//var badge = 0

class StartScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
    }
    
    func setupData(){
        self.navigationController?.isNavigationBarHidden = true
        DispatchQueue.main.asyncAfter(wallDeadline: .now()+0.8) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
            let nvc = UINavigationController(rootViewController: vc)
            self.present(nvc, animated: true, completion: nil)
            }
    }
    
    
    
}



