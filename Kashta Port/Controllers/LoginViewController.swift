//
//  LoginViewController.swift
//  Kashta Port
//
//  Created by mac on 24/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
//import AccountKit
import OneSignal

public typealias CallBack = ()->()

class LoginViewController: UIViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var tfEmail: CustomTextField!
    @IBOutlet weak var tfPassword: CustomTextField!
    
    var type:ControllerFrom = .Other
    
    //for account kit
//    var _accountKit: AccountKit!
    
    private var facebookData:[String:String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupData(){
        self.navigationItem.title = Login
        let item = UIButton(type: .custom)
        item.setImage(UIImage(named:"go_back"), for: .normal)
        item.addTarget(self, action: #selector(onBackClick), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: item)
    
//        self.setBackButton(withText: "", action: .dismiss)
    }
    
    @objc func onBackClick(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }

    @IBAction func onLoginClick(_ sender: Any) {
        if self.validate(){
            
            let parmas = ["email":tfEmail.text!,
                          "password":tfPassword.text!,
                          ]

            let wait = self.addWaitSpinner()
            
            JSONRequest.makeRequest(kLogin, parameters: parmas) { (data, error) in
                
                self.removeWaitSpinner(waitView: wait)
                if error == nil {
                    if let data = data as? [String:Any]{
                        if let err = data["error"] as? Bool, err{
                            if let msg = data["message"] as? String{
                                self.displayMessage(msg)
                            }
                        }else{
                            if let dataModel = data["userData"] as? [String:Any] {
                                MyProfile.set(rawData: dataModel)
                                MessageCenter.updateNotificationToken()
                                appDel.getNotificationsFromServer()
                                
                                if MyProfile.isNotificationEnabled{
                                    OneSignal.setSubscription(true)
                                }
                                
                                NotificationCenter.default.post(name: .UserLoginLogout, object: nil)
                                if self.type == .Home{
                                    self.dismiss(animated: true, completion: {
                                        if let vc = UIStoryboard(name: "AddProperty", bundle: nil).instantiateViewController(withIdentifier: "AddPropertyViewController") as? AddPropertyViewController{
                                            appDel.topViewController?.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    })
                                }else{
                                    self.navigationController?.dismiss(animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onRegisterClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onForgotPasswordClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onFBLoginClick(_ sender: Any) {
        self.facebookLogin()
    }
    
    func facebookLogin() {
        
        let fbLoginManager = LoginManager()
        
        fbLoginManager.loginBehavior = LoginBehavior.browser
        
        fbLoginManager.logIn(permissions: ["public_profile","email"], from: self) { (result, error) in
            
            if error == nil && result?.token != nil {
                
                // logged in
                let waitView = self.addWaitSpinner()
                
                self.getUserFacebookData(callback:{
                    
                    self.removeWaitSpinner(waitView: waitView)
                    
                    if self.facebookData != nil{
                        
                        //call server to register or login
                        print(self.facebookData)
                        
                        if let fb_id = self.facebookData["fb_id"]{
                            
                            let wait = self.addWaitSpinner()
                            
                            //call server to check the user alerady have facebook associated with his account
                            
                            let params = ["facebook_id":fb_id,
                                          "email":self.facebookData["usr_email"] ?? ""]
                            
                            JSONRequest.makeRequest(kFacebookLogin, parameters: params, callback: { (data, error) in
                                
                                self.removeWaitSpinner(waitView: wait)
                                
                                if let data = data as? [String:Any]{
                                    if let err = data["error"] as? Bool, !err {
                                        // login user
                                        if let user = data["data"] as? [String:Any] {
                                            MyProfile.set(rawData: user)
                                            MessageCenter.updateNotificationToken()
                                            MyProfile.isLogin = true
                                            NotificationCenter.default.post(name: .UserLoginLogout, object: nil)
                                            if self.type == .Home{
                                                self.dismiss(animated: true, completion: {
                                                    if let vc = UIStoryboard(name: "AddProperty", bundle: nil).instantiateViewController(withIdentifier: "AddPropertyViewController") as? AddPropertyViewController{
                                                        appDel.topViewController?.navigationController?.pushViewController(vc, animated: true)
                                                    }
                                                })
                                            }else{
                                                self.navigationController?.dismiss(animated: true, completion: nil)
                                            }
                                        }
                                    }else{
                                        if let msg = data["message"] as? String {
                                            if msg == "user not found" {
                                                // register user
                                                
                                                if let name = self.facebookData["name"] {
                                                    self.registerUser(name: name, email: self.facebookData["usr_email"], profileImage: self.facebookData["usr_profile_image"], facebookId: fb_id)
                                                }else{
                                                    self.logOutFacebook()
                                                    self.displayMessage(title: nil, msg: "Something went wrong. Please try again later ! [1]")
                                                }
                                            }else{
                                                self.logOutFacebook()
                                                self.displayMessage(title: nil, msg: "Something went wrong. Please try again later ![2]")
                                            }
                                        }
                                    }
                                }else{
                                    self.logOutFacebook()
                                    self.displayMessage(title: nil, msg: "The user name or password is incorrect. Try again[10]")
                                }
                            })
                        }
                        else{
                            self.logOutFacebook()
                            self.displayMessage(title: nil, msg: "The user name or password is incorrect. Try again[11]")
                            return
                        }
                    }
                    else{
                        self.logOutFacebook()
                    }
                })
            } else if error != nil {
                print(error)
                self.displayMessage(title: nil, msg: error!.localizedDescription)
            }
        }
    }
    
    func registerUser(name:String, email:String?, profileImage:String?, facebookId:String) {
        
        let regParams = [
            "name" : name,
            "email": email ?? "",
            "countryCode": "",
            "phone": "",
            "type": "Seller",
            "password": "",
            "profile_image_url": profileImage ?? "",
            "facebook_id":facebookId
        ]
        
        if let vc = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "PhoneNumberViewController") as? PhoneNumberViewController {
            vc.regParams = regParams
            vc.type = self.type
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func logOutFacebook() {
        if AccessToken.current != nil{
            let loginManager = LoginManager()
            loginManager.logOut() // this is an instance function
            AccessToken.current = nil
        }
    }
    
    func getUserFacebookData(callback:@escaping CallBack){
        
        let graphRequest : GraphRequest = GraphRequest(graphPath: "/me", parameters: ["fields":"id,name,email,picture.type(large)"])
        
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil) {
                // Process error
                self.displayMessage(title: nil, msg: error!.localizedDescription)
            }
            else {
                 print("fetched user: \(result)")
                
                if let result = result as? [String:Any]{
                    
                    //verified
                    if let value = result["verified"] as? String, value != "1"{
                        
                        self.displayMessage(title: nil, msg: "Unverified facebook account.")
                        callback()
                        return
                    }
                    
                    self.facebookData = [String:String]()
                    
                    //facebook id
                    if let value = result["id"] as? String{
                        
                        self.facebookData["fb_id"] = value
                    }
                    else{
                        self.facebookData["fb_id"] = ""
                    }
                    //name
                    if let value = result["name"] as? String{
                        
                        self.facebookData["name"] = value
                    }
                    else{
                        self.facebookData["name"] = ""
                    }
                    //email
                    if let value = result["email"] as? String{
                        
                        self.facebookData["usr_email"] = value
                    }
                    else{
                        self.facebookData["usr_email"] = ""
                    }
                    //ProfilePic
                    if let picture = result["picture"] as? [String:Any] {
                        if let data = picture["data"] as? [String:Any] {
                            if let url = data["url"] as? String{
                                self.facebookData["usr_profile_image"] = url
                            }else{
                                self.facebookData["usr_profile_image"] = ""
                            }
                        }else{
                            self.facebookData["usr_profile_image"] = ""
                        }
                    }else{
                        self.facebookData["usr_profile_image"] = ""
                    }
                }
                else{
                    self.displayMessage(title: nil, msg: "The user name or password is incorrect. Try again[12]")
                }
            }
            callback()
        })
    }
    
    func validate() -> Bool{
        
        if let email = tfEmail.text, email.trim() == ""{
            self.displayMessage(EnterEmail)
            return false
        }
        
        if !isValidEmailId(tfEmail.text){
            self.displayMessage(ValidEmail)
            return false
        }
        
        if let password = tfPassword.text,password.trim() == ""{
            self.displayMessage(EnterPass)
            return false
        }
        
        return true
    }
    
}

//MARK: AccountKit
//extension LoginViewController: AKFViewControllerDelegate {

    //To handle a successful login in Authorization Code mode:
    
//    func viewController(_ viewController: UIViewController & AKFViewController, didCompleteLoginWith code: String, state: String) {
//
//        print("did complete login with access token \(code) state \(state ?? "state nil")")
//
//        self._accountKit.requestAccount { [weak self] (account, error) in
//
//            if let error = error {
//                self?.displayMessage(title: nil, msg: error.localizedDescription)
//                self?.logOutFacebook()
//            } else {
//
//                if let phone = account?.phoneNumber?.phoneNumber{
//                    if let countryCode = account?.phoneNumber?.countryCode{
//                        var countryName = ""
//                        if let item = getCountryCode().first(where: { (key, value) -> Bool in
//                            if let value = value as? String{
//                                if value == "+\(countryCode)"{
//                                    return true
//                                }
//                            }
//                            return false
//                        }){
//                            countryName = item.key as! String
//                        }
//                        else{
//                            countryName = "Other"
//                        }
//
//                        self?._accountKit.logOut()
//
//                        UserDefaults.standard.set(countryName, forKey: "countyNameVal")
//                        UserDefaults.standard.set(countryCode, forKey: "countryCodeVal")
//                        UserDefaults.standard.set(phone, forKey: "phoneNumberVal")
//                        UserDefaults.standard.synchronize()
//
//                        self?.confirmPhoneNumber(countryCode: countryCode, phoneNumber: phone)
//
//                    }
//                }
//                self?.logOutFacebook()
//            }
//        }
//    }
//
//
//    func viewControllerDidCancel(_ viewController: UIViewController & AKFViewController) {
//        // ... handle user cancellation of the login process ...
//    }
//
//    func loginWithPhone(){
//
//        if _accountKit == nil {
//            _accountKit = AccountKit(responseType: .accessToken)
//        }
//
//        let inputState = UUID().uuidString
//        if let vc = (_accountKit?.viewControllerForPhoneLogin(with: nil, state: inputState)){
//            self.prepareLoginViewController(loginViewController: vc as! AKFViewController)
//            self.present(vc as UIViewController, animated: true, completion: nil)
//        }
//
//    }
//
//    func prepareLoginViewController(loginViewController: AKFViewController) {
//
//        loginViewController.delegate = self
//
//        //UI Theming - Optional
//        loginViewController.uiManager = SkinManager(skinType: .translucent, primaryColor: APP_PRIMARY_COLOR, backgroundImage: UIImage(named: "BeachHotal"), backgroundTint: BackgroundTint.black, tintIntensity: 0)
//
//        loginViewController.isSendToFacebookEnabled = true
//
//        loginViewController.isGetACallEnabled = true
//
//    }
    
    ///Request for Verification Code
//    func confirmPhoneNumber(countryCode:String, phoneNumber:String){
    
        //verify->bool    //false country_code->String phone->String
        
//        let parameter = ["country_code": countryCode,
//                         "phone": phoneNumber,
//                         "version":VERSION]
//
//        let waitView = addWaitSpinner()
//
//        JSONRequest.makeRequest(.checkPhone, parameters: parameter, callback: { [unowned self] data, error in
//
//            self.removeWaitSpinner(waitView: waitView)
//
//            if let data = data as? [String: Any]{
//                //Invalid email, OK, logout, email id not activated, invalid password
//                if let error = data["error"] as? Bool, !error{
//
//                    //Verify Success
//                    if let countryCode = UserDefaults.standard.value(forKey: "countryCodeVal") as? String{
//                        if let phoneNumber = UserDefaults.standard.value(forKey: "phoneNumberVal") as? String{
//                            if let country = UserDefaults.standard.value(forKey: "countyNameVal") as? String{
//
//                                if registrationData == nil{
//                                    registrationData = [String:String]()
//                                }
//                                registrationData["country"] = country
//                                registrationData["country_code"] = countryCode
//                                Defaults.setPhoneNumber(phone: phoneNumber.trim())
//
//                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewSignUpViewController")
//                                self.navigationController?.pushViewController(vc!, animated: true)
//
//                                return
//                            }
//                        }
//                    }
//
//                }
//                else if let msg = data["message"] as? String{
//                    //remove the cached values
//                    UserDefaults.standard.set(nil, forKey: "countyNameVal")
//                    UserDefaults.standard.set(nil, forKey: "countryCodeVal")
//                    UserDefaults.standard.set(nil, forKey: "phoneNumberVal")
//                    self.displayMessage(title: nil, msg: msg)
//                }
//                else{
//                    //remove the cached values
//                    UserDefaults.standard.set(nil, forKey: "countyNameVal")
//                    UserDefaults.standard.set(nil, forKey: "countryCodeVal")
//                    UserDefaults.standard.set(nil, forKey: "phoneNumberVal")
//                    self.displayMessage(title: nil, msg: "Failed to Check Phone Number with us. Please try again.")
//                }
//            }
//            else{
//                //remove the cached values
//                UserDefaults.standard.set(nil, forKey: "countyNameVal")
//                UserDefaults.standard.set(nil, forKey: "countryCodeVal")
//                UserDefaults.standard.set(nil, forKey: "phoneNumberVal")
//                self.displayMessage(title: nil, msg: "Failed to parse server response. Please try again.")
//            }
//        })
//    }
//}


enum ControllerFrom:String{
    case Home = "Home"
    case Other = "Other"
}
