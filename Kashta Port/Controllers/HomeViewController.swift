//
//  HomeViewController.swift
//  Kashta Port
//
//  Created by mac on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import CarbonKit

class HomeViewController: UIViewController,CarbonTabSwipeNavigationDelegate, UITextFieldDelegate  {

    @IBOutlet weak var viewForCarbonSwipe: UIView!
    @IBOutlet weak var searchBarView: UITextField!
    @IBOutlet weak var filterCount: UILabel!
    
    var isLogin:Bool = false
    
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        setupCarbonSwipe()
        searchBarView.delegate = self
        searchBarView.returnKeyType = .search
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleSearchHotelNoti(_:)), name: .SearchHotel, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDynamicLink), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        self.handleDynamicLink()
    }
    
    @objc func handleSearchHotelNoti(_ noti:Notification) {
        if let userInfo = noti.userInfo{
            if let type = userInfo["type"] as? String{
                if type == "Camp" {
                    self.carbonTabSwipeNavigation.setCurrentTabIndex(0, withAnimation: true)
                }else if type == "Farm"{
                    self.carbonTabSwipeNavigation.setCurrentTabIndex(1, withAnimation: true)
                }else if type == "Chalet"{
                    self.carbonTabSwipeNavigation.setCurrentTabIndex(2, withAnimation: true)
                }
            }
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        let city = textField.text!.trim()
        
            SearchHotel.reset()
            SearchHotel.city = city
            if (textField.text?.isEmpty)!{
                NotificationCenter.default.post(name: .SearchHotel, object: nil, userInfo: nil)
            }else{
                NotificationCenter.default.post(name: .SearchHotel, object: nil, userInfo: nil)
            }

        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.searchBarView.text = ""
        self.getFilterCount()
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CampViewController") as! CampViewController
            vc.controllerType = .Camp
            return vc
        }else if index == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CampViewController") as! CampViewController
            vc.controllerType = .Farm
            return vc
        }else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CampViewController") as! CampViewController
            vc.controllerType = .Chalet
            return vc
        }
    }
    
    func getFilterCount(){
        var count = 0
        
        if SearchHotel.type != "" {
            count = count + 1
        }
        if SearchHotel.city != "" {
            count = count + 1
        }
        if SearchHotel.country != "" {
            count = count + 1
        }
        if SearchHotel.startDate != "" && SearchHotel.endDate != "" {
            count = count + 1
        }
        if SearchHotel.room != "" {
            count = count + 1
        }
        if SearchHotel.child != "" {
            count = count + 1
        }
        if SearchHotel.wc != "" {
            count = count + 1
        }
        if SearchHotel.person != "" {
            count = count + 1
        }
        
        if count != 0 {
            self.filterCount.isHidden = false
            self.filterCount.text = "\(count)"
        }else{
            self.filterCount.isHidden = true
        }
        
        
        
    }
    
    @IBAction func onFilterClick(_ sender: Any) {
        if SearchHotel.country == "" {
            SearchHotel.city = ""
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onSellWithUsClick(_ sender: Any) {
        if MyProfile.isLogin{
            if let vc = UIStoryboard(name: "AddProperty", bundle: nil).instantiateViewController(withIdentifier: "AddPropertyViewController") as? AddPropertyViewController{
                appDel.topViewController?.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let vc = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            vc.type = .Home
            let nvc = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nvc, animated: true, completion: nil)
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func setupCarbonSwipe() {
        
        _ = UIFont(name: "Roboto-Regular", size: 16)
        
        let items = [Camp, Farm, Chalet]
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: viewForCarbonSwipe)
        let widthOfTabIcons = self.view.frame.width/3
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 2)
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.backgroundColor = UIColor.white
        carbonTabSwipeNavigation.setNormalColor(UIColor.gray,font: UIFont(name: "Roboto-Regular", size: 16)!)
        carbonTabSwipeNavigation.setIndicatorColor(APP_PRIMARY_COLOR)
        carbonTabSwipeNavigation.setSelectedColor(APP_PRIMARY_COLOR,font: UIFont(name: "Roboto-Regular", size: 16)!)
    }
    
    @objc func handleDynamicLink() {
        
        if let str = appDel.dynamicLink {
            appDel.dynamicLink = nil
            
            if let url = URL(string: str)?.queryParameters {
                if let hotelId = url["post_id"] {
                    self.getHotel(id: hotelId, callback: { (p) in
                        if let property = p{
                            if let _ = appDel.topViewController as? HotelDetailsViewController{
                                appDel.topViewController?.navigationController!.popViewController(animated: true)
                            }
                            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelDetailsViewController") as? HotelDetailsViewController{
                                vc.property = property
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }else{
                            self.displayMessage("Property Not Found")
                        }
                    })
                }
            }else{
                print("No Dynamic link list")
            }
        }
    }
    
    func getHotel(id:String,callback:@escaping ((PropertyModel?)->())){
        
        let params = ["hotel_id":id]
        
        JSONRequest.makeRequest(kSingleProperty, parameters: params) { (data, error) in
            
            if error == nil {
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        print("Error")
                    }else{
                        if let _ = data["message"] as? String{
                            if let array = data["data"] as? [Any]{
                                for item in array{
                                    if let itemData = item as? [String:Any]{
                                        let hotel = PropertyModel(data: itemData)
                                        callback(hotel)
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                self.displayMessage("Property Not Found")
                callback(nil)
            }
        }
    }
    
}
