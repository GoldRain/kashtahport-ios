//
//  SucessFeedBackViewController.swift
//  Kashta Port
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class SucessFeedBackViewController: UIViewController {

    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    
    var isFeedBack:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let flag = isFeedBack{
            if flag{
                self.lblText.text = ProFeedSuccess
                self.btnDone.setTitle("Back To Home", for: .normal)
            }else{
                self.lblText.text = PaySuccess
                self.btnDone.setTitle("Done", for: .normal)
            }
        }
    }
    
    @IBAction func onBackToHomepageClick(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
            appDel.topViewController?.navigationController?.popToRootViewController(animated: true)
        })
    }
}
