//
//  GoogleMapsViewController.swift
//  kashtahPORT
//
//  Created by mac on 02/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class GoogleMapsViewController: UIViewController,CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    let regionRadius: CLLocationDistance = 300
    
    var marker:GMSMarker = {
        let mark = GMSMarker()
        mark.isDraggable = true
        return mark
    }()
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    
    var callback:((Double,Double,String,String,String)->())?
    
    var city = ""
    var country = ""
    var address = ""
    var lat:CLLocationDegrees?
    var long:CLLocationDegrees?

    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var btnAddLocation: UIButton!
    @IBOutlet weak var lblLocation: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = AddLocation
        self.setBackButton(withText: "")
        
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        viewMap.delegate = self
        
        marker.map = viewMap
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
    
        view.addSubview((searchController?.searchBar)!)
    
        definesPresentationContext = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        manager.stopUpdatingLocation()
        
        self.lat = locationManager.location!.coordinate.latitude
        self.long = locationManager.location!.coordinate.longitude
        let camera = GMSCameraPosition.camera(withLatitude: self.lat!, longitude: self.long!, zoom: 15.0)
        marker.position = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.long!)
        viewMap.camera = camera
        GMSGeocoder().reverseGeocodeCoordinate(marker.position) { (response, error) in
            if let addressComponent = response?.firstResult() {
                if let address = self.getAddress(addressCom: addressComponent) {
                    self.address = address
                    self.lblLocation.text = address
                    self.viewMap.bringSubviewToFront(self.viewLocation)
                }
            }
        }
    }
    
    @IBAction func onCurrentLocationClick(_ sender: Any) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                
                self.locationManager.requestWhenInUseAuthorization()
                
            case .authorizedAlways, .authorizedWhenInUse:
                guard let location = locationManager.location else{
                    return
                }
                self.lat = location.coordinate.latitude
                self.long = location.coordinate.longitude
                let camera = GMSCameraPosition.camera(withLatitude: self.lat!, longitude: self.long!, zoom: 15.0)
                marker.position = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.long!)
                viewMap.camera = camera
                GMSGeocoder().reverseGeocodeCoordinate(marker.position) { (response, error) in
                    if let addressComponent = response?.firstResult() {
                        if let address = self.getAddress(addressCom: addressComponent) {
                            self.address = address
                            self.lblLocation.text = address
                            self.viewMap.bringSubviewToFront(self.viewLocation)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onAddLocationClick(_ sender: Any) {

        callback!(self.lat!,self.long!,address,city,country)
        self.navigationController?.popViewController(animated: true)
    }
}

extension GoogleMapsViewController: GMSAutocompleteResultsViewControllerDelegate,GMSMapViewDelegate {
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        self.lat = place.coordinate.latitude
        self.long = place.coordinate.longitude
        let camera = GMSCameraPosition.camera(withLatitude: self.lat!, longitude: self.long!, zoom: 15.0)
        marker.position = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.long!)
        viewMap.camera = camera
        if let name = place.name{
            marker.title = name
        }
        
        print(place.addressComponents)
        
        let addresscomponet = place.addressComponents
        for item in addresscomponet!{
            let string = item.type
            if string == "country"{
                country = item.name
            }
            
            if string == "locality"{
                city = item.name
            }
        }
        let address = place.formattedAddress!
        self.address = address
        lblLocation.text = address
        self.viewMap.bringSubviewToFront(viewLocation)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        
        self.marker = marker
        self.lat = marker.position.latitude
        self.long = marker.position.longitude
        
        GMSGeocoder().reverseGeocodeCoordinate(marker.position) { (response, error) in
            if let addressComponent = response?.firstResult() {
                if let address = self.getAddress(addressCom: addressComponent) {
                    self.address = address
                    self.lblLocation.text = address
                    self.viewMap.bringSubviewToFront(self.viewLocation)
                }
            }
        }
    }

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.lat = coordinate.latitude
        self.long = coordinate.longitude
        marker.position = coordinate
        marker.map = viewMap
        GMSGeocoder().reverseGeocodeCoordinate(coordinate) { (response, error) in
            if let addressComponent = response?.firstResult() {
                if let address = self.getAddress(addressCom: addressComponent) {
                    self.address = address
                    self.lblLocation.text = address
                    self.viewMap.bringSubviewToFront(self.viewLocation)
                }
            }
        }
    }
    
    func getAddress(addressCom:GMSAddress) -> String?{
        print(addressCom )
        
        if let city = addressCom.locality{
            self.city = city
        }
        
        if let country = addressCom.country{
            self.country = country
        }
        
        if let line = addressCom.lines {
            if let address = line.first{
                return address
            }
        }
        return nil
    }
    
}

