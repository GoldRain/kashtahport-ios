//
//  HotelDetailsViewController.swift
//  Kashta Port
//
//  Created by mac on 20/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import Cosmos
import ImageViewer
import SDWebImage
import FirebaseDynamicLinks

class HotelDetailsViewController: UIViewController, UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, GalleryItemsDataSource, GalleryItemsDelegate  {
    
    //MARK:- Outlet
    @IBOutlet var btnAmenities: [UIButton]!
    @IBOutlet weak var imgViewBig: UIImageView!
    @IBOutlet weak var imgViewSmall: UIImageView!
    @IBOutlet weak var lblImageCount: UILabel!
    @IBOutlet weak var lblHotelTitle: UILabel!
    @IBOutlet weak var viewStarRating: CosmosView!
    @IBOutlet weak var lblHotelDec: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var bookNowMainView:UIView!
    @IBOutlet weak var ratingView:UIView!
    @IBOutlet weak var roomTypeView:UIView!
    @IBOutlet weak var dateView:UIView!
    @IBOutlet weak var amenitiesView:UIView!
    @IBOutlet weak var contactView:UIView!
    @IBOutlet weak var imagView:UIView!
    @IBOutlet weak var btnLike:UIButton!
    @IBOutlet weak var priceStarRating: CosmosView!
    @IBOutlet weak var deliveryStarRating: CosmosView!
    @IBOutlet weak var communicationStarRating: CosmosView!
    @IBOutlet weak var proficiencyStarRating: CosmosView!
    @IBOutlet weak var btnReadMore:UIButton!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var lblPirce: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblPhoneNumberSeller: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnBook:UIButton!
    @IBOutlet weak var btnCalender:UIButton!
    @IBOutlet weak var imgInContactUsSecondView: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet var contactUsToHotel: UIView!
    @IBOutlet var contactUsToCustomer: UIView!
    @IBOutlet weak var btnCancelButton: UIButton!
    @IBOutlet weak var btnMapButton: UIButton!
    
    @IBOutlet weak var lblContactToCustomer:UILabel!
    
    @IBOutlet weak var contactUsTopConstraint:NSLayoutConstraint!
    
    var fromMap:Bool = false
    
    //MARK:- Variable
    var dataImage: [GalleryItem] = [GalleryItem]()
    var galleryController:GalleryViewController?
    
    var userType:UserType = UserType.Buyer
    var fromVC:FromViewController = FromViewController.HomeVC
    
    var selectedPhoto = [UIImage]()
    var roomTypeArray = [RoomType]
    
    var dateFrom:Date?
    var dateTo:Date?
    
    var property:PropertyModel?
    var bookings:BookingModel?
    
    var dateRange = [Date]()
    
    var workItem:DispatchWorkItem?
    
    var btnAmenitiesTag = 1
    var amenitiesStatus = [false, false, false, false, false, false, false, false, false, false]
    
    var rate:Int = 0
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "E, MMM d"
        return formatter
    }()
    
    
    //MARK:- Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    @IBAction func share(_ sender:UIButton){
        self.share(id: self.property!.propertyId!)
    }
    
    
    
    func share(id:String){
        if let top = appDel.topViewController {
            
            let wait = top.addWaitSpinner()
            
            if let link = URL(string: "https://kashtahport.page.link?post_id=\(id)"){
                
                let dynamicLinksDomainURIPrefix = "https://kashtahport.page.link"
                
                let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
                linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.itwhiz4u.q8mercato.kashtahport")
                linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.itwhiz4u.q8mercato.kashtahport")
                linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
                linkBuilder!.socialMetaTagParameters?.title = self.property!.name!
                linkBuilder!.socialMetaTagParameters?.descriptionText = self.property!.description!
                linkBuilder!.socialMetaTagParameters?.imageURL = URL(string: self.property!.pictures!.first!)
                linkBuilder?.iOSParameters?.appStoreID = "1467932974"
                
                if let longDynamicLink = linkBuilder?.url{
                    
                    print("The long URL is: \(longDynamicLink)")
                    
                    linkBuilder?.shorten(completion: { (url, arr, error) in
                        if let error = error{
                            self.displayMessage(error.localizedDescription)
                            return
                        }
                        else if let url = url?.absoluteString{
                            
                            print("The short url is: \(url)")
                            
                            let textToShare = url
                            
                            let activityViewController = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
                            
                            activityViewController.popoverPresentationController?.sourceView = top.view // so that iPads won't crash
                            
                            top.present(activityViewController, animated: true, completion: {
                                top.removeWaitSpinner(waitView: wait)
                            })
                        }
                        
                    })
                    return
                }
            }
        }
    }
    
    
    
    func setupData(){
        
        if let property = property{
            
            
            if fromMap{
                btnMapButton.isHidden = true
            }else{
                btnMapButton.isHidden = false
            }
            
            
            self.navigationController?.isNavigationBarHidden = true
            pageControl.tintColor = UIColor.lightGray
            
            self.btnCancelButton.fd_collapsed = true
            
            if !MyProfile.isLogin{
                self.btnLike.isHidden = true
            }else{
                self.btnLike.isHidden = false
            }
            
            //Add Tag To Amenities Button
            
            self.btnAmenitiesTag = 1
            
            self.btnAmenities.forEach { (btn) in
                btn.layer.cornerRadius = btn.bounds.size.height / 2
                btn.backgroundColor = LIGHT_GREY
                btn.tag = btnAmenitiesTag
                btnAmenitiesTag += 1
            }
            
            self.imgViewSmall.roundedImage()
            self.imagView.roundedCorner()
            self.imgViewSmall.clipsToBounds = true
            
            // Add Shadow
            self.ratingView.addShadow(shadowColor: .gray, size: CGSize.zero, shadowOpacity: 0.2)
            self.roomTypeView.addShadow(shadowColor: .gray, size: CGSize.zero, shadowOpacity: 0.2)
            self.dateView.addShadow(shadowColor: .gray, size: CGSize.zero, shadowOpacity: 0.2)
            self.amenitiesView.addShadow(shadowColor: .gray, size: CGSize.zero, shadowOpacity: 0.2)
            self.contactView.addShadow(shadowColor: .gray, size: CGSize.zero, shadowOpacity: 0.2)
            
            self.lblImageCount.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewImages)))
            
            self.getRating()
            
            if let pictures = property.pictures{
                let imgUrl1 = pictures[0]
                self.imgViewBig.addWaitView()
                self.imgViewBig.sd_setImage(with: URL(string: imgUrl1)!) { (img, err, _, _) in
                    self.imgViewBig.removeWaitView()
                    if let image = img {
                        self.imgViewBig.image = image
                    }else{// placeholder image
                        self.imgViewBig.image = #imageLiteral(resourceName: "hotelPlaceholder")
                    }
                }
                
                let imgUrl2 = pictures[1]
                self.imgViewSmall.addWaitView()
                self.imgViewSmall.sd_setImage(with: URL(string: imgUrl2)!) { (img, err, _, _) in
                    self.imgViewSmall.removeWaitView()
                    if let image = img {
                        self.imgViewSmall.image = image
                    }else{// placeholder image
                        self.imgViewSmall.image = #imageLiteral(resourceName: "hotelPlaceholder")
                    }
                }
                self.imagView.isHidden = true
                if pictures.count > 2{
                    self.lblImageCount.text = "+ \(pictures.count - 2)"
                    self.imagView.isHidden = false
                }
                
            }
            
            if let propertyId = property.propertyId{
                if MessageCenter.isLiked(hotelId: propertyId) {
                    btnLike.setImage(UIImage(named: "like (4)"), for: .normal)
                }else{
                    btnLike.setImage(UIImage(named: "Image-2"), for: .normal)
                }
            }
            
            self.btnReadMore.isHidden = true
            
            if let des = property.description{
                if des.count > 250{
                    btnReadMore.isHidden = false
                }
            }
            
            for item in property.amenities.enumerated(){
                self.amenitiesStatus[item.offset] = item.element.status
            }
            
            if let name = property.name,let dec = property.description,let rating = property.rating,let address = property.address,let contactInfo = property.contactInfo{
                self.lblHotelTitle.text = name.capitalized
                self.lblHotelDec.text = dec
                self.viewStarRating.rating = rating
                self.lblAddress.text = address
                self.lblPhoneNumber.text = contactInfo
            }
            
            if self.fromVC == .HomeVC && self.userType == .Buyer {
                self.lblPirce.isHidden = false
//                self.lblPirce.text = "USD $\(property.rate!) / \(Night)"
                var currency = CurrentCurrency
                if let value = self.property?.currency, value != ""{
                    if value.lowercased() == "usd"{
                    }else{
                        currency = value
                    }
                }
                self.lblPirce.text = "\(currency.uppercased()) \(property.rate!) / \(Night)"
            }else{
                self.lblPirce.isHidden = true
            }
            
            setupAmenities()
            
            roomTypeArray.removeAll()
            roomTypeArray.append(RoomType)
            self.pageControl.numberOfPages = roomTypeArray.count
            
            collectionView.reloadData()
            
            if let bookingStatus = self.bookings?.status?.lowercased() {
                
                switch bookingStatus {
                    
                case "cancel" :
                    if self.fromVC == .OtherVC && self.userType == .Buyer {
                        self.setupStatusButton(title: Cancelled, textColor: .white, bgColor: .gray, isBookBtnBUserInteractionEnabled: false, isCancelButtonHidden: true)
                    }else if self.fromVC == .OtherVC && self.userType == .Seller{
                        self.setupStatusButton(title: Cancelled, textColor: .white, bgColor: .red, isBookBtnBUserInteractionEnabled: false, isCancelButtonHidden: true)
                    }
                    
                    
                    
                case "pending" :
                    if self.fromVC == .OtherVC && self.userType == .Buyer {
                        self.setupStatusButton(title: Cancel, textColor: .white, bgColor: .red, isBookBtnBUserInteractionEnabled: true, isCancelButtonHidden: true)
                    }else if self.fromVC == .OtherVC && self.userType == .Seller{
                        self.setupStatusButton(title: Accept, textColor: .white, bgColor: APP_PRIMARY_COLOR, isBookBtnBUserInteractionEnabled: true, isCancelButtonHidden: false)
                        self.btnCancelButton.setTitle(Reject, for: .normal)
                        self.btnCancelButton.setRedLayer()
                    }
                    
                    
                case "reject" :
                    
                    if self.fromVC == .OtherVC && self.userType == .Buyer {
                        self.setupStatusButton(title: Rejected, textColor: .white, bgColor: .gray, isBookBtnBUserInteractionEnabled: false, isCancelButtonHidden: true)
                    }else if self.fromVC == .OtherVC && self.userType == .Seller{
                        self.setupStatusButton(title: Rejected, textColor: .white, bgColor: .red, isBookBtnBUserInteractionEnabled: false, isCancelButtonHidden: true)
                    }
                    
                    
                case "confirm" :
                    if self.fromVC == .OtherVC && self.userType == .Buyer {
                        self.btnCancelButton.setRedLayer()
                        self.setupStatusButton(title: PayNow, textColor: .white, bgColor: APP_PRIMARY_COLOR, isBookBtnBUserInteractionEnabled: true, isCancelButtonHidden: false)
                    }else if self.fromVC == .OtherVC && self.userType == .Seller{
                        self.setupStatusButton(title: Accepted, textColor: .white, bgColor: APP_PRIMARY_COLOR, isBookBtnBUserInteractionEnabled: true, isCancelButtonHidden: true)
                    }
                    
                    
                case "paid":
                    if self.fromVC == .OtherVC && self.userType == .Buyer {
                        self.setupStatusButton(title: Paid, textColor: .white, bgColor: GREEN, isBookBtnBUserInteractionEnabled: false, isCancelButtonHidden: true)
                    }else if self.fromVC == .OtherVC && self.userType == .Seller{
                        self.setupStatusButton(title: Paid, textColor: .white, bgColor: GREEN, isBookBtnBUserInteractionEnabled: false, isCancelButtonHidden: true)
                    }
                    
                default:
                    print("DO NOTHING")
                }
            }
            
            if let booking = self.bookings{
                
                self.btnMapButton.isHidden = true
                self.btnCalender.isUserInteractionEnabled = false
                
                self.lblStartDate.text = formatter.string(from: Date(milliseconds: Int((bookings?.dateFrom)!)!))
                if let endDate = bookings?.dateTo{
                    if let intEndDate = Int(endDate){
                        self.lblEndDate.text = formatter.string(from: Date(milliseconds: intEndDate))
                    }
                }
            }
            
            if self.userType == .Buyer{ // For the End User Show Hotel Contact Info
//                self.contactView.addSubViewWithConstraints(view: self.contactUsToHotel)
                if let status = self.bookings?.status {
                    if status.lowercased() == "paid" {
                        self.contactView.addSubViewWithConstraints(view: self.contactUsToHotel)
                        self.contactUsTopConstraint.constant = 20.0
                        self.view.setNeedsLayout()
                    }else{
                        self.contactUsTopConstraint.constant = 0.0
                        self.view.setNeedsLayout()
                    }
                }else{
                    self.contactUsTopConstraint.constant = 0.0
                    self.view.setNeedsLayout()
                }
                if bookings == nil {
                    self.btnBook.setPrimaryLayer()
                    if appDel.currentAppleLanguage() == "ar" {
                        self.btnBook.contentHorizontalAlignment = .left
                        self.btnBook.titleEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
                    }else{
                        self.btnBook.contentHorizontalAlignment = .right
                        self.btnBook.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 15)
                    }
                }
                
            }else{ // For Hotel Owner Show Customer Contact Info
                self.contactView.addSubViewWithConstraints(view: self.contactUsToCustomer)
                if let buyer = bookings?.buyer{
                    if let imageUrl = buyer.profileImageUrl, imageUrl != "" {
                        self.imgInContactUsSecondView.sd_setImage(with: URL(string: imageUrl)!, placeholderImage: #imageLiteral(resourceName: "2019-05-18"), options: []) { (image, err, _, _) in
                            if image == nil {
                                self.imgInContactUsSecondView.image = #imageLiteral(resourceName: "2019-05-18")
                            }else{
                                self.imgInContactUsSecondView.image = image
                            }
                        }
                    }else{
                        self.imgInContactUsSecondView.image = #imageLiteral(resourceName: "2019-05-18")
                    }
                    self.lblUserName.text = buyer.name
                    if let name = buyer.name {
                        self.lblContactToCustomer.text = "Contact to \(name)"
                    }
                    self.lblEmail.text = buyer.email
                    self.lblPhoneNumberSeller.text = buyer.phone
                }
            }
        }else{
            self.navigationController?.popViewController(completion: {
                appDel.topViewController?.displayMessage(AlertNotFound)
            }, animation: true)
            return;
        }
        
    }
    
    func setupStatusButton(title: String, textColor: UIColor, bgColor: UIColor, isBookBtnBUserInteractionEnabled: Bool, isCancelButtonHidden:Bool) {
        
        self.btnCancelButton.fd_collapsed = isCancelButtonHidden
        self.btnCancelButton.isUserInteractionEnabled = !isCancelButtonHidden
        
        self.btnBook.setTitle(title, for: .normal)
        self.btnBook.setTitleColor(textColor, for: .normal)
        
        if bgColor == APP_PRIMARY_COLOR{
            self.btnBook.setPrimaryLayer()
        }else if bgColor == UIColor.red{
            self.btnBook.setRedLayer()
        }else if bgColor == UIColor.gray{
            self.btnBook.setGreyLayer()
        }else if bgColor == GREEN{
            self.btnBook.setGreenLayer()
        }else {
            self.btnBook.backgroundColor = bgColor
        }
        
        self.btnBook.isUserInteractionEnabled = isBookBtnBUserInteractionEnabled
    }
    
    func setupAmenities(){
        
        for item in btnAmenities.enumerated(){
            let sender = item.element
            switch sender.tag {
            case 1:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "ic_wifi_gray"), for: .normal)
                    sender.backgroundColor = LIGHT_GREY
                }else{
                    sender.setImage(UIImage(named: "ic_wifi_white"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
            case 2:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "desk_gray"), for: .normal)
                    sender.backgroundColor = LIGHT_GREY
                }else{
                    sender.setImage(UIImage(named: "desk_white"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
            case 3:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "drink_gray"), for: .normal)
                    sender.backgroundColor = LIGHT_GREY
                }else{
                    sender.setImage(UIImage(named: "drink_white"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 4:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "ic_parking_gray"), for: .normal)
                    sender.backgroundColor = LIGHT_GREY
                    
                }else{
                    sender.setImage(UIImage(named: "ic_parking_white"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 5:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "ic_spa_gray"), for: .normal)
                    sender.backgroundColor = LIGHT_GREY
                    
                }else{
                    sender.setImage(UIImage(named: "ic_spa_white"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                    
                }
                
            case 6:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "kitGrey"), for: .normal)
                    sender.backgroundColor = LIGHT_GREY
                }else{
                    sender.setImage(UIImage(named: "Kit"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 7:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "gymGrey"), for: .normal)
                    sender.backgroundColor = LIGHT_GREY
                    
                }else{
                    sender.setImage(UIImage(named: "gym"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 8:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "smartTVGrey"), for: .normal)
                    sender.backgroundColor = LIGHT_GREY
                }else{
                    sender.setImage(UIImage(named: "smartTV"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 9:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "poolGrey"), for: .normal)
                    sender.backgroundColor = LIGHT_GREY
                }else{
                    sender.setImage(UIImage(named: "pool"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
            case 10:
                if !self.amenitiesStatus[sender.tag - 1] {
                    sender.setImage(UIImage(named: "bonfireGrey"), for: .normal)
                    sender.backgroundColor = LIGHT_GREY
                }else{
                    sender.setImage(UIImage(named: "bonfire"), for: .normal)
                    sender.backgroundColor = APP_PRIMARY_COLOR
                }
                
                
            default:
                print("Hello world")
                
            }
        }
    }
    
    func getRating(){
        
        guard let id = property?.propertyId else { return }
        
        let params = ["hotel_id":id]
        
        JSONRequest.makeRequest(kRating, parameters: params) { (data, error) in
            if error == nil{
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        print("Error")
                    }else{
                        if let _ = data["message"] as? String{
                            if let arrData = data["data"] as? [Any]{
                                if let rawData = arrData.first as? [String:Any]{
                                    if let comm = rawData["avgCommunication"] as? Double,let price = rawData["avgPrice"] as? Double,let delivery = rawData["avgDelivery"] as? Double,let pro = rawData["avgProficiency"] as? Double {
                                        self.communicationStarRating.rating = comm
                                        self.priceStarRating.rating = price
                                        self.deliveryStarRating.rating = delivery
                                        self.proficiencyStarRating.rating = pro
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func viewImages(){
        self.showImagesPreview(index: 0)
        return;
    }
    
    func bookHotel(){
        
        if let property = property {
            let waitView = self.addWaitSpinner()
            
            let params = ["owner_id":property.ownerId!,
                          "hotel_id":property.propertyId!,
                          "buyer_id":MyProfile.userId!,
                          "room":property.roomsNumber!,
                          "adult":property.personNumber!,
                          "children":property.childernNumber!,
                          "date_from":"\(dateFrom!.millisecondsSince1970 + 36000000)",
                          "date_to":"\(dateTo!.millisecondsSince1970 + 36000000)",
                          "price":"\(rate)"
            ]
            
            JSONRequest.makeRequest(kCreateBooking, parameters: params) { (data, error) in
                self.removeWaitSpinner(waitView: waitView)
                if error == nil{
                    if let data = data as? [String:Any]{
                        if let err = data["error"] as? Bool, err{
                            print("Error")
                        }else{
                            if let msg = data["message"] as? String{
                                appDel.topViewController?.displayMessage(msg, callback: { () -> (Void) in
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    func cancelBooking(){
        if let property = bookings{
            if let id = property.bookingId{
                
                let wait = self.addWaitSpinner()
                
                MessageCenter.bookingUpdateStatus(id: id, status: "Cancel") { res , msg in
                    
                    self.removeWaitSpinner(waitView: wait)
                    
                    if !res{
                        self.bookings?.status = "Cancel"
                        NotificationCenter.default.post(name: .BookingRequestStatusUpdate, object: nil, userInfo: ["id":id])
                        self.setupData()
                    }else{
                        if let mssg = msg{
                            appDel.topViewController?.displayMessage(mssg)
                        }else{
                            appDel.topViewController?.displayMessage(FailedCancel)
                        }
                    }
                }
            }
        }
    }
    
    func payNow(){
        
        if let fullName = MyProfile.userName,let email = MyProfile.userEmail, let number = MyProfile.mobile, let price = bookings?.price {
            var firstName = ""
            var lastName = ""
            var components = fullName.components(separatedBy: " ")
            if(components.count > 0)
            {
                firstName = components.removeFirst()
                lastName = components.joined(separator: " ")
            }
            
            var currency = CurrencyType
            
            if let value = self.property?.currency, currency != ""{
                currency = value
            }
            
            _ = PaymentViewController.showPaymentWithData(amount: Decimal(string: price)!,
                                                          currency: currency.lowercased(),
                                                          customerFirstName: firstName,
                                                          customerLastName: lastName,
                                                          customerEmail: email,
                                                          customerPhone: number,
                                                          delegate: self)
        }else{
            self.displayMessage(ErrorPay)
        }
        
    }
    
    func validate() -> Bool{
        
        if dateFrom == nil{
            self.displayMessage(SelectStartDate)
            return false
        }
        
        if dateTo == nil{
            self.displayMessage(SelectEndDate)
            return false
        }
        
        return true
    }
    
    func validateAndCountPrice(){
        self.rate = 0
        for date in self.dateRange{
            for item in (property?.availableDate.enumerated())!{
                let hotelDate = item.element
                if (date.millisecondsSince1970 >= hotelDate.startDate) && (date.millisecondsSince1970 <= hotelDate.endDate){
                    if let price = Int(hotelDate.nightRate){
                        self.rate += price
                        break;
                    }
                    break;
                }
            }
        }
        
//        self.lblPirce.text = "USD $ \(rate/self.dateRange.count) / Night"
        
        var currency = CurrentCurrency
        if let value = self.property?.currency, value != ""{
            if value.lowercased() == "usd"{
            }else{
                currency = value
            }
        }
        self.lblPirce.text = "\(currency.uppercased()) \(rate/self.dateRange.count) / Night"
        self.btnBook.isUserInteractionEnabled = true
    }
    
    // MARK:- Outlet Action
    @IBAction func onCalanderClick(_ sender: Any) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CalenderViewController") as? CalenderViewController {
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            vc.dates = (self.property?.availableDate)!
            if let value = self.property?.currency, value != ""{
                if value.lowercased() == "usd"{
                }else{
                    vc.currency = value
                }
            }
            vc.type = .Book
            vc.callback = { dateRanges in
                self.dateRange = dateRanges
                self.dateFrom = self.dateRange.first
                self.dateTo = self.dateRange.last
                self.lblStartDate.text = self.formatter.string(from: self.dateFrom!)
                self.lblEndDate.text = self.formatter.string(from: self.dateTo!)
                self.validateAndCountPrice()
            }
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
        
//        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "HotelBookDateViewController") as? HotelBookDateViewController {
//            vc.modalTransitionStyle = .crossDissolve
//            vc.modalPresentationStyle = .overCurrentContext
//            vc.callback = { startDate,endDate,startDateStr,endDateStr in
//                if startDateStr != "From" && endDateStr != "To"{
//                    self.lblStartDate.text = startDateStr
//                    self.lblEndDate.text = endDateStr
//                    self.dateFrom = startDate
//                    self.dateTo = endDate
//                    self.validateAndCountPrice()
//                }
//            }
//            let nvc = UINavigationController(rootViewController: vc)
//            nvc.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
//            nvc.modalTransitionStyle = .crossDissolve
//            nvc.modalPresentationStyle = .overCurrentContext
//            self.navigationController?.present(nvc, animated: true, completion: nil)
//        }
    }
    
    @IBAction func onBookNowClick(_ sender: UIButton) {
        
        if !MyProfile.isLogin {
            if let vc = self.storyboard!.instantiateViewController(withIdentifier: "RegisterDialogViewController") as? RegisterDialogViewController {
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overCurrentContext
                self.navigationController?.present(vc, animated: true, completion: nil)
            }
            return;
        }
        
        guard let hotelId = property?.propertyId else {
            return
        }
        
        MessageCenter.isHotelDeleted(hotelId: hotelId) { (error) in
            if error{
               self.displayMessage(AlertNotFound)
            }else{
                if let btnTitle = sender.title(for: .normal) {
                    
                    if btnTitle == PayNow {
                        
                        self.payNow()
                        return;
                        
                    }
                        
                    else if btnTitle == Cancel {
                        
                        let optionMenu = appDel.topViewController!.getAlertController(title: nil, message: AlertCancel)
                        
                        let logoutAction = UIAlertAction(title: Yes, style: .destructive) { (action) in
                            
                            self.cancelBooking()
                            return;
                        }
                        
                        let cancelAction = UIAlertAction(title: No, style: .cancel, handler: nil)
                        
                        optionMenu.addAction(logoutAction)
                        optionMenu.addAction(cancelAction)
                        
                        appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
                        
                    }
                        
                    else if btnTitle == Accept {
                        
                        let optionMenu = appDel.topViewController!.getAlertController(title: nil, message: AlertAccept)
                        
                        let logoutAction = UIAlertAction(title: Yes, style: .default) { (action) in
                            if let id = self.bookings?.bookingId{
                                
                                MessageCenter.bookingUpdateStatus(id: id, status: "confirm"){ res , msg in
                                    
                                    if !res{
                                        self.bookings?.status = "confirm"
                                        NotificationCenter.default.post(name: .BookingRequestStatusUpdate, object: nil, userInfo: ["id":id])
                                        self.setupData()
                                    }else{
                                        if let mssg = msg{
                                            appDel.topViewController?.displayMessage(mssg)
                                        }else{
                                            appDel.topViewController?.displayMessage(FailedAccept)
                                        }
                                    }
                                }
                            }
                            return;
                        }
                        
                        let cancelAction = UIAlertAction(title: No, style: .destructive, handler: nil)
                        
                        optionMenu.addAction(logoutAction)
                        optionMenu.addAction(cancelAction)
                        
                        appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
                        
                    }
                        
                    else if btnTitle == RequestBooking{
                        if self.validate(){
                            
                            var currency = CurrentCurrency
                            if let value = self.property?.currency, value != ""{
                                if value.lowercased() == "usd"{
                                }else{
                                    currency = value
                                }
                            }
                            
                            let optionMenu = appDel.topViewController!.getAlertController(title: nil, message: "Book for \(currency.uppercased()) \(self.rate) !!")
                            
                            let logoutAction = UIAlertAction(title: Yes, style: .default) { (action) in
                                self.bookHotel()
                                return;
                            }
                            
                            let cancelAction = UIAlertAction(title: No, style: .destructive, handler: nil)
                            
                            optionMenu.addAction(logoutAction)
                            optionMenu.addAction(cancelAction)
                            
                            appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onCancelClick(_ sender: UIButton) {
        if let btnTitle = sender.title(for: .normal) {
            
            if btnTitle == Cancel {
                
                let optionMenu = appDel.topViewController!.getAlertController(title: nil, message: AlertCancel)
                
                let logoutAction = UIAlertAction(title: Yes, style: .destructive) { (action) in
                    self.bookings?.status = "Cancel"
                    self.cancelBooking()
                    return;
                }
                
                let cancelAction = UIAlertAction(title: No, style: .cancel, handler: nil)
                
                optionMenu.addAction(logoutAction)
                optionMenu.addAction(cancelAction)
                
                appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
                
            }
            
            else if  btnTitle == Reject {
                
                let optionMenu = appDel.topViewController!.getAlertController(title: nil, message: AlertReject)
                
                let logoutAction = UIAlertAction(title: Yes, style: .destructive) { (action) in
                    if let id = self.bookings?.bookingId{
                        
                        let wait = self.addWaitSpinner()
                        
                        MessageCenter.bookingUpdateStatus(id: id, status: "reject"){ res ,msg in
                            
                            self.removeWaitSpinner(waitView: wait)
                            
                            if !res{
                                self.bookings?.status = "reject"
                                NotificationCenter.default.post(name: .BookingRequestStatusUpdate, object: nil, userInfo: ["id":id])
                                self.setupData()
                            }else{
                                if let mssg = msg{
                                    appDel.topViewController?.displayMessage(mssg)
                                }else{
                                    appDel.topViewController?.displayMessage(FailedReject)
                                }
                            }
                        }
                    }
                    return;
                }
                
                let cancelAction = UIAlertAction(title: No, style: .cancel, handler: nil)
                
                optionMenu.addAction(logoutAction)
                optionMenu.addAction(cancelAction)
                
                appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
                
            }
        }
        
    }
    
    @IBAction func onMapClick(_ sender: Any) {
        if let vc = UIStoryboard(name: "Map", bundle: nil).instantiateViewController(withIdentifier: "MapViewController") as? MapViewController {
            vc.property = self.property
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCallClick(_ sender: Any) {
       
        if userType == .Buyer{
            if let number = self.lblPhoneNumber.text {
                openCallingApp(phoneNumber: number)
            }
        }else{
            if let number = self.lblPhoneNumberSeller.text{
                openCallingApp(phoneNumber: number)
            }
        }
    
    }
    
    @IBAction func onMailClick(_ sender: Any) {
        let email = self.lblEmail.text!
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    @IBAction func onReadMoreClick(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "DescriptionViewController") as? DescriptionViewController {
            vc.descriptionText = property?.description!
            vc.tiltetext = Description
            self.navigationController!.present(vc, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func onViewTermsClick(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "DescriptionViewController") as? DescriptionViewController {
            vc.descriptionText = property?.conditions
            vc.tiltetext = TermsandConditions
            self.navigationController!.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func onLikeClick(_ sender: UIButton) {
        
        workItem?.cancel()
        
        var isLiked:Bool = false
        
        if MyProfile.isLogin{
            if sender.image(for: .normal) == UIImage(named: "Image-2"){
                sender.setImage(UIImage(named: "like (4)"), for: .normal)
                isLiked = true
            }else{
                sender.setImage(UIImage(named: "Image-2"), for: .normal)
                isLiked = false
            }
        }else{
            self.displayMessage(AlertLogin)
        }
        
        workItem = DispatchWorkItem{
            if let hotelId = self.property?.propertyId{
                if isLiked{
                    MessageCenter.addFavouritesHotel(hotelId: hotelId) {
                        NotificationCenter.default.post(name: .HotelsFavouritesStatusUpdate, object: nil, userInfo: ["id":hotelId])
                    }
                }else{
                    MessageCenter.removeFavouritesHotel(hotelId: hotelId) {
                        NotificationCenter.default.post(name: .HotelsFavouritesStatusUpdate, object: nil, userInfo: ["id":hotelId])
                    }
                }
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: workItem!)
        
        
    }
    
    
    // MARK: Collection Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return roomTypeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RoomSwipeCollectionViewCell", for: indexPath) as! RoomSwipeCollectionViewCell
        cell.lblRoomType.text = roomTypeArray[indexPath.row]
        cell.lblRoomNumber.text = "\(property!.roomsNumber!) \(Room)"
        cell.lblChildrenNumber.text = "\(property!.childernNumber!) \(Children)"
        cell.lblAdultNumber.text = "\(property!.personNumber!) \(Adults)"
        cell.lblWCNumber.text =  "\(property!.wcNumber!) \(WC)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/scrollView.frame.width)
        self.pageControl.currentPage = Int(pageIndex)
    }
    
    // MARK:- Image preview
    func itemCount() -> Int {
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    func showImagesPreview(index:Int){
        
        self.dataImage.removeAll()
        
        for item in (property?.pictures?.enumerated())! {
            
            let img = item.element
            
            let galleryItem = GalleryItem.image { imageCompletion in
                
                SDWebImageDownloader.shared().downloadImage(with: URL(string: img), options: [], progress: nil, completed: { (img, data, err, fin) in
                    
                    if let image = img{
                        imageCompletion(image)
                    }
                })
            }
            
            self.dataImage.append(galleryItem)
        }
        
        guard dataImage.count > 0 else{
            if let top = appDel.topViewController{
                top.displayMessage("Image Not Found!!")
            }
            return
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
        //self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

//MARK:- Extra
enum UserType{
    case Seller
    case Buyer
}

enum FromViewController {
    case HomeVC
    case OtherVC
}

enum ButtonTitle:String{
    case Cancel = "Cancel"
    case BookNow = "Request Booking"
    case Accept = "Accept"
    case PayNow = "Pay Now"
    case Reject = "Reject"
}


//MARK:- PaymentViewControllerDeletgate
extension HotelDetailsViewController:PayementViewControllerDelegate {
    
    func paymentWebViewDidCancel(_ paymentViewController: PaymentViewController) {
        
        paymentViewController.dismiss(animated: true) {
            
        }
    }
    
    func paymentWebViewDidFinish(_ paymentViewController: PaymentViewController, paymentInfo: PaymentData) {
        
        paymentViewController.dismiss(animated: true) {
            if let booking = self.bookings{
                
                MessageCenter.paymentRequest(paymentData: paymentInfo, booking: booking, completion: { error in
                    
                    if !error {
                        MessageCenter.bookingUpdateStatus(id: (self.bookings?.bookingId!)!, status: "Paid", completion: {res,msg in
                            
                            if !res{
                                self.bookings?.status = "paid"
                                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SucessFeedBackViewController") as? SucessFeedBackViewController {
                                    self.navigationController?.present(vc, animated: true, completion: {
                                        NotificationCenter.default.post(name: .BookingRequestStatusUpdate, object: nil, userInfo: ["id":self.bookings!.bookingId!])
                                        self.setupData()
                                    })
                                }
                                
                            }else{
                                if let vc = UIStoryboard(name: "Transaction", bundle: nil).instantiateViewController(withIdentifier: "TransactionFailedViewController") as? TransactionFailedViewController {
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        })
                    }else{
                        if let vc = UIStoryboard(name: "Transaction", bundle: nil).instantiateViewController(withIdentifier: "TransactionFailedViewController") as? TransactionFailedViewController {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                })
            }
        }
    }
    
    func paymentWebViewDidFail(_ paymentViewController: PaymentViewController) {
        
        paymentViewController.dismiss(animated: true) {
            
        }
    }
}
