//
//  MapViewController.swift
//  Kashta Port
//
//  Created by mac on 20/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import MapViewPlus
import GoogleMaps

class MapViewController: UIViewController, MapViewPlusDelegate, CLLocationManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource  {

    let locationManager:CLLocationManager = CLLocationManager()
    let regionRadius: CLLocationDistance = 300
    
    var Locations = [PropertyModel]()
    
    var property:PropertyModel?
    
    @IBOutlet weak var mapView: MapViewPlus!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblResult: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.mapView.delegate = self
        
        let annotation = AnnotationPlus(viewModel: property! , coordinate: CLLocationCoordinate2D(latitude: (property?.latitude)!, longitude: (property?.longitude)!))
        var annotations : [AnnotationPlus] = []
        annotations.append(annotation)
        mapView.setup(withAnnotations: annotations)
        let initialLocation = CLLocation(latitude: (property?.latitude)!, longitude: (property?.longitude)!)
        centerMapOnLocation(location: initialLocation)
        
        self.getLocations()
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func onBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCurrentLocationClick(_ sender:UIButton) {
        self.mapView.showsUserLocation = true
        if let location = locationManager.location{
            centerMapOnLocation(location: location)
        }
    }
    
    func mapView(_ mapView: MapViewPlus, imageFor annotation: AnnotationPlus) -> UIImage {
        return UIImage(named: "placeholder3")!
    }
    
    func mapView(_ mapView: MapViewPlus, calloutViewFor annotationView: AnnotationViewPlus) -> CalloutViewPlus {
        let calloutView = Bundle.main.loadNibNamed("CustomViewForMap", owner: nil, options: nil)!.first as! CustomViewForMap
        return calloutView
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Locations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationCollectionViewCell", for: indexPath) as? LocationCollectionViewCell
        cell?.location = Locations[indexPath.row]
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let initialLocation = CLLocation(latitude: (Locations[indexPath.row].latitude)!, longitude: (Locations[indexPath.row].longitude)!)
        centerMapOnLocation(location: initialLocation)
        let annotation = AnnotationPlus(viewModel: Locations[indexPath.row], coordinate: CLLocationCoordinate2D(latitude: (Locations[indexPath.row].latitude)!, longitude: (Locations[indexPath.row].longitude)!))
        var annotations : [AnnotationPlus] = []
        annotations.append(annotation)
        mapView.setup(withAnnotations: annotations)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func checkLocationServiceEnabled(){
        if CLLocationManager.locationServicesEnabled(){
            setupLocationManager()
            checkForAccess()
        }else{
            
        }
    }
    
    func setupLocationManager(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func checkForAccess(){
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            break
        case .denied:
            break
        case .authorizedAlways:
            break
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region = MKCoordinateRegion(center: location, span: span)
        
        self.mapView.setRegion(region, animated: true)
        let annotationForCurrentUserLocation = MKPointAnnotation()
        annotationForCurrentUserLocation.title = CurrentLocation
        annotationForCurrentUserLocation.coordinate = location
        mapView.addAnnotation(annotationForCurrentUserLocation)
        
        
        self.locationManager.stopUpdatingLocation()
    }
    
    func getLocations(){
        if let lat = property?.latitude,let long = property?.longitude{
            let params = ["latitude": "\(lat)",
                          "longitude":"\(long)"]
            
            self.collectionView.addWaitView()
            
            JSONRequest.makeRequest(kNearByHotel, parameters: params) { (data, error) in
                
                self.collectionView.removeWaitView()
                
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        print("Error")
                    }else{
                        if let _ = data["message"] as? String{
                            if let data = data["data"] as? [Any]{
                                for item in data {
                                    if let rawData = item as? [String:Any] {
                                        let location = PropertyModel(data: rawData)
                                        self.Locations.append(location)
                                    }
                                }
                            }
                        }
                        self.collectionView.reloadData()
                        self.lblResult.text = "\(self.Locations.count) \(Results)"
                    }
                }
            }
        }
    }
}

extension PropertyModel:CalloutViewModel{
    
}
