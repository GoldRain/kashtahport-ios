//
//  AboutUsViewController.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = AboutUs
        self.setBackButton(withText: "")
        if appDel.currentAppleLanguage() == "ar" {
            self.webView.loadRequest(URLRequest(url: URL(string: kAboutUsAr)!))
        }else{
            self.webView.loadRequest(URLRequest(url: URL(string: kAboutUsEn)!))
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.view.addWaitView()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.view.removeWaitView()
    }

}
