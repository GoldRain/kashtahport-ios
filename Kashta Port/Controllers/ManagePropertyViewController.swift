//
//  ManagePropertyViewController.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class ManagePropertyViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    let refreshControl = UIRefreshControl()
    
    var emptyText = Loading
    
    var Properties = [PropertyModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.getPropertyList()
    }
    
    func setupData(){
        self.navigationController?.isNavigationBarHidden = false
        self.title = ManageProperty
        self.setBackButton(withText: "")
        self.tableView.tableFooterView = UIView()
        addRefreshView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Properties.count == 0 {
            return 1
        }else{
            return Properties.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if Properties.count != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ManagePropertyTableViewCell", for: indexPath)  as! ManagePropertyTableViewCell
            cell.property = Properties[indexPath.row]
            cell.backgroundColor = UIColor.white
            cell.btnEdit.restorationIdentifier = "\(indexPath.row)"
            return cell
        }else{
            let cell = UITableViewCell()
            cell.textLabel?.text = self.emptyText
            cell.textLabel?.textAlignment = .center
            cell.isUserInteractionEnabled = false
            return cell
        }
    }
    
    func getPropertyList(){
        
        self.Properties.removeAll()
        
        guard let userId = MyProfile.userId else {
            tableView.reloadData()
            return
        }
        
        let params = ["user_id":userId]
        
        self.tableView.addWaitView()
        
        JSONRequest.makeRequest(kListProperty, parameters: params) { (data, error) in
            self.emptyText = ManageProNotFound
            self.tableView.removeWaitView()
            if error == nil{
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool, err{
                        print("Error")
                    }else{
                        if let msg = data["message"] as? String{
                            if let array = data["data"] as? [Any]{
                                for item in array{
                                    if let itemData = item as? [String:Any]{
                                        let property = PropertyModel(data: itemData)
                                        self.Properties.append(property)
                                    }
                                }
                            }
                            self.Properties = self.Properties.reversed()
                        }
                    }
                }
                 self.tableView.reloadData()
            }else{
                 self.tableView.reloadData()
            }
        }
        
    }
    
    func addRefreshView(){
        refreshControl.attributedTitle = NSAttributedString(string: Refreshing)
        refreshControl.tintColor = APP_PRIMARY_COLOR
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    @objc func refresh(){
        self.getPropertyList()
        self.refreshControl.endRefreshing()
    }
    
}
