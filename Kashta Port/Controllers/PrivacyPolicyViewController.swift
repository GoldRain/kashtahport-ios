//
//  PrivacyPolicyViewController.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = PrivacyPolicy
        self.setBackButton(withText: "")
        if appDel.currentAppleLanguage() == "ar" {
            self.webView.loadRequest(URLRequest(url: URL(string: kPrivacyPolicyAr)!))
        }else{
            self.webView.loadRequest(URLRequest(url: URL(string: kPrivacyPolicyEn)!))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.view.addWaitView()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.view.removeWaitView()
    }
    

}
