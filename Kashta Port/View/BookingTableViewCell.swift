//
//  BookingTableViewCell.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class BookingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgHotelImage: UIImageView!
    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblHotelDescription: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblRoomCount: UILabel!
    @IBOutlet weak var lblChildrenCount: UILabel!
    @IBOutlet weak var lblAdultCount: UILabel!
    @IBOutlet weak var viewStarRating: CosmosView!
    
    @IBOutlet weak var btnBookingStatus: UIButton!
    
    override func prepareForReuse() {
        self.btnBookingStatus.isHidden = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.btnBookingStatus.isHidden = true
//        self.btnBookingStatus.isUserInteractionEnabled = false
    }
    
    var bookRequest:BookingModel!{
        didSet{
            if let bookHotel = bookRequest.hotel{
                if let imgUrl = bookHotel.pictures?.first{
                    self.imgHotelImage.addWaitView()
                    self.imgHotelImage.sd_setImage(with: URL(string: imgUrl)!) { (img, err, _, _) in
                        self.imgHotelImage.removeWaitView()
                        if let image = img {
                            self.imgHotelImage.image = image
                        }else{
                            self.imgHotelImage.image = #imageLiteral(resourceName: "hotelPlaceholder")
                        }
                    }
                }else{
                    self.imgHotelImage.image = #imageLiteral(resourceName: "hotelPlaceholder")
                }
                self.lblHotelName.text = bookHotel.name!
                self.lblHotelDescription.text = bookHotel.description!
                self.lblRoomCount.text = "\(bookHotel.roomsNumber!) Room"
                self.lblChildrenCount.text = "\(bookHotel.childernNumber!) Children"
                self.lblAdultCount.text = "\(bookHotel.personNumber!) Adult"
                var currency = CurrentCurrency
                if let value = self.bookRequest.hotel?.currency, value != ""{
                    if value.lowercased() == "usd"{
                    }else{
                        currency = value
                    }
                }
                self.lblAmount.text = "\(currency.uppercased() ) \(bookRequest.price!)"
                self.viewStarRating.rating = bookHotel.rating!
            }
            if bookRequest.status?.lowercased() == "confirm" {
                self.setupBookingStatus(title: Accepted, color: APP_PRIMARY_COLOR, isHidden: false)
                
            }else if bookRequest.status?.lowercased() == "pending" {
                self.setupBookingStatus(title: "", color: .clear, isHidden: true)
                
            }else if bookRequest.status?.lowercased() == "reject" {
                self.setupBookingStatus(title: Rejected, color: .red, isHidden: false)
                
            }else if bookRequest.status?.lowercased() == "paid"{
                self.setupBookingStatus(title: Paid, color: GREEN, isHidden: false)
            }else if bookRequest.status?.lowercased() == "cancel"{
                self.setupBookingStatus(title: Cancelled, color: .red, isHidden: false)
            }
        }
    }
    
    func setupBookingStatus(title:String,color:UIColor, isHidden:Bool) {
        self.btnBookingStatus.isHidden = isHidden
        self.btnBookingStatus.setTitle(title, for: .normal)
        if color == APP_PRIMARY_COLOR{
            self.btnBookingStatus.setPrimaryLayer()
        }else if color == UIColor.red{
            self.btnBookingStatus.setRedLayer()
        }else if color == GREEN {
            self.btnBookingStatus.setGreenLayer()
        }else {
            self.btnBookingStatus.backgroundColor = color
        }
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func onAcceptClick(_ sender: Any) {
        let optionMenu = appDel.topViewController!.getAlertController(title: nil, message: AlertAccept)
        
        let logoutAction = UIAlertAction(title: Yes, style: .destructive) { (action) in
            if let id = self.bookRequest.bookingId{
                
                let wait = appDel.topViewController?.addWaitSpinner()
                
                MessageCenter.bookingUpdateStatus(id: id, status: "confirm"){ res,msg in
                    
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                    
                    if !res{
                        self.bookRequest.status = "confirm"
                        NotificationCenter.default.post(name: .BookingRequestStatusUpdate, object: nil, userInfo: ["id":id])
                    }else{
                        if let message = msg{
                            appDel.topViewController?.displayMessage(message)
                        }else{
                            appDel.topViewController?.displayMessage(FailedReject)
                        }
                    }
                }
            }
            return;
        }
        
        let cancelAction = UIAlertAction(title: No, style: .cancel, handler: nil)
        
        optionMenu.addAction(logoutAction)
        optionMenu.addAction(cancelAction)
        
        appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
        
    }
    
    @IBAction func onRejectClick(_ sender: Any) {
        let optionMenu = appDel.topViewController!.getAlertController(title: nil, message: AlertReject)
        
        let logoutAction = UIAlertAction(title: Yes, style: .destructive) { (action) in
            if let id = self.bookRequest.bookingId{
                
                let wait = appDel.topViewController?.addWaitSpinner()
                
                MessageCenter.bookingUpdateStatus(id: id, status: "reject"){ res, msg in
                    
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                    
                    if !res{
                        self.bookRequest.status = "reject"
                        NotificationCenter.default.post(name: .BookingRequestStatusUpdate, object: nil, userInfo: ["id":id])
                    }else{
                        if let message = msg{
                            appDel.topViewController?.displayMessage(message)
                        }else{
                            appDel.topViewController?.displayMessage(FailedReject)
                        }
                    }
                }
            }
            return;
        }
        
        let cancelAction = UIAlertAction(title: No, style: .cancel, handler: nil)
        
        optionMenu.addAction(logoutAction)
        optionMenu.addAction(cancelAction)
        
        appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
    }
    
    
    
}
