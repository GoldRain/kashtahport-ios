//
//  ImageCollectionViewCell.swift
//  Kashta Port
//
//  Created by mac on 23/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgViewImage: UIImageView!
}
