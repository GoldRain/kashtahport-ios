//
//  FavouriteTableViewCell.swift
//  Kashta Port
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class FavouriteTableViewCell: UITableViewCell {

    @IBOutlet weak var imgHotelImage: UIImageView!
    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblHotelDescription: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblRoomCount: UILabel!
    @IBOutlet weak var lblChildrenCount: UILabel!
    @IBOutlet weak var lblAdultCount: UILabel!
    @IBOutlet weak var viewStarRating: CosmosView!
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var favHotel:PropertyModel!{
        didSet{
            if let imgUrl = favHotel.pictures?.first{
                self.imgHotelImage.addWaitView()
                self.imgHotelImage.sd_setImage(with: URL(string: imgUrl)!) { (img, err, _, _) in
                    self.imgHotelImage.removeWaitView()
                    if let image = img {
                        self.imgHotelImage.image = image
                    }else{// placeholder image
                        self.imgHotelImage.image = #imageLiteral(resourceName: "hotelPlaceholder")
                    }
                }
            }else{// placeholder image
                self.imgHotelImage.image = #imageLiteral(resourceName: "hotelPlaceholder")
            }
            
            self.lblHotelName.text = favHotel.name!
            self.lblHotelDescription.text = favHotel.description!
            self.lblRoomCount.text = "\(favHotel.roomsNumber!) \(Room)"
            self.lblChildrenCount.text = "\(favHotel.wcNumber!) \(WC)"
            self.lblAdultCount.text = "\(favHotel.personNumber!) \(Adults)"
            var currency = CurrentCurrency
            if let value = self.favHotel?.currency, value != ""{
                if value.lowercased() == "usd"{
                }else{
                    currency = value
                }
            }
            self.lblAmount.text = "\(currency.uppercased()) \(favHotel.rate!)"
            self.viewStarRating.rating = Double(favHotel.rating!)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onCancelClick(_ sender: Any) {
        
    }
}
