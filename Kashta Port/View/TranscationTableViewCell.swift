//
//  TranscationTableViewCell.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class TranscationTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTranscationTitle: UILabel!
    @IBOutlet weak var lblTranscationTime: UILabel!
    @IBOutlet weak var lblTranscationAmount: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bgView.addShadow()
    }
    
    var transcation:TransactionModel!{
        didSet{
            self.lblTranscationTitle.text = transcation.title!
            self.lblTranscationTime.text = timeAgoSinceDate(Date(milliseconds: Int(transcation.createdAt!)!), numericDates: false)
            
            var currency = CurrentCurrency
            if let value = self.transcation.currency, value != ""{
                if value.lowercased() == "usd"{
                }else{
                    currency = value
                }
            }
            self.lblTranscationAmount.text = "\(currency) \(transcation.amount!)"
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
