//
//  SettingTableViewCell.swift
//  Kashta Port
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import OneSignal

class SettingTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSettingName: UILabel!
    @IBOutlet weak var onOffSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.onOffSwitch.isOn = MyProfile.isNotificationEnabled
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func OnOffSwitch(_ sender: UISwitch) {
        if sender.isOn {
            self.updateNotifiactionStatus(status: true)
        }else{
            self.updateNotifiactionStatus(status: false)
        }
    }
    
    func updateNotifiactionStatus(status:Bool){
        
        guard let userId = MyProfile.userId else{
            return
        }
        
        let params = ["user_id":userId,
                      "enable":"\(status)"]
        
        JSONRequest.makeRequest(kUpdateNotificationStatus, parameters: params) { (data, error) in
            if error == nil{
                if let data = data as? [String:Any]{
                    if let err = data["error"] as? Bool,err{
                        if let msg = data["message"] as? String{
                            print(msg)
                        }
                    }else{
//                        OneSignal.setSubscription(status)
                        MyProfile.isNotificationEnabled = status
                    }
                }
            }else{
                print(Error)
            }
        }
    }
}
