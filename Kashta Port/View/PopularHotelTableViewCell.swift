//
//  PopularHotelTableViewCell.swift
//  Kashta Port
//
//  Created by mac on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class PopularHotelTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnPrice: UIButton!
    @IBOutlet weak var viewStarrating: CosmosView!
    @IBOutlet weak var btnLike: UIButton!
    
    var workItem:DispatchWorkItem?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    var hotel:PropertyModel!{
        didSet{
            if let imgUrl = hotel.pictures?.first{
                self.imgView.addWaitView()
                self.imgView.sd_setImage(with: URL(string: imgUrl)!) { (img, err, _, _) in
                    self.imgView.removeWaitView()
                    if let image = img {
                        self.imgView.image = image
                    }else{// placeholder image
                        self.imgView.image = #imageLiteral(resourceName: "hotelPlaceholder")
                    }
                }
            }else{// placeholder image
                self.imgView.image = #imageLiteral(resourceName: "hotelPlaceholder")
            }
            if !MyProfile.isLogin{
                self.btnLike.isHidden = true
            }else{
                self.btnLike.isHidden = false
            }
            if MessageCenter.isLiked(hotelId: hotel.propertyId!) {
                btnLike.setImage(UIImage(named: "like (4)"), for: .normal)
            }else{
                btnLike.setImage(UIImage(named: "Image-2"), for: .normal)
            }
            self.lblTitle.text = hotel.name!
            self.lblDescription.text = hotel.description!
            var currency = CurrentCurrency
            if let value = self.hotel.currency, value != ""{
                if value.lowercased() == "usd"{
                }else{
                    currency = value
                }
            }
            self.btnPrice.setTitle("  \(currency.uppercased()) \(hotel.rate!)  ", for: .normal)
            self.viewStarrating.rating = Double(hotel.rating!)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onLikeClick(_ sender: UIButton) {
        
        workItem?.cancel()
        var isLike = false
        if MyProfile.isLogin{
            if sender.image(for: .normal) == UIImage(named: "Image-2"){
                sender.setImage(UIImage(named: "like (4)"), for: .normal)
                isLike = true
            }else{
                sender.setImage(UIImage(named: "Image-2"), for: .normal)
                isLike = false
            }
            
            workItem = DispatchWorkItem{
                if isLike{
                    MessageCenter.addFavouritesHotel(hotelId: self.hotel.propertyId!) {
                    }
                }else{
                    MessageCenter.removeFavouritesHotel(hotelId: self.hotel.propertyId!) {
                    }
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: workItem!)
            
        }else{
            appDel.topViewController?.displayMessage(AlertLogin)
        }
    }
}
