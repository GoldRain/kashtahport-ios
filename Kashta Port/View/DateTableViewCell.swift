//
//  DateTableViewCell.swift
//  Kashta Port
//
//  Created by mac on 24/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class DateTableViewCell: UITableViewCell {

    @IBOutlet weak var tfStartDate: CustomTextField!
    @IBOutlet weak var tfEndDate: CustomTextField!
    @IBOutlet weak var tfNightRate: CustomTextField!
    
    @IBOutlet weak var dateExpandableView:UIView!
    @IBOutlet weak var dateTopView:UIView!
    
    var delegate:DateCellExpandDelegate!
    
    var indexPath:IndexPath!
    
    var isExpand:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.contentView.addShadow()
    }
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "E, MMM d"
        return formatter
    }()
    
    fileprivate let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter
    }()
    
    var date : HotelDate!{
        didSet{
            self.tfStartDate.text = formatter.string(from: Date(milliseconds: date.startDate))
            self.tfEndDate.text = formatter.string(from: Date(milliseconds: date.endDate))
            self.tfNightRate.text = "\(date.nightRate) / \(Night)"
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func onCalenderClick(_ sender: Any) {
        
        if self.dateExpandableView.fd_collapsed{ // it means cell is Collaped
            
            self.delegate.expandDateTableViewCell(indexPath: self.indexPath, isExpand: true)
            
        }else{ // it means cell is Expanded
            
            self.delegate.expandDateTableViewCell(indexPath: self.indexPath, isExpand: false)
        }
    }
}

protocol DateCellExpandDelegate {
    func expandDateTableViewCell(indexPath:IndexPath, isExpand:Bool)
}
