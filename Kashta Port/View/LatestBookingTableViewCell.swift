//
//  LatestBookingTableViewCell.swift
//  Kashta Port
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import Cosmos
import goSellSDK

class LatestBookingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgHotelImage: UIImageView!
    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblHotelDescription: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblRoomCount: UILabel!
    @IBOutlet weak var lblChildrenCount: UILabel!
    @IBOutlet weak var lblAdultCount: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var viewStarRating: CosmosView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    var bookHotel:BookingModel!{
        didSet{
            if let letHotel = bookHotel.hotel{
                if let imgUrl = letHotel.pictures?.first{
                    self.imgHotelImage.addWaitView()
                    self.imgHotelImage.sd_setImage(with: URL(string: imgUrl)!) { (img, err, _, _) in
                        self.imgHotelImage.removeWaitView()
                        if let image = img {
                            self.imgHotelImage.image = image
                        }else{// placeholder image
                            self.imgHotelImage.image = #imageLiteral(resourceName: "adult-avatar")
                        }
                    }
                }else{// placeholder image
                    self.imgHotelImage.image = #imageLiteral(resourceName: "adult-avatar")
                }
                
                self.lblHotelName.text = letHotel.name!
                self.lblHotelDescription.text = letHotel.description!
                self.lblRoomCount.text = "\(letHotel.roomsNumber!) \(Room)"
                self.lblChildrenCount.text = "\(letHotel.childernNumber!) \(Children)"
                self.lblAdultCount.text = "\(letHotel.personNumber!) \(Adults)"
                var currency = CurrentCurrency
                if let value = self.bookHotel.hotel?.currency, value != ""{
                    if value.lowercased() == "usd"{
                    }else{
                        currency = value
                    }
                }
                self.lblAmount.text = "\(currency.uppercased()) \(bookHotel.price!)"
                self.viewStarRating.rating = letHotel.rating!
            }
            if let bookingStatus = bookHotel.status?.lowercased(){
                
                switch bookingStatus {
                    
                case "cancel" :
                    self.setupStatus(title: Cancelled, textColor: .red, bgColor: .clear, isUserInteraction: false)
                    
                case "pending" :
                    self.setupStatus(title: Cancel, textColor: .white, bgColor: .red, isUserInteraction: true)
                    
                case "reject" :
                    self.setupStatus(title: Rejected, textColor: .red, bgColor: .clear, isUserInteraction: false)
                    
                case "confirm" :
                    self.setupStatus(title: PayNow, textColor: .white, bgColor: APP_PRIMARY_COLOR, isUserInteraction: true)
                    
                case "paid" :
                    self.setupStatus(title: Paid, textColor: .white, bgColor: GREEN, isUserInteraction: false)
                    
                default:
                    print("DO NOTHING")
                }
            }
        }
    }
    
    func setupStatus(title:String, textColor:UIColor, bgColor:UIColor, isUserInteraction:Bool) {
        self.btnStatus.setTitle(title, for: .normal)
        self.btnStatus.setTitleColor(textColor, for: .normal)
        self.btnStatus.backgroundColor = bgColor
        self.btnStatus.isUserInteractionEnabled = isUserInteraction
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onCancelClick(_ sender: Any) {
        
    }
    
    @IBAction func onStatusClick(_ sender: UIButton) {
        
        if let isDeleted = bookHotel.hotel?.isDeleted, !isDeleted {
            
            if sender.title(for: .normal) == Cancel {
                
                let optionMenu = appDel.topViewController!.getAlertController(title: nil, message: AlertCancel)
                
                let logoutAction = UIAlertAction(title: Yes, style: .destructive) { (action) in
                    if let id = self.bookHotel.bookingId{
                        
                        let wait = appDel.topViewController?.addWaitSpinner()
                        
                        MessageCenter.bookingUpdateStatus(id: id, status: "Cancel", completion: { res , msg in
                            
                            if let wait = wait{
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                            
                            if !res{
                                self.bookHotel.status = "Cancel"
                                NotificationCenter.default.post(name: .BookingRequestStatusUpdate, object: nil, userInfo: ["id":id])
                                
                            }else{
                                if let mssg = msg{
                                    appDel.topViewController?.displayMessage(mssg)
                                }else{
                                    appDel.topViewController?.displayMessage(FailedCancel)
                                }
                            }
                        })
                    }
                }
                
                let cancelAction = UIAlertAction(title: No, style: .cancel, handler: nil)
                
                optionMenu.addAction(logoutAction)
                optionMenu.addAction(cancelAction)
                
                appDel.topViewController?.present(optionMenu, animated: true, completion: nil)
                
            }
                
            else{
                if let fullName = MyProfile.userName,let email = MyProfile.userEmail, let number = MyProfile.mobile, let price = bookHotel?.price {
                    var firstName = ""
                    var lastName = ""
                    var components = fullName.components(separatedBy: " ")
                    if(components.count > 0)
                    {
                        firstName = components.removeFirst()
                        lastName = components.joined(separator: " ")
                    }
                    
                    var curreny = CurrencyType
                    
                    if let value = self.bookHotel.hotel?.currency, value != ""{
                        curreny = value
                    }
                    
                    _ = PaymentViewController.showPaymentWithData(amount: Decimal(string: price)!,
                                                                  currency: curreny.lowercased(),
                                                                  customerFirstName: firstName,
                                                                  customerLastName: lastName,
                                                                  customerEmail: email,
                                                                  customerPhone: number,
                                                                  delegate: self)
                }else{
                    appDel.topViewController!.displayMessage(ErrorPay)
                }
            }
        }else{
            appDel.topViewController?.displayMessage(AlertNotFound)
        }
    }
}

//MARK: PaymentViewControllerDeletgate
extension LatestBookingTableViewCell:PayementViewControllerDelegate {
    
    func paymentWebViewDidCancel(_ paymentViewController: PaymentViewController) {
        
        paymentViewController.dismiss(animated: true) {
            
        }
    }
    
    func paymentWebViewDidFinish(_ paymentViewController: PaymentViewController, paymentInfo: PaymentData) {
        
        paymentViewController.dismiss(animated: true) {
            if let booking = self.bookHotel{
                MessageCenter.paymentRequest(paymentData: paymentInfo, booking: booking, completion: { error in
                    
                    let wait = appDel.topViewController?.addWaitSpinner()
                    if !error{
                        MessageCenter.bookingUpdateStatus(id:booking.bookingId!, status: "Paid", completion: { res , msg in
                            
                            if let wait = wait{
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                            
                            if !res{
                                if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SucessFeedBackViewController") as? SucessFeedBackViewController {
                                    appDel.topViewController!.navigationController?.present(vc, animated: true, completion: {
                                        self.bookHotel?.status = "Paid"
                                        NotificationCenter.default.post(name: .BookingRequestStatusUpdate, object: nil, userInfo: ["id":self.bookHotel.bookingId!])
                                    })
                                }
                            }else{
                                
                                if let vc = UIStoryboard(name: "Transactions", bundle: nil).instantiateViewController(withIdentifier: "TransactionFailedViewController") as? TransactionFailedViewController {
                                    vc.modalTransitionStyle = .crossDissolve
                                    vc.modalPresentationStyle = .overCurrentContext
                                    appDel.topViewController!.navigationController?.present(vc, animated: true, completion: nil)
                                }
                            }
                        })
                    }else{
                        if let vc = UIStoryboard(name: "Transactions", bundle: nil).instantiateViewController(withIdentifier: "TransactionFailedViewController") as? TransactionFailedViewController {
                            vc.modalTransitionStyle = .crossDissolve
                            vc.modalPresentationStyle = .overCurrentContext
                            appDel.topViewController!.navigationController?.present(vc, animated: true, completion: nil)
                        }
                    }
                })
            }
        }
    }
    
    func paymentWebViewDidFail(_ paymentViewController: PaymentViewController) {
        
        paymentViewController.dismiss(animated: true) {
            
        }
    }
}

