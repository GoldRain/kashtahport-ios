//
//  RoomSwipeCollectionViewCell.swift
//  Kashta Port
//
//  Created by mac on 22/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class RoomSwipeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblRoomType: UILabel!
    @IBOutlet weak var lblRoomNumber: UILabel!
    @IBOutlet weak var lblAdultNumber: UILabel!
    @IBOutlet weak var lblChildrenNumber: UILabel!
    @IBOutlet weak var lblWCNumber: UILabel!
}
