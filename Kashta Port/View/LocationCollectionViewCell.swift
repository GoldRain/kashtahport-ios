//
//  LocationCollectionViewCell.swift
//  Kashta Port
//
//  Created by mac on 20/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class LocationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewStarRating: CosmosView!
    @IBOutlet weak var lblPropertyPrice: UILabel!
    
    var location:PropertyModel!{
        didSet{
            if let imgUrl = location.pictures?.first{
                self.imageView.addWaitView()
                self.imageView.sd_setImage(with: URL(string: imgUrl)!) { (img, err, _, _) in
                    self.imageView.removeWaitView()
                    if let image = img {
                        self.imageView.image = image
                    }else{
                        self.imageView.image = #imageLiteral(resourceName: "hotelPlaceholder")
                    }
                }
            }else{
                self.imageView.image = #imageLiteral(resourceName: "hotelPlaceholder")
            }
            self.lblName.text = location.name!
            self.viewStarRating.rating = location.rating!
            var currency = CurrentCurrency
            if let value = location.currency, value != ""{
                if value.lowercased() == "usd"{
                }else{
                    currency = value
                }
            }
            self.lblPropertyPrice.text = " \(currency.uppercased()) \(location.rate!)   "
        }
    }
}
