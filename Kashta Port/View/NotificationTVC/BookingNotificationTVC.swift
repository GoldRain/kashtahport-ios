//
//  NotificationViewProfileTableViewCell.swift
//  kashtahPORT
//
//  Created by mac on 29/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class BookingNotificationTVC: UITableViewCell {
    
    @IBOutlet weak var lblMessage:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var imgPropertyImage:UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    
    var bookingNotification:NotificationModel! {
        didSet{
            self.lblMessage.text = bookingNotification.message
            if let createdAt = Int(bookingNotification.created_at){
                self.lblTime.text = timeAgoSinceDate(Date(milliseconds: createdAt), numericDates: false)
            }
            if let imgUrl = bookingNotification.hotelData?.pictures?.first, imgUrl != ""{
                self.imgPropertyImage.sd_addActivityIndicator()
                self.imgPropertyImage.sd_setImage(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "hotelPlaceholder"), options: []) { (_, _, _, _) in
                    self.imgPropertyImage.sd_removeActivityIndicator()
                }
                self.lblDescription.text = bookingNotification.hotelData?.description ?? ""
            }else{
                self.imgPropertyImage.image = #imageLiteral(resourceName: "hotelPlaceholder")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.addShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
