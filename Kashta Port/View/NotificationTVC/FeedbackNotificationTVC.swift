//
//  NotificationStarRatingTableViewCell.swift
//  kashtahPORT
//
//  Created by mac on 29/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import Cosmos

class FeedbackNotificationTVC: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewStar: CosmosView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDec: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    var bookingNotification:NotificationModel! {
        didSet{
            
            let message = bookingNotification.message

            if message.split(separator: " ").first == "You" {
                self.lblTitle.text = message
            }else{
                
                var firstName = message.split(separator: "@").first
                
                let restMessage = message.split(separator: "@").reversed().first
                if (firstName?.contains(" "))!{
                   firstName = firstName?.split(separator: " ").first
                }
        
                let finalMessage =  "\(firstName!.capitalized)\(restMessage!)"

                self.lblTitle.text = finalMessage
            }
            
            if let created_At = Int(bookingNotification.created_at){
                self.lblTime.text = timeAgoSinceDate(Date(milliseconds: created_At), numericDates: false)
            }
            
            self.viewStar.rating = bookingNotification.rating
    
            self.lblDec.text = bookingNotification.comment
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bgView.addShadow()
        // Initialization code
    }
    
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
