//
//  NotificationTableViewCell.swift
//  Kashta Port
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class FavouriteNotificationTVC: UITableViewCell {

    @IBOutlet weak var lblMessage:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    
    var bookingNotification:NotificationModel! {
        didSet{
            
            let message = bookingNotification.message
            
            if message.split(separator: " ").first == "You" {
                self.lblMessage.text = message
            }else{
                var name = message.split(separator: "@").first
                
                if (name?.contains(" "))! {
                    name = name?.split(separator: " ").first
                }
                
                let attribute1 = [NSAttributedString.Key.foregroundColor : APP_PRIMARY_COLOR]
                let attribute2 = [NSAttributedString.Key.foregroundColor : N_GREY]
                
                let firstAttributeName = NSAttributedString(string: "\(name!.capitalized)", attributes: attribute1)
                let secondMessage = NSAttributedString(string: "\(message.split(separator: "@").reversed().first!)", attributes: attribute2)
                
                let finalAttributeMessage = NSMutableAttributedString()
                finalAttributeMessage.append(firstAttributeName)
                finalAttributeMessage.append(secondMessage)
                
                self.lblMessage.attributedText = finalAttributeMessage
            }
    
            if let createdAt = Int(bookingNotification.created_at){
                self.lblTime.text = timeAgoSinceDate(Date(milliseconds: createdAt), numericDates: false)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func onViewListingClick(_ sender:UIButton) {
        
        if bookingNotification.message == "You have liked a property" {
            TabBarVC?.selectedIndex = 1
        }else{
            if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ManagePropertyViewController") as? ManagePropertyViewController{
                appDel.topViewController!.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
