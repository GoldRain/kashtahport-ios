//
//  ManagePropertyTableViewCell.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class ManagePropertyTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTimePorperty: UILabel!
    @IBOutlet weak var lblPropertyTitle: UILabel!
    @IBOutlet weak var lblPropertyDec: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnEnable: UIButton!
    @IBOutlet weak var btnDisable: UIButton!
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "E, MMM d"
        return formatter
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }
    
    var property:PropertyModel!{
        didSet{
            if property.isEnabled! {
                btnEnable.isEnabled = false
                btnDisable.isEnabled = true
                btnEnable.setGreyLayer()
                btnDisable.setRedLayer()
            }else{
                btnDisable.isEnabled = false
                btnEnable.isEnabled = true
                btnEnable.setPrimaryLayer()
                btnDisable.setGreyLayer()
            }
            
            if property.isApproved! {
                lblStatus.text = Approved
                lblStatus.textColor = UIColor.green
            }else{
                lblStatus.text = WaitApproved
                lblStatus.textColor = UIColor.red
            }
            self.lblPropertyTitle.text = property.name
            self.lblPropertyDec.text = property.description
            self.lblTimePorperty.text = formatter.string(from: Date(timeIntervalSince1970: TimeInterval(Int(property.createdAt!)!)/1000))
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onEditClick(_ sender: UIButton) {
        if let vc = UIStoryboard(name: "AddProperty", bundle: nil).instantiateViewController(withIdentifier: "AddPropertyViewController") as? AddPropertyViewController {
            vc.titleType = .edit
            vc.property = property
            appDel.topViewController!.navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func onEnableClick(_ sender: Any) {
        updateStatus(status: true)
    }
    
    @IBAction func onDisableClick(_ sender: Any) {
        updateStatus(status: false)
    }

    func updateStatus(status:Bool){
        guard let hotelId = property.propertyId else {
            return
        }
        
        let params = ["hotel_id":hotelId,
                      "is_enable":"\(status)"
        ]
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kManageProperty, parameters: params) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, err{
                    if let msg = data["message"] as? String{
                        appDel.topViewController?.displayMessage(msg)
                    }
                }else{
                    self.btnEnable.isEnabled = !status
                    self.btnDisable.isEnabled = status
                    if status{
                        self.btnEnable.setGreyLayer()
                        self.btnDisable.setRedLayer()
                    }else{
                        self.btnEnable.setPrimaryLayer()
                        self.btnDisable.setGreyLayer()
                    }
                    if let _ = data["message"] as? String{
                        
                    }
                }
            }
        }
    }
    
}
