//
//  ProfileTableViewCell.swift
//  Kashta Port
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProfileImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserProfession: UILabel!
    @IBOutlet weak var lblUserDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupData(){
        if let imgUrl = MyProfile.profilePhotoUrl, imgUrl != ""{
            self.imgProfileImage.addWaitView()
            self.imgProfileImage.sd_setImage(with: URL(string: imgUrl)!) { (img, err, _, _) in
                self.imgProfileImage.removeWaitView()
                if let image = img {
                    self.imgProfileImage.image = image
                }else{// placeholder image
                    self.imgProfileImage.image = #imageLiteral(resourceName: "2019-05-18")
                }
            }
        }else{// placeholder image
            self.imgProfileImage.image = #imageLiteral(resourceName: "2019-05-18")
        }
        self.lblUserName.text = MyProfile.userName?.capitalized
        self.lblUserProfession.text = MyProfile.profession
        self.lblUserDetails.text = MyProfile.description
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
