//
//  Helper.swift
//  kashtahPORT
//
//  Created by alienbrainz on 11/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore
import SDWebImage
import Alamofire


enum NavigationItemBackButtonAction {
    case pop
    case dismiss
}

func uploadImagesToAws(_ images: [Image], folderName:String, randomName:String, callback:@escaping (([String]?)->())) {
    
    var uploadUrls = [String]()
    
    var count = images.count
    for image in images{
        
        if image.imgUrl == ""{

            let img = image.image.wxCompress()
            
            //This gives you the URL of the path
            var randomFileName = ""
            if images.count <= 1 {
                randomFileName = randomName + ".jpg"
            }else{
                randomFileName = randomString(length: 20) + ".jpg"
            }
            
            let imageFileName = folderName + randomFileName
            
            do{
                
                let transferUtility = AWSS3TransferUtility.default()
                let uploadExpression = AWSS3TransferUtilityUploadExpression()
                uploadExpression.setValue("AES256", forRequestHeader: "x-amz-server-side-encryption")
                
                uploadExpression.progressBlock = {(task, progress) in
                    print("Upload progress: ", progress.fractionCompleted)
                    
                    if Int (progress.fractionCompleted * 100) == 100{
                        NotificationCenter.default.post(name: .ImageUploadCompleted, object: nil)
                    }
                }
                
                let uploadCompletionHandler = { (task: AWSS3TransferUtilityUploadTask, error: Error?) -> Void in
                    if let error = error {
                        //Error completing transfer. Handle Error
                        print("Upload failed with error: (\(error.localizedDescription))")
                        print("Upload failed with exception (\(task.description))")
                        count = -1
                        DispatchQueue.main.async {
                            callback(nil)
                        }
                    }
                    else {
                        //Successfully uploaded.
                        
                    }
                }
                
                transferUtility.uploadData(
                    img.jpegData(compressionQuality: compression)!,
                    bucket: bucket,
                    key: imageFileName,
                    contentType: "image/jpg",
                    expression: uploadExpression,
                    completionHandler: uploadCompletionHandler
                    ).continueWith (block: { (task) -> Any? in
                        if let error = task.error {
                            print(error.localizedDescription)
                            //Error initiating transfer. Handle error
                            count = -1
                            DispatchQueue.main.async {
                                callback(nil)
                            }
                            return nil
                        }
                        if task.isCompleted{
                            
                            let url = AWSS3.default().configuration.endpoint.url
                            let publicURL = url?.appendingPathComponent(bucket).appendingPathComponent(imageFileName)
                            print("Uploaded to:\(publicURL!)")
                            
                            if let publicURL = publicURL{
                                
                                uploadUrls.append(publicURL.absoluteString)
                                SDWebImageManager.shared().saveImage(toCache: img, for: publicURL)
                            }
                            count = count - 1
                            if count >= 0{
                                if count == 0{
                                    DispatchQueue.main.async {
                                        callback(uploadUrls)
                                    }
                                }
                            }
                        }
                        
                        return nil
                    })
            }
        }else{
            uploadUrls.append(image.imgUrl)
            count = count - 1
            if count == 0{
                DispatchQueue.main.async {
                    callback(uploadUrls)
                }
            }
        }
        
        
        
    }
}

func deleteAwsFile(folderName:String, fullPath:String){
    
    if let fileName = URL(string: fullPath)?.lastPathComponent{
        let s3 = AWSS3.default()
        if let deleteObjectRequest = AWSS3DeleteObjectRequest(){
            deleteObjectRequest.bucket = bucket
            deleteObjectRequest.key = folderName+fileName
            s3.deleteObject(deleteObjectRequest).continueWith { (task) -> Any? in
                if let error = task.error {
                    print("Error occurred: \(error)")
                }
                else{
                    print("Deleted successfully.")
                }
                return nil
            }
        }
    }
}

func uploadVideosToAws(_ videos: [Data], folderName:String, randomName:String , callback:@escaping (([String]?)->())) {
    
    var uploadUrls = [String]()
    
    var count = videos.count
    for video in videos{
        
        if let documentsPathURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            //This gives you the URL of the path
            let randomFileName = randomName + ".mp4"
            let imageFileName = folderName + randomFileName
            
            do{
                //crate upload request
                let uploadRequest = AWSS3TransferManagerUploadRequest()!
                uploadRequest.bucket = bucket
                uploadRequest.acl = .publicRead
                //                    uploadRequest.uploadProgress = {(bytesSent, totalBytesSent, totalBytesExpectedToSend) in
                //
                //                        print("Uploading \(bytesSent)")
                //
                //                    }
                
                let localUrl = documentsPathURL.appendingPathComponent(randomFileName)
                //copy files to crate url
                try video.write(to: localUrl)
                uploadRequest.contentType = "video/mp4"
                
                //set upload body and filename
                uploadRequest.body = localUrl
                uploadRequest.key = imageFileName
                
                let transferManager = AWSS3TransferManager.default()
                transferManager.upload(uploadRequest).continueWith { (task:AWSTask) -> Any? in
                    
                    if let error = task.error {
                        print("Upload failed with error: (\(error.localizedDescription))")
                        print("Upload failed with exception (\(task.description))")
                        count = -1
                        DispatchQueue.main.async {
                            callback(nil)
                        }
                    }
                    
                    if task.result != nil {
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                        print("Uploaded to:\(publicURL!)")
                        
                        if let publicURL = publicURL{
                            
                            uploadUrls.append(publicURL.absoluteString)
                            try! FileManager.default.removeItem(at: localUrl)
                        }
                        count = count - 1
                        if count >= 0{
                            if count == 0{
                                DispatchQueue.main.async {
                                    callback(uploadUrls)
                                }
                            }
                        }
                    }
                    else{
                        print("Upload Error uploading file to aws")
                        DispatchQueue.main.async {
                            callback(nil)
                        }
                    }
                    return nil
                }
            }
            catch{
                print("Error writing file")
                count = -1
                DispatchQueue.main.async {
                    callback(nil)
                }
            }
        }
    }
}

func randomString(length: Int) -> String {
    
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    
    return randomString
}

func fullFormattedDate(date:Date) -> String{
    
    let formatter = DateFormatter()
    
    formatter.dateFormat = "EEE dd-MMM-yyyy hh:mm a"
    
    return formatter.string(from: date)
    
}

func shortFormattedDate(date:Date) -> String{
    
    let formatter = DateFormatter()
    
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    let myString = formatter.string(from: date)
    let yourDate = formatter.date(from: myString)
    
    formatter.dateFormat = "dd-MMM-yyyy"
    
    return formatter.string(from: yourDate!)
    
}

func getRefreshControl(attrStr:NSAttributedString) -> UIRefreshControl {
    
    let refreshControl = UIRefreshControl()
    
    refreshControl.backgroundColor = .clear
    refreshControl.tintColor = APP_PRIMARY_COLOR
    refreshControl.attributedTitle = attrStr
    
    return refreshControl
}

func openCallingApp(phoneNumber:String){
    if let url = URL(string: "tel://\(phoneNumber)") {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url as URL)
        }
    }
}

func timeAgoSinceDate(_ date:Date, numericDates:Bool = false) -> String {
    let calendar = NSCalendar.current
    let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
    let now = Date()
    let earliest = now < date ? now : date
    let latest = (earliest == now) ? date : now
    let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
    
    if (components.year! >= 2) {
        let str = NSLocalizedString("years ago", comment: "years ago")
        return "\(components.year!) \(str)"
    } else if (components.year! >= 1){
        if (numericDates){
            let str = NSLocalizedString("1 year ago", comment: "1 year ago")
            return str
        } else {
            let str = NSLocalizedString("Last year", comment: "Last year")
            return str
        }
    } else if (components.month! >= 2) {
        let str = NSLocalizedString("months ago", comment: "months ago")
        return "\(components.month!) \(str)"
    } else if (components.month! >= 1){
        if (numericDates){
            let str = NSLocalizedString("1 month ago", comment: "1 month ago")
            return str
        } else {
            let str = NSLocalizedString("Last month", comment: "Last month")
            return str
        }
    } else if (components.weekOfYear! >= 2) {
        let str = NSLocalizedString("weeks ago", comment: "weeks ago")
        return "\(components.weekOfYear!) \(str)"
    } else if (components.weekOfYear! >= 1){
        if (numericDates){
            let str = NSLocalizedString("1 week ago", comment: "1 week ago")
            return str
        } else {
            let str = NSLocalizedString("Last week", comment: "Last week")
            return str
        }
    } else if (components.day! >= 2) {
        let str = NSLocalizedString("days ago", comment: "days ago")
        return "\(components.day!) \(str)"
    } else if (components.day! >= 1){
        if (numericDates){
            let str = NSLocalizedString("1 day ago", comment: "1 day ago")
            return str
        } else {
            let str = NSLocalizedString("Yesterday", comment: "Yesterday")
            return str
        }
    } else if (components.hour! >= 2) {
        let str = NSLocalizedString("hours ago", comment: "hours ago")
        return "\(components.hour!) \(str)"
    } else if (components.hour! >= 1){
        if (numericDates){
            let str = NSLocalizedString("1 hour ago", comment: "1 hour ago")
            return str
        } else {
            let str = NSLocalizedString("An hour ago", comment: "An hour ago")
            return str
        }
    } else if (components.minute! >= 2) {
        let str = NSLocalizedString("minutes ago", comment: "minutes ago")
        return "\(components.minute!) \(str)"
    } else if (components.minute! >= 1){
        if (numericDates){
            let str = NSLocalizedString("1 minute ago", comment: "1 minute ago")
            return str
        } else {
            let str = NSLocalizedString("A minute ago", comment: "A minute ago")
            return str
        }
    } else if (components.second! >= 3) {
        let str = NSLocalizedString("seconds ago", comment: "seconds ago")
        return "\(components.second!) \(str)"
    } else {
        let str = NSLocalizedString("Just now", comment: "Just now")
        return str
    }
    
}
