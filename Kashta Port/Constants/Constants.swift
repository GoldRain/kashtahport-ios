//
//  Constants.swift
//  Kashta Port
//
//  Created by alienbrainz on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

//#03A9F4
public let APP_PRIMARY_COLOR = UIColor(red:0.01, green:0.66, blue:0.96, alpha:1.0)

public let LIGHT_GREY = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.0)

public let Am_Grey = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)

public let N_GREY = UIColor(red:59/255, green:59/255, blue:59/255, alpha:1.0)

public let LIGHT_BLACK = UIColor(red:0.34, green:0.34, blue:0.34, alpha:1.0)

public let GREEN = UIColor(red:0.40, green:0.76, blue:0.04, alpha:1.0)

let firstColor = UIColor(red:0.39, green:0.78, blue:0.95, alpha:1.0)
let seconColor = UIColor(red:0.02, green:0.67, blue:0.96, alpha:1.0)

public final class GradientPrimaryButton: UIButton {
    
    public override class var layerClass : AnyClass{
        return CAGradientLayer.self
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let thisLayer = layer as! CAGradientLayer
        thisLayer.colors = [firstColor.cgColor, seconColor.cgColor];
        thisLayer.startPoint = CGPoint(x: 0.3, y: 0.0)
        thisLayer.endPoint = CGPoint(x: 1.0, y: 0.05)
    }
    public override init(frame: CGRect) {
        super.init(frame:frame)
        let thisLayer = layer as! CAGradientLayer
        thisLayer.colors = [firstColor.cgColor, seconColor.cgColor];
        thisLayer.startPoint = CGPoint(x: 0.3, y: 0.0)
        thisLayer.endPoint = CGPoint(x: 1.0, y: 0.05)
    }
    
}

let firstColorRed = UIColor(red:1.00, green:0.32, blue:0.32, alpha:1.0)
let seconColorRed = UIColor(red:0.96, green:0.01, blue:0.01, alpha:1.0)

public final class GradientRedButton: UIButton {
    
    public override class var layerClass : AnyClass{
        return CAGradientLayer.self
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let thisLayer = layer as! CAGradientLayer
        thisLayer.colors = [firstColorRed.cgColor, seconColorRed.cgColor];
        thisLayer.startPoint = CGPoint(x: 0.3, y: 0.0)
        thisLayer.endPoint = CGPoint(x: 1.0, y: 0.05)
    }
    public override init(frame: CGRect) {
        super.init(frame:frame)
        let thisLayer = layer as! CAGradientLayer
        thisLayer.colors = [firstColorRed.cgColor, seconColorRed.cgColor];
        thisLayer.startPoint = CGPoint(x: 0.3, y: 0.0)
        thisLayer.endPoint = CGPoint(x: 1.0, y: 0.05)
    }
    
}

extension UIButton{
    
    open override class var layerClass : AnyClass{
        return CAGradientLayer.self
    }
    
    func setPrimaryLayer(){
        let thisLayer = layer as! CAGradientLayer
        thisLayer.colors = [firstColor.cgColor, seconColor.cgColor];
        thisLayer.startPoint = CGPoint(x: 0.3, y: 0.0)
        thisLayer.endPoint = CGPoint(x: 1.0, y: 0.05)
    }
    
    func setRedLayer(){
        let thisLayer = layer as! CAGradientLayer
        thisLayer.colors = [firstColorRed.cgColor, seconColorRed.cgColor];
        thisLayer.startPoint = CGPoint(x: 0.3, y: 0.0)
        thisLayer.endPoint = CGPoint(x: 1.0, y: 0.05)
    }
    
    func setGreyLayer(){
        let thisLayer = layer as! CAGradientLayer
        thisLayer.colors = [UIColor.gray.cgColor, UIColor.gray.cgColor];
        thisLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        thisLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
    }
    
    func setGreenLayer(){
        let thisLayer = layer as! CAGradientLayer
        thisLayer.colors = [GREEN.cgColor, GREEN.cgColor];
        thisLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        thisLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
    }
    
}
