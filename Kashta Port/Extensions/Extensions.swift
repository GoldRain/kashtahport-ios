//
//  Extensions.swift
//  Kashta Port
//
//  Created by alienbrainz on 17/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import Foundation
import NVActivityIndicatorView

public typealias WaitView = (first: UIView, second: UIView)

enum UserDefaultsKeys : String {
    case isLoggedIn
    case userID
}

class Extensions: UIViewController, UICollectionViewDelegateFlowLayout {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension UserDefaults{
    
    //MARK: Check Login
    func setLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //synchronize()
    }
    
    func isLoggedIn()-> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    //MARK: Save User Data
    func setUserID(value: Int){
        set(value, forKey: UserDefaultsKeys.userID.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getUserID() -> Int{
        return integer(forKey: UserDefaultsKeys.userID.rawValue)
    }
    
    func removeIsLogin(){
        
        removeObject(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    func removeUserID(){
        
        removeObject(forKey: UserDefaultsKeys.userID.rawValue)
    }
    
    func resetDefaults() {
        let dictionary = dictionaryRepresentation()
        dictionary.keys.forEach { key in
            removeObject(forKey: key)
        }
    }
    
    
}


extension UIColor {
    
    
    static let buttonColor  = UIColor(red:0.57999998330000002, green:0.80000001190000003 ,blue:0.33300000429999999, alpha:1.00)
    static let loginColor   = UIColor(red:0.31000000238418579, green:0.3449999988079071 ,blue:0.34900000691413879, alpha:1.00)
    static let imageGrayColor = UIColor.lightGray//UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0)
    static let purpleColor = UIColor(red: 42/255, green: 2/255, blue: 85/255, alpha: 1.0)
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    
}


extension UITextField{
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setPlaceholder(placeholder: String, color: UIColor){
        
        self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: color])
        
    }
    
    func isValidEmail() -> Bool{
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: self.text)
        
    }
    
    func addRightView(image:UIImage){
        
        let v = UIView()
        v.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        let imageView = UIImageView(image:image)
        
        imageView.frame = CGRect(x: 0, y: 0, width: 28, height: 28)
        imageView.frame.origin.y = 7
        
        v.addSubview(imageView)
        
        self.rightViewMode = .always
        self.rightView = v
        
    }
    
}

extension UIImage{
    
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func invertedImage(callback:@escaping ((UIImage?)->())){
        guard let cgImage = self.cgImage else {
            callback(nil)
            return
            
        }
        
        DispatchQueue(label:"invertImage").async {
            
            let ciImage = CoreImage.CIImage(cgImage: cgImage)
            guard let filter = CIFilter(name: "CIColorInvert") else { DispatchQueue.main.async {
                
                callback(nil)
                }
                return
            }
            filter.setDefaults()
            filter.setValue(ciImage, forKey: kCIInputImageKey)
            let context = CIContext(options: nil)
            guard let outputImage = filter.outputImage else { DispatchQueue.main.async {
                
                callback(nil)
                
                }
                return
            }
            guard let outputImageCopy = context.createCGImage(outputImage, from: outputImage.extent) else {
                DispatchQueue.main.async {
                    
                    callback(nil)
                    
                }
                return
                
            }
            
            DispatchQueue.main.async {
                
                callback(UIImage(cgImage: outputImageCopy))
                
            }
        }
        
        
    }
    
}

extension UIImageView{
    
    
    
}

extension UITableView{
    
    
    
}

extension UILabel{
    
    
    
}

extension UITextView{
    
    
    
}

extension UITableViewCell{
    
    
    
}

extension UIScrollView{
    
    public func addSubViewInScrollViewWithConstraint(view childView: UIView){
        
        childView.frame = self.bounds
        self.addSubview(childView)
        self.bringSubviewToFront(childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let right = NSLayoutConstraint(item: childView, attribute: .right , relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0)
        let bottom = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let left = NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0)
        
        self.addConstraints([top, right, bottom, left])
    }
    
    // Scroll to a particular view
    func scrollToView(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x: 0, y: childStartPoint.y, width: 1, height: frame.height), animated: animated)
        }
    }
    
    // Scroll to top
    func scrollToTop(animated: Bool) {
        let topOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(topOffset, animated: animated)
    }
    
    // Scroll to bottom
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }
    
    
}

extension UIView {
    
    @IBInspectable var BorderColor: UIColor?{
        set{
            layer.borderColor = newValue?.cgColor
        }
        
        get{
            return nil
        }
    }
    
    @IBInspectable var BorderWidth: CGFloat{
        set{
            layer.borderWidth = newValue
        }
        
        get  {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var CornerRadious: CGFloat{
        set{
            layer.cornerRadius = newValue
            if newValue > 0{
                layer.masksToBounds = true
            }else{
                layer.masksToBounds = false
            }
        }
        
        get{
            return layer.cornerRadius
        }
    }
    
    
    public func addSubViewWithConstraints(view childView: UIView){
        
        childView.frame = self.bounds
        self.addSubview(childView)
        self.bringSubviewToFront(childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let right = NSLayoutConstraint(item: childView, attribute: .right , relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0)
        let bottom = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let left = NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0)
        
        self.addConstraints([top, right, bottom, left])
    }
    
    public func addSubViewFromRightAnimated(view childView: UIView){
        
        childView.frame = self.bounds
        self.addSubview(childView)
        self.bringSubviewToFront(childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let right = NSLayoutConstraint(item: childView, attribute: .right , relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0)
        let bottom = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let left = NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0)
        
        self.addConstraints([top, right, bottom, left])
        
        childView.frame.origin.x += self.frame.width
        
        UIView.animate(withDuration: 0.5) {
            
            childView.frame.origin.x -= self.frame.width
            
            
        }
        
        
    }
    
    public func addSubViewFromLeftAnimated(view childView: UIView){
        
        childView.frame = self.bounds
        self.addSubview(childView)
        self.bringSubviewToFront(childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let right = NSLayoutConstraint(item: childView, attribute: .right , relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0)
        let bottom = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let left = NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0)
        
        self.addConstraints([top, right, bottom, left])
        
        childView.frame.origin.x -= self.frame.width
        
        UIView.animate(withDuration: 0.5) {
            
            childView.frame.origin.x += self.frame.width
            
        }
        
    }
    
    public func removeSubViewFromRightAnimated(){
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.frame.origin.x += self.frame.width
            
        }) { (true) in
            
            self.removeFromSuperview()
        }
        
        //self.removeFromSuperview()
        
    }
    
    public func removeSubViewFromLeftAnimated(){
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.frame.origin.x -= self.frame.width
            
        }) { (true) in
            
            self.removeFromSuperview()
        }
        //self.removeFromSuperview()
        
    }
    
    func addShadow(shadowColor:UIColor = UIColor.gray, size: CGSize = CGSize.zero, shadowOpacity:Float = 0.3) {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = size
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = 8.0
        
        
    }
    
    func addShadow1(shadowColor:UIColor = UIColor.gray, size: CGSize = CGSize.zero, shadowOpacity:Float = 0.1) {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = size
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = 8.0
        
        
    }
    
    func addGradient() {
        
        let v = UIView()
        v.tag = 12255
        v.frame = self.bounds
        let thisLayer = CAGradientLayer()
        thisLayer.frame = self.bounds
        //thisLayer.colors = [redColor.cgColor, purpleColor.cgColor];
        //thisLayer.colors = [goldenColor, goldenDark]
        thisLayer.startPoint = CGPoint(x: 0.5, y: 0.5)
        thisLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        v.layer.insertSublayer(thisLayer, at: 0)
        v.CornerRadious = 3.0
        v.layer.masksToBounds = true
        self.addSubview(v)
        self.sendSubviewToBack(v)
        
    }
    func removeGradientLayer()
    {
        if let v = self.viewWithTag(12255){
            
            v.removeFromSuperview()
        }
        
    }
    
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
    
    func setBackGroundImage(img:UIImage){
        
        UIGraphicsBeginImageContext(self.frame.size);
        
        img.draw(in: self.bounds)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext();
        
        self.backgroundColor = UIColor.init(patternImage: image!)
        
    }
    func drawInnerShadow(color:UIColor) {
        
        let innerShadowView = UIImageView(frame: bounds)
        innerShadowView.contentMode = .scaleToFill
        innerShadowView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(innerShadowView)
        innerShadowView.layer.masksToBounds = true
        innerShadowView.layer.borderColor = color.cgColor
        innerShadowView.layer.shadowColor = color.cgColor
        innerShadowView.layer.borderWidth = 1.0
        innerShadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
        innerShadowView.layer.shadowOpacity = 1.0
        // this is the inner shadow thickness
        innerShadowView.layer.shadowRadius = 5.0
    }
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func addWaitView(height:Int = 50, width:Int = 50){
        
        if let v = self.viewWithTag(500) as? NVActivityIndicatorView{
            self.bringSubviewToFront(v)
            v.startAnimating()
            return
        }
        
        assert(self.viewWithTag(500) == nil, "Another view having tag 500")
        
        let indicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: height, height: width), type: .circleStrokeSpin, color: APP_PRIMARY_COLOR)
        indicatorView.tag = 500
        
        self.addSubview(indicatorView)
        
        indicatorView.translatesAutoresizingMaskIntoConstraints = false
        
        indicatorView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        indicatorView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        indicatorView.widthAnchor.constraint(equalToConstant: CGFloat(width)).isActive = true
        indicatorView.heightAnchor.constraint(equalToConstant: CGFloat(height)).isActive = true
        
        self.bringSubviewToFront(indicatorView)
        
        indicatorView.startAnimating()
        
    }
    
    func removeWaitView(){
        
        if let v = self.viewWithTag(500) as? NVActivityIndicatorView{
            v.stopAnimating()
            self.viewWithTag(500)?.removeFromSuperview()
        }
    }
    
    func addActivityIndicator(style:UIActivityIndicatorView.Style = .whiteLarge) {
        
        self.removeActivityIndicator()
        
        let activityIndicator = UIActivityIndicatorView(style: style)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.tag = 6912
        self.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        activityIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        activityIndicator.startAnimating()
    }
    
    func removeActivityIndicator() {
        if let activityIndicator = self.viewWithTag(6912) as? UIActivityIndicatorView {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
}

extension UIViewController{
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let v = touches.first?.view{
            if v == self.view{
                self.view.endEditing(true)
            }
        }
    }
    
    // MARK: Check Internet Connection
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    func getAlertController(title:String?, message:String?, callback:(()->(Void))? = nil) -> UIAlertController{
        
        var alertController:UIAlertController!
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            
            alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        }
        else{
            
            alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        }
        return alertController
    }
    
    public func displayMessage(_ msg: String?, callback:(()->(Void))? = nil){
        
        self.displayMessage(title: nil, msg: msg, callback: callback)
        
    }
    public func displayMessage(title: String?, msg: String?, callback:(()->(Void))? = nil){

        if parent != nil{
            parent!.displayMessage(title: title, msg: msg, callback: callback)
            return
        }

        let alertView:UIAlertController!
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            alertView = UIAlertController(title: msg, message: title, preferredStyle: .alert)
        }
        else{
            alertView = UIAlertController(title: msg, message: title, preferredStyle: .actionSheet)
        }
        
        let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: { _ in
            alertView.dismiss(animated: true, completion: nil)
            callback?()
        })
        alertView.addAction(alertAction)
        present(alertView, animated: true, completion: nil)

    }
    
    //    public func addWaitSpinner()->WaitView{
    //
    //        let backView = UIView(frame: view.bounds)
    //        //        let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    //        //        indicatorView.color = UIColor.red
    //        //        view.addSubview(backView)
    
    public func addWaitSpinner()->WaitView {
        
        let backView = UIView(frame: view.bounds)
        //        let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        //        indicatorView.color = .white
        //        view.addSubview(backView)
        //        view.bringSubview(toFront: backView)
        
        let indicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .circleStrokeSpin, color: APP_PRIMARY_COLOR)
        view.addSubview(backView)
        
        
        backView.addSubview(indicatorView)
        indicatorView.center = backView.center
        backView.bringSubviewToFront(indicatorView)
        indicatorView.startAnimating()
        backView.alpha = 0.0
        backView.backgroundColor = UIColor.gray.withAlphaComponent(0.3)
        
        UIView.animate(withDuration: 0.25, animations: {
            backView.alpha = 1.0
        })
        
        return (backView, indicatorView)
    }
    
    public func removeWaitSpinner(waitView: WaitView){
        UIView.animate(withDuration: 0.25, animations: {
            waitView.first.alpha = 0.0
        }, completion: { _ in
            waitView.second.removeFromSuperview()
            waitView.first.removeFromSuperview()
        })
    }
    
    
    func setNavigationBar(){
        
        self.navigationController?.isNavigationBarHidden = false
        let nav = self.navigationController?.navigationBar
        nav?.barTintColor = UIColor(red: 79.0/255.0, green: 88.0/255.0, blue: 89.0/255.0, alpha: 1.0)
        nav?.barStyle = UIBarStyle.black
        //nav?.backgroundColor = UIColor(named: "LoginColor")
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }
    
    func setBackButton(withText : String, action:NavigationItemBackButtonAction = NavigationItemBackButtonAction.pop) {
        
//        let backButtonView  = UIView()
//
//        let textLabelInButton : UILabel = {
//            let lbl = UILabel()
//            lbl.text = withText
//            lbl.translatesAutoresizingMaskIntoConstraints = false
//            //            lbl.textColor = UIColor.black
//            lbl.textColor = UIColor(red:0.25, green:0.25, blue:0.25, alpha:1.0)
//            let font = UIFont.systemFont(ofSize: 17)
//            lbl.font = font
//            return lbl
//        }()
//
//        let imageInBackButton : UIImageView = {
//            let img = UIImageView()
//            img.image = UIImage(named: "go-back-left-arrow (3)")
//            img.translatesAutoresizingMaskIntoConstraints = false
//            img.contentMode = .scaleAspectFit
//            img.isUserInteractionEnabled = true
//
//            if action == .pop{
//                img.target(forAction: #selector(self.buttonClicked1), withSender: self)
//            }else{
//                img.target(forAction: #selector(self.buttonClicked2), withSender: self)
//            }
//
//            return img
//        }()
//        let button : UIButton = {
//            let btn = UIButton()
//
//            if action == .pop{
//                btn.addTarget(self, action: #selector(self.buttonClicked1), for: .touchUpInside)
//            }else{
//                btn.addTarget(self, action: #selector(self.buttonClicked2), for: .touchUpInside)
//            }
//
//            //            btn.backgroundColor = UIColor.red
//            btn.translatesAutoresizingMaskIntoConstraints = false
//            return btn
//        }()
//
//        backButtonView.addSubview(textLabelInButton)
//        backButtonView.addSubview(imageInBackButton)
//        backButtonView.addSubview(button)
//
//        // constarints for image inside of button
//        imageInBackButton.centerYAnchor.constraint(equalTo: backButtonView.centerYAnchor).isActive = true
//        imageInBackButton.leftAnchor.constraint(equalTo: backButtonView.leftAnchor , constant : -10).isActive = true
//        imageInBackButton.widthAnchor.constraint(equalToConstant: 22).isActive = true
//        imageInBackButton.heightAnchor.constraint(equalToConstant: 22).isActive = true
//
//        // constraints for label inside button
//        textLabelInButton.centerYAnchor.constraint(equalTo: backButtonView.centerYAnchor).isActive = true
//        textLabelInButton.leftAnchor.constraint(equalTo: imageInBackButton.rightAnchor ).isActive = true
//        textLabelInButton.widthAnchor.constraint(equalToConstant: 46).isActive = true
//        textLabelInButton.heightAnchor.constraint(equalToConstant: 20.5).isActive = true
//
//        // constraints for button
//        //        button.leadingAnchor.constraint(equalTo: backButtonView.leadingAnchor).isActive = true
//        button.leftAnchor.constraint(equalTo: backButtonView.leftAnchor , constant : -13).isActive = true
//        button.trailingAnchor.constraint(equalTo: backButtonView.trailingAnchor).isActive = true
//        button.topAnchor.constraint(equalTo: backButtonView.topAnchor).isActive = true
//        button.bottomAnchor.constraint(equalTo: backButtonView.bottomAnchor).isActive = true
//
//        backButtonView.frame = CGRect(x: -20, y: 0, width: 150, height: 35)
//        let spacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
//        spacer.width = -16; // it was -6 in iOS 6
//        let backButton = UIBarButtonItem(customView: backButtonView)
//        backButton.width = 80
//
//        //        backButtonView.backgroundColor = .red
//        self.navigationItem.leftBarButtonItems = [spacer,backButton]
    }
    
    @objc func buttonClicked1(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func buttonClicked2(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension UINavigationController {
    
    ///Get previous view controller of the navigation stack
    func previousViewController() -> UIViewController?{
        
        let lenght = self.viewControllers.count
        
        let previousViewController: UIViewController? = lenght >= 2 ? self.viewControllers[lenght-2] : nil
        
        return previousViewController
    }
    
    func popViewController(completion:@escaping (()->()), animation:Bool){
        
        CATransaction.begin()
        
        CATransaction.setCompletionBlock({
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                completion()
            })
        })//**/*//
        
        self.popViewController(animated: animation)
        
        CATransaction.commit()
    }
    
}
extension UIButton{
    @IBInspectable var setContentMode:Bool{
        
        get{
            return self.imageView?.contentMode == .scaleAspectFit
        }
        set(value){
            if value == true{
                self.imageView?.contentMode = .scaleAspectFit
            }
        }
        
    }
}

extension UIView{
    
    @IBInspectable var shadowOffset: CGSize{
        get{
            return self.layer.shadowOffset
        }
        set{
            self.layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowColor: UIColor{
        get{
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set{
            self.layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat{
        get{
            return self.layer.shadowRadius
        }
        set{
            self.layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float{
        get{
            return self.layer.shadowOpacity
        }
        set{
            self.layer.shadowOpacity = newValue
        }
    }
    
    func roundedCorner(){
        
        var corner:UIRectCorner?
        
        if appDel.currentAppleLanguage() == "ar"{
            corner = UIRectCorner.topRight
        }else{
            corner = UIRectCorner.topLeft
        }
        
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [corner!],
                                     cornerRadii: CGSize(width: 15, height: 15))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}
extension UIImageView{
    func roundedImage(){
        
         var corner:UIRectCorner?
        
        if appDel.currentAppleLanguage() == "ar"{
            corner = UIRectCorner.topRight
        }else{
            corner = UIRectCorner.topLeft
        }
        
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [corner!],
                                     cornerRadii: CGSize(width: 15, height: 15))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}

extension UILabel {
    
    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText
        
        let lengthForVisibleString: Int = self.vissibleTextLength
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font])
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        self.attributedText = answerAttributed
    }
    
    var vissibleTextLength: Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        
        let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedString.Key : Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
        
        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedString.Key : Any], context: nil).size.height <= labelHeight
            return prev
        }
        return self.text!.count
    }
}
