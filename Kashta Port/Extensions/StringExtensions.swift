//
//  StringExtensions.swift
//  Kashta Port
//
//  Created by mac on 19/04/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension String{
    
    func elapsedTimeFrom(dateValue:String) -> String {
        let before = Date(timeIntervalSince1970: Double(dateValue)!/1000)
        
        //just to create a date that is before the current time
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        //getting the current time
        let now = Date()
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.zeroFormattingBehavior = .dropAll
        formatter.maximumUnitCount = 1 //increase it if you want more precision
        formatter.allowedUnits = [.year, .month, .weekOfMonth, .day, .hour, .minute]
        formatter.includesApproximationPhrase = false //to write "About" at the beginning
        
        
        let formatString = NSLocalizedString("%@ ago", comment: "Used to say how much time has passed. e.g. '2 hours ago'")
        let timeString = formatter.string(from: before, to: now)
        return String(format: formatString, timeString!)
        
    }
    
    func trim() -> String{
        let str = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return str
    }
    
    func isEmpty() -> Bool{
        let str = self.trim()
        return str == ""
    }
    
    var value: String{
        return self.trim() == "" ? "0" : self.trim()
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func isValidInput() -> Bool {
        print(self)
        return !contains("\"") && !contains("\'") && !contains("&") && !contains("*") && !contains("=") && !contains("<") && !contains(">")
    }
    
    var length: Int {
        return self.characters.count
    }
    
    //    subscript (i: Int) -> String {
    //        return self[Range(i ..< i + 1)]
    //    }
    //
    //    func substring(from: Int) -> String {
    //        return self[Range(min(from, length) ..< length)]
    //    }
    //
    //    func substring(to: Int) -> String {
    //        return self[Range(0 ..< max(0, to))]
    //    }
    
    //    subscript (r: Range<Int>) -> String {
    //        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
    //                                            upper: min(length, max(0, r.upperBound))))
    //        let start = index(startIndex, offsetBy: range.lowerBound)
    //        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
    //        return self[Range(start ..< end)]
    //    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
    
}

func isEmpty(_ string: String?) -> Bool{
    
    if let empty = string{
        return empty.trim() == ""
    }else{
        return true
    }
}


func isValidEmailId(_ id: String?) -> Bool{
    
    if isEmpty(id){
        return false
    }
    
    let emailRegEx = "^[a-z0-9](\\.?[a-z0-9_-]){0,}@[a-z0-9-]+\\.([a-z]{1,6}\\.)?[a-z]{2,6}$"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: id)
}

func isValidMobileNumber(_ number: String?)->Bool{
    
    if isEmpty(number){
        return false
    }else if let _ = Int(number!){
        return true
    }else{
        return false
    }
}

func isValidDateString(dateString str:String) -> Bool{
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd MM yyyy"
    let dateFromString = dateFormatter.date(from: str)
    
    if dateFromString == nil {
        return true
    }
    return false
}
// convert images into base64 and keep them into string

func convertBase64ToImage(base64String: String) -> UIImage {
    
    let decodedData = NSData(base64Encoded: base64String, options: NSData.Base64DecodingOptions(rawValue: 0) )
    
    let decodedimage = UIImage(data: decodedData! as Data)
    
    return decodedimage!
    
}// end convertBase64ToImage


