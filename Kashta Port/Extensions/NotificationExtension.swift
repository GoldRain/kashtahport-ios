//
//  NotificationExtension.swift
//  kashtahPORT
//
//  Created by mac on 10/05/19.
//  Copyright © 2019 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let BookingRequestStatusUpdate = Notification.Name("BookingRequestStatusUpdate")
    static let HotelsFavouritesStatusUpdate = Notification.Name("HotelsFavouritesStatusUpdate")
    static let UserLoginLogout = Notification.Name("UserLoginLogout")
    static let SearchHotel = Notification.Name("SearchHotel")
    static let GetCity = Notification.Name("getCity")
    static let ImageUploadCompleted = Notification.Name("imageUploadCompleted")
    static let PaymentSuccess = Notification.Name("PaymentSuccess")
    static let UpdateBadge = Notification.Name("UpdateBadge")
}

